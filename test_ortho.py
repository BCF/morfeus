import pickle
import unittest
from os import listdir
from os.path import isfile, join


class OrthoCppTest(unittest.TestCase):
	"""Test to compare result between orthology function from decide_on_orthology and orthology.cpp"""

	def setUp(self):
		"""Load the name of all pickle file for comparaison"""
		self.file_cpp, self.file_py = 0, 0
		files = [f for f in listdir("compare/") if isfile(join("compare", f))]
		self.assertGreater(len(files), 0)

		for file in files:
			if file.startswith("ortho_res_cpp_"):
				self.file_cpp += 1
			elif file.startswith("ortho_res"):
				self.file_py += 1


	def test_compare_len(self):
		"""Compare the len of orthology search done"""
		self.assertEqual(self.file_py, self.file_cpp)


	def test_have_same_result(self):
		"""Compare the pickle file"""

		for i in range(self.file_py):
			t_cpp = open("compare/ortho_res_cpp_"+str(i+1), "rb")
			t_py = open("compare/ortho_res_"+str(i+1), "rb")
			cpp = pickle.load(t_cpp)
			py = pickle.load(t_py)
			t_cpp.close()
			t_py.close()

			t_c, t_p = sorted(list(cpp[0])), sorted(list(py[0]))
			self.assertEqual(t_p, t_c, f"Not same ortholog in py_job {py[3]} and cpp_job {cpp[3]} corresponding to " +
			f"iteration ortho_res_{i} and ortho_res_cpp_{i}")
			self.assertEqual(py[1], cpp[1], f"Not same uncertain_ortholog in py_job {py[3]} and cpp_job {cpp[3]} corresponding to " +
			f"iteration ortho_res_{i} and ortho_res_cpp_{i}")
			self.assertEqual(py[2], cpp[2], f"Not same paralog in iteration ortho_res_{i} and ortho_res_cpp_{i}")
			self.assertEqual(py[3], cpp[3], f"Not same job id py_{py[3]}, cpp_{cpp[3]} corresponding to " +
			f"iteration ortho_res_{i} and ortho_res_cpp_{i}")


