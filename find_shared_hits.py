
def ID_synonyms(blast_info):

	IDs_globalblast = set([])
	for key in list(blast_info.keys()):
		IDs_globalblast = IDs_globalblast.union(blast_info[key][0])
	return IDs_globalblast

def Ref_IDs(blast_info):

	synonyms = {}
	for key in blast_info:
		for entry in blast_info[key][0]:
			synonyms[entry] = key
	return synonyms


def find_common_hits(common_hits, blast_main_info, IDs_globalblast, Reference_IDs, actual_job, cut_evalue, fold,
					 query_identical_sequences):
	tocheck = [actual_job]
	while len(tocheck) > 0:	
		cand = tocheck[0]
		tocheck.remove(cand)
		common = IDs_globalblast['0'].intersection(IDs_globalblast[cand])
		commonIds_in_query_cand = {}
		for entry in common:
			key = Reference_IDs['0'][entry]
			value = Reference_IDs[cand][entry]
				
			commonIds_in_query_cand[key] = value

		while len(common) > 0:
			hit = list(common)[0]
			hitId_in_query = Reference_IDs['0'][hit]
			hitId_in_cand = Reference_IDs[cand][hit]
			hitProteinIds_in_query = blast_main_info['0'][hitId_in_query][0]
			hitProteinIds_in_cand = blast_main_info[cand][hitId_in_cand][0]
			common = common.difference(hitProteinIds_in_query)
			hitnewIds = hitProteinIds_in_cand.difference(hitProteinIds_in_query)

			if len(hitnewIds) > 0:

				for newId in hitnewIds:
					Reference_IDs['0'][newId] = hitId_in_query
					blast_main_info['0'][hitId_in_query][0].add(newId)
			if cand != hitId_in_query:
				common_hits = evaluate_common_hit(common_hits, cand, hitId_in_cand, hitId_in_query, blast_main_info,
												  cut_evalue, fold, query_identical_sequences, commonIds_in_query_cand)
		break
	return common_hits, blast_main_info, IDs_globalblast, Reference_IDs


def evaluate_common_hit(common_hits, cand, hitId_in_cand, hitId_in_query, blast_main_info, cut_evalue, fold, query_identical_sequences, commonIds_in_query_cand):
	species_hit = blast_main_info[cand][hitId_in_cand][1]
	evalue_hit = blast_main_info[cand][hitId_in_cand][2]
	species_evalues = [[blast_main_info[cand][x][1], blast_main_info[cand][x][2]] for x in list(blast_main_info[cand].keys())]
	min_evalue_species_hit = min([y[1] for y in [x for x in species_evalues if x[0] == species_hit]])
	if hitId_in_query  not in query_identical_sequences:

		if evalue_hit == min_evalue_species_hit:
			common_hits[cand]['best'].add(hitId_in_query)
		elif min_evalue_species_hit >= cut_evalue and min_evalue_species_hit >= evalue_hit/int(fold):
			common_hits[cand]['acceptable'].add(hitId_in_query)
		else:
			common_hits[cand]['exclude'].add(hitId_in_query)
	elif cand != '0':
		qisIds_evalues = [blast_main_info[cand][commonIds_in_query_cand[x]][2] for x in set(query_identical_sequences).intersection(set(list(commonIds_in_query_cand.keys())))]
		min_evalue_qis = min(qisIds_evalues)
		if min_evalue_qis == min_evalue_species_hit:
			common_hits[cand]['best'].add('0')
		elif min_evalue_species_hit >= cut_evalue and min_evalue_species_hit >= min_evalue_qis/int(fold):
			common_hits[cand]['acceptable'].add('0')
		else:
			common_hits[cand]['exclude'].add('0')

	
	return common_hits

def create_common_hits(blast_info):
	
	common_hits = {}
	for key in list(blast_info.keys()):
		common_hits[key] = {'best' : set([]), 'acceptable' : set([]), 'exclude' : set([])}
	
	return common_hits


###########################################################################################################################
