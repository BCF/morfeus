import time
from threading import Thread
from pickleshare import PickleShareDB
from init import EUTILS_RESULT, EUTILSJOBS, API_KEY

class Eutilsdaemon(Thread):
	def __init__(self):
		Thread.__init__(self)
		self.eutilsjob = PickleShareDB(EUTILSJOBS)
		self.eutilsresult = PickleShareDB(EUTILS_RESULT)

	def run(self):
		to_delete = []
		while True:
			time.sleep(1.5)
			# Check if eutilsjob are present
			jobs = self.eutilsjob.keys()
			jobs.sort()
			if len(jobs) == 0:
				# print("Wait for Eutils")
				time.sleep(1)
			else:
				if API_KEY == "":
					if len(jobs) > 3:
						jobs = jobs[0:2]
				else:
					if len(jobs) > 10:
						jobs = jobs[0:8]

				t = str(int(time.time()))
				to_delete.append(t)
				# print("accept job :", jobs)
				self.eutilsresult[t] = jobs

				for key in jobs:
					# print("del flask request in eutils_jobs :", key)
					del self.eutilsjob[key]

			for t in to_delete:
				if int(time.time()) - int(t) > 5:
					# for key in self.eutilsresult[t]:
					# 	print("del flask request in eutils_jobs :", key)
					# 	del self.eutilsjob[key]
					del self.eutilsresult[t]
					to_delete.remove(t)
					# print("delete authorization in eutils_result :", t)
