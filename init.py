import os

# QUICKSTART
MORFEUS_USER    = "zweig"  # default morfeus user
BLASTP          = "/home/akusem/miniconda3/envs/test3/bin/blastp"    # Path to blastp
DATABASE        = "/home/akusem/Desktop/db/fungi.protein.faa"    # Path to blast database
API_KEY		    = "9bd32707762da632ec5cbb8e9a9039d81808"  # API_Key to make the esearch/efetch request faster and
                                                          # not being limited to 3 search by sec on the IP address.
                                                          # If you don't have any, information about their creation are here:
                                                          # https://ncbiinsights.ncbi.nlm.nih.gov/2017/11/02/new-api-keys-for-the-e-utilities/


MORFEUS_DIR     = os.getcwd() #morfeus base
#Path
#BIOPYTHON_PATH  = "/usr/local/lib/python2.6/dist-packages/biopython-1.57-py2.6-linux-x86_64.egg/"
DATA_PATH       = "www/run_data"
STORE_DATA      = os.path.join(MORFEUS_DIR, DATA_PATH)
STORE_SEQIN     = os.path.join(STORE_DATA, "%s", "inputseq")
STORE_BLASTOUT  = os.path.join(STORE_DATA, "%s", "outputseq")
STORE_CALC      = os.path.join(STORE_DATA, "%s", "calculate")
STORE_RESULT    = os.path.join(STORE_DATA, "%s", "results")
BLASTJOBS       = os.path.join(STORE_DATA, "%s", "todo_blast.db")
PARSEJOBS       = os.path.join(STORE_DATA, "%s", "todo_parse.db")
FINAL_RESULT    = os.path.join(MORFEUS_DIR, "final_results")


#BLAST queue
QHOST			= "bio"
QDATA	        = "/export2/zweig/to3/"
QINSEQ          = os.path.join(QDATA, "input")
QBLASTOUT       = os.path.join(QDATA, "output")
QINLOG          = os.path.join(QDATA, "inputlog")
QOUTLOG         = os.path.join(QDATA, "outputlog")
QSEQ_EXT        = '.seq'
QCMD_EXT        = '.cmd'
QOUT_EXT        = '.blop'
QLOG_EXT        = '.fin'

NETWORK_SIZE    = 4000
BLAST_TYPE      = 'blastp' # 'psiblast' # 

#Blast
BLASTPROGRAM    = BLASTP
BLASTDATABASE   = DATABASE

NBLASTPROGRAM	= BLASTP # os.path.join(MORFEUS_DIR, "bin/blastp_linux32")  # /usr/bin/blastcl3"
NBLASTDATABASE	= "refseq_protein" # "nr" #

# QUEUE BLAST
QBLASTPROGRAM	= BLASTP # "blastall"
QBLASTDATABASE	= DATABASE
QGILIST         = "/export/bio-databases/blast/Opisthokonta.gilist"
#QGILIST        = ""
QSEQPROGRAM	= "/home/akusem/anaconda3/envs/morfeus/bin/fastacmd"
SSH_USER	= "morfeus@localhost"			# could be different from morfeus user for now (bhabermann <-> BHabermann)
#SSH_HOST        = ("localhost", "localhost", "localhost")
SSH_PORT	= ("13303", "13304", "13307")
SSH_OPTION      = "-C -o CompressionLevel=1"

#Blast parameters
DEF_EVALUE      = 100.0
FILTER		= "yes"  # '"m S;C"'	        # soft masking, SEG + coiled-coil
SOFT_MASKING    = "true"
MATRIX          = "BLOSUM62"                    # default substitution matrix
CBS             = 'T'                           # Composition-based score adjustments 
LIMIT		= '"txid2759 [ORGN]"'		# no limit for searching
# LIMIT           = '"txid33154 [ORGN]"' # db search limit for netblast
NO_SEQ_ALIGN    = 2000  # no. seq to show alignment for
NO_SEQ_DESC     = 2000  # no. seq to show one-line descriptions for
NO_SEQ          = 1000  # new single no. seq parameter as xml output has no extra description lines
NO_CPU          = 4  # no. CPUs


# no. of speculative blasts
QSPEC           = NO_CPU - 1
USE_CPP         = True

#psiBlast parameters
MSA_TYPE = 'mafft'                              #choose any out of muscle, mafft, tcoffee, clustal
EVAL_INCLUSION = 1                              #The threshold for including a sequence to create the PSSM on the next iteration. 
PSI_ITERATIONS = 2                             #Choose number of iterations for PSI_BLAST
PSI_PROCESS = 'MANUAL'  #  'AUTO'#               #Auto carries on the PSI_BLAST with the default selection of sequence for pssm and manual allows you to choose seq for 1st pssm
MSA_SEQ = 'AUTO'  # 'SELECT'                      #selects top 10 sequences for pattern generation,
                                                #select option allows you to write a file with the seq of your interest for pattern generation for 1st pssm
                                                #this options works only for the manual PSI_PROCESS


from Bio.Blast import NCBIXML
# BLASTOUT_FORMAT = 0 # pairwise plain
# BLASTOUT_PARSER = NCBIStandalone.BlastParser()
# cluster blast output options
BLASTOUT_FORMAT = 5  # 7 # XML
BLASTOUT_PARSER = NCBIXML # NCBIXML.BlastParser()
SEQPROGRAM      = "/home/akusem/anaconda3/envs/morfeus/bin/fastacmd"
SEQDATABASE     = "/export/bio-databases/blast-db/nr"
# must be different from blast database as queue is used

#Filenames
QUERYFILE       = "query"
SAVEQUERY       = os.path.join(STORE_SEQIN, QUERYFILE)

#matrix
OPTIMA          = {'A': 36, 'C': 99, 'E': 40, 'D': 65, 'G': 67, 'F': 57, 'I': 35,
                   'H': 86, 'K': 37, 'M': 51, 'L': 32, 'N': 59, 'Q': 46, 'P': 74,
                   'S': 36, 'R': 56, 'T': 48, 'W': 110, 'V': 38, 'Y': 69, 'X': 0}

# Formula
FORMULAFILE     = "gnuplotip.txt"
FORMULA	        = '''   f(x) = k * exp(m * x)
                        k_value
                        m_value
                        fit f(x) "inputfile" using 1:2 via k, m
                        print k
                        print m
                  '''
# Evalue Cutoff for determine of best/acceptable/excluded relationship
CUT_EVALUE      = 0.000001
FOLD	        = 100

#Web server
NAME            = ""
PORT	        = "8080"  # "8080:"
HOME	        = "www/"
CGIJOBS         = os.path.join(MORFEUS_DIR, HOME, "todo_cgi.db")
EUTILSJOBS      = os.path.join(MORFEUS_DIR, HOME, "todo_eutils.db")
EUTILS_RESULT   = os.path.join(MORFEUS_DIR, HOME, "result_eutils.db")
JOB_DATETIME    = "%m%d%H%M"  # job datetime id formating
# JOB_DB          = os.path.join(MORFEUS_DIR, HOME, 'jobDB')

# mail server
MAILSERVER  = "mail"
MAILDOMAIN	= ""
MAILPORT	= "5870"
MAILUSER	= "zweig"
MAILPW      = ""
MAILSEC		= ""

# networkx.eigenvector_centrality
EVC_MAX_ITER = 100
EVC_TOL = 9.9999999999999995e-07


# ratio of id in exclusion set of all backblasts
EXCLUSION_RATIO = 0.33

# max no. blast hits before switching of clustering
MAX_CLUST = 1000

