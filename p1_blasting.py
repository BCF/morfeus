#!/usr/bin/env python
import time
from pickleshare import PickleShareDB
from parse_psiblast import *
from daemontools import firstRun, create_textfile
from init import BLASTJOBS, PARSEJOBS, STORE_DATA, STORE_SEQIN, STORE_BLASTOUT, \
		 BLASTPROGRAM, BLASTDATABASE, NO_CPU, BLASTOUT_FORMAT, FILTER, \
		 MATRIX, NO_SEQ_ALIGN, NO_SEQ_DESC, SEQPROGRAM, NBLASTPROGRAM, \
		 NBLASTDATABASE, LIMIT, QBLASTPROGRAM, QBLASTDATABASE, QINSEQ, \
		 QBLASTOUT, QINLOG, QOUTLOG, QSEQ_EXT, QCMD_EXT, QOUT_EXT, \
		 QLOG_EXT, QGILIST, QHOST, QSEQPROGRAM, BLAST_TYPE,CGIJOBS, \
		 SOFT_MASKING, MORFEUS_USER, SSH_USER, SSH_PORT, SSH_OPTION, NO_SEQ, \
		 MAILUSER

from Bio import Entrez 
Entrez.email = MAILUSER

class Daemon_blast(object):
	def __init__(self):
		self.cgijobDB = PickleShareDB(CGIJOBS %'.') # no need for path correction
	
	def run(self):
		"""Abstract daemon main loop"""
		raise NotImplementedError("Must implement run method!")
	
	def _do_blast(self):
		"""chosen blast method"""
		raise NotImplementedError("Must implement _do_blast method!")	
	
	def _get_fasta(self, fasta_cmd):
		print(fasta_cmd)
		result = os.popen(fasta_cmd).readlines()
		fhead = fseq = ''
		for line in result:
			if line.startswith('[fastacmd] ERROR:'):  # no id found
				#break
				return '', ''
			if line.startswith('>'):
				fhead += line
			else:
				fseq += line.rstrip()  # ommit last '\n'
		return fhead, fseq

	def _getsave_sequence(self, inseqdir, job):
		seqid  = job[1]
		seqfile = os.path.join(inseqdir, seqid)
		if os.path.exists(seqfile):
			print("get_sequence: seq file already exists: ", seqfile)
		else:
			print("getsave_sequence: retrieving sequence: ", seqid)	
			fhead, fseq = self._get_fasta(self.fasta_cmd % seqid)  # no need for fhead yet
			if fseq:
				create_textfile(seqfile, fseq)
			else:
				print("getsave_sequence: retrieval failed: ", seqid)
				return False
		return True  # default return
	
	def _updateset_parsejobs(self, user, job, seqID):
		parsejobDB = PickleShareDB(PARSEJOBS %user)
		parsejobDB[job[0]] = seqID
		print("daem_blasting() update parse dict: ", job[0], seqID)  # parsejobDB.items() gives sometimes KeyError?


class Local_blast(Daemon_blast):	
	def __init__(self):
		Daemon_blast.__init__(self)
		self.fasta_cmd = "%s -d %s -s %s -l -1" % (SEQPROGRAM, BLASTDATABASE, "%s")
		self.blast_cmd = "%s -p blastp -d %s -i %s -e %s -m %s -v %s -b %s -a %s -o %s -F %s -M %s" \
		%(BLASTPROGRAM, BLASTDATABASE, "%s", "%s", BLASTOUT_FORMAT, NO_SEQ_DESC, NO_SEQ_ALIGN, NO_CPU, "%s", FILTER, MATRIX)
		
	def run(self):
		while True:
			time.sleep(1)
			print('Local blast is running')
			users = list(self.cgijobDB.keys())
			users.sort()
			for user in users:
				print(user)
				e_value = self.cgijobDB[user][0]
				blastjobDB = PickleShareDB(BLASTJOBS %user)
				jobs = list(blastjobDB.items())	
				print("daem_blast - before removal: ", jobs)
				if len(jobs):
					job = jobs[0]
					print("daem_blast - after removal: ", jobs)
					if not self._getsave_sequence(STORE_SEQIN %user, job):
						print("daem_blast: no sequence found: ", job[1])
						blastjobDB.clear()
						break
					else:
						self._do_blast(user, job[1], e_value)
						self._updateset_parsejobs(user, job, job[1])
					# remove finished blast 
					jobs.remove(job)
					# update BLAST queue
					del blastjobDB[job[0]]		
				#else:
				#	time.sleep(0.3)
			if not len(users): 
					time.sleep(0.3)

	def _do_blast(self, user, job, e_value):
		input = os.path.join(STORE_SEQIN %user, job)
		output = os.path.join(STORE_BLASTOUT %user, job)
		if os.path.exists(output):
			print('blasting: blast output already exists:', output)
			return True
		else:
			print('blasting: ', job)
			os.system(self.blast_cmd %(input, e_value, output))
		return

class Net_blast(Local_blast):
	def __init__(self):
		#Local_blast.__init__(self)
		self.cgijobDB = PickleShareDB(CGIJOBS) # no need for path correction
		self.fasta_cmd = "%s"			    # no need for fastacmd
		self.blast_cmd = "%s -remote -db %s -query %s -evalue %s -outfmt %s -num_descriptions %s -num_alignments %s -out %s -seg %s -soft_masking %s -matrix %s -entrez_query %s" \
		%(NBLASTPROGRAM, NBLASTDATABASE, "%s", "%s", BLASTOUT_FORMAT, NO_SEQ_DESC, NO_SEQ_ALIGN, "%s", FILTER, SOFT_MASKING, MATRIX, LIMIT)

	def _get_fasta(self,fasta_cmd):
		print("Net_blast:", fasta_cmd)
		try:
			result = Entrez.efetch(db="protein", id=fasta_cmd, rettype="fasta").readlines()
		except IOError: 				# [Errno socket error] [Errno 110] Connection timed out
			print("Net_blast._get_fasta : NCBI Entrez connection timeout - wait 20s and try again...")
			time.sleep(20)
			try:
				result = Entrez.efetch(db="protein", id=fasta_cmd, rettype="fasta").readlines()
			except IOError:
				print("Net_blast._get_fasta : NCBI Entrez connection timeout - give up")
				sys.exit(-1)

		fhead = fseq = ''
		for line in result:
			if line.startswith('>'):
				fhead += line
			else:
				fseq += line.rstrip() # ommit last '\n'
		return fhead, fseq


class Queue_blast(Daemon_blast):
	def __init__(self):
		Daemon_blast.__init__(self)
		self.fasta_cmd = 'ssh %s@%s "%s -d %s -s %s -l -1"' % (MORFEUS_USER, QHOST, QSEQPROGRAM, QBLASTDATABASE, "%s")
		self.blast_cmd =  "%s -p blastp -d %s -e %s -m %s -v %s -b %s -F %s -M %s" \
		% (QBLASTPROGRAM, QBLASTDATABASE, "%s", BLASTOUT_FORMAT, NO_SEQ_DESC, NO_SEQ_ALIGN, FILTER, MATRIX)

	def run(self):
		while True:
			time.sleep(1)
			print('Queue blast is running')
			# print blastjobs
			users = list(self.cgijobDB.keys())
			users.sort()
			# print jobs_dict
			for user in users:
				e_value = self.cgijobDB[user][0]
				filter = self.cgijobDB[user][2]
				limit = self.cgijobDB[user][5]
				blastjobDB = PickleShareDB(BLASTJOBS %user)
				try:
					for job in list(blastjobDB.items()):
						print("In Queue_Blast.run", job)
						try:
							seqID, jobState = job[1]
						except ValueError:  # no state yet
							seqID = job[1]
							jobState = 0  # default: not submitted yet
						blastout = os.path.join(STORE_BLASTOUT %user,seqID)
						if jobState: 	# job state:  1-submitted
							if self._check_queue(seqID, blastout):
								self._updateset_parsejobs(user, job, seqID)
								del blastjobDB[job[0]]
								print('daem_blast: BLAST job finished: ', job)
						else:		# job state:0- not submitted yet (pending)
							if os.path.exists(blastout):
								print('daem_blast: blast output already exists:', seqID)
								self._updateset_parsejobs(user, job, seqID)
								del blastjobDB[job[0]]
								continue
							if not self._getsave_sequence(STORE_SEQIN %user,job):
								print("daem_blast: no sequence found: ", job[1])
								self._updateset_parsejobs(user, job, seqID)
								blastjobDB.clear()
								break
							else:
								if self._do_blast(user, seqID, e_value, filter, limit):
									jobState = 1 # job submitted
									blastjobDB[job[0]] = (seqID, jobState)

								else:
									print("daem_blast: Queuing failed: ", job)
				# PickleShareDB.items() gives sometimes KeyError
				except KeyError:
					continue  # try it again later

	def _do_blast(self, user, job, e_value, filter, limit):

		print('doBlast: queueing: ',job)

		try:
			inputM = os.path.join(STORE_SEQIN %user,job)
			inputQ = os.path.join(QINSEQ, job + QSEQ_EXT)
			print(inputM)
			print(inputQ)
			os.link(inputM, inputQ) 		# link input sequence -> blast input
			cmdFile = open(os.path.join(QINSEQ, job + QCMD_EXT),'w')
			cmd = self.blast_cmd % e_value
			print(cmd, file=cmdFile)
			cmdFile.close()
			print("doBlast: ", cmd)
			os.mknod(os.path.join(QINLOG, job), 0o644)			# touch inputlog/<input>
			return True
		except IOError:
			return False

	def _check_queue(self, job, blastout):
		qblastlog = os.path.join(QOUTLOG, MORFEUS_USER + '_' + job + QLOG_EXT)
		if os.path.exists(qblastlog):
			qblastout = os.path.join(QBLASTOUT, MORFEUS_USER + '_' + job + QOUT_EXT)
			print(qblastout, blastout)
			if os.stat(qblastout).st_size == 0:
				print("Zero blast output")
				return False
			os.rename(qblastout, blastout)		# link queue output -> morfeus
			os.remove(qblastlog)			# remove output log file	
			return True
		else:
			return False


class Queue_Net_blast(Queue_blast, Net_blast):
	
	def __init__(self):
		Net_blast.__init__(self)
		
	def _do_blast(self, user, job, e_value, filter, limit):
		input = os.path.join(STORE_SEQIN %user, job)
		output = os.path.join(STORE_BLASTOUT %user, job)

		print('_do_blast: blasting ', job)

		job_colm = user.split('-')
		jobID = job_colm[0]

		print(self.blast_cmd %(input, e_value, output))

		if job == jobID:
			if BLAST_TYPE == 'blastp': # Normal Blast
				p = subprocess.Popen(self.blast_cmd %(input, e_value, output), shell=True)
				print(p)
				return p
			if BLAST_TYPE == 'psiblast': #psi blast
				query = input.split('/')
				queryID = query[-1]
				PSI_process(queryID, e_value, output)
			else:
				print("please select Standard Blastp or PSI Blast for pairwise alignment search of your query !")
			print("Initial BLASTing !")
		else:
			p = subprocess.Popen(self.blast_cmd %(input, e_value, output), shell=True)
			return p
			print("Reciprocal BLASTing !")

	def _check_queue(self, job, blastout):
		if os.path.exists(blastout):
			if os.path.getsize(blastout) == 0:
				## print "_check_queue: Zero blast output"
				return False
			else:
				return True
		return False


class Queue_Local_blast(Queue_Net_blast):

	def __init__(self):
		Queue_Net_blast.__init__(self)
		self.blast_cmd = "%s -db %s -query %s -evalue %s -outfmt %s -max_target_seqs %s -out %s -seg %s -soft_masking %s -matrix %s" \
						 % (BLASTPROGRAM, BLASTDATABASE, "%s", "%s", BLASTOUT_FORMAT, NO_SEQ_DESC, "%s",
							FILTER, SOFT_MASKING, MATRIX)

class  Queue_NetSSH_blast(Queue_Net_blast):
	def __init__(self):
		self.port = SSH_PORT
		self.selected = 0
		self.count = 0
		self.cgijobDB = PickleShareDB(CGIJOBS) # no need for path correction
		self.fasta_cmd = "ssh -p %s %s %s -d %s -s %s -l -1" % (self.port[0], SSH_USER, QSEQPROGRAM, QBLASTDATABASE, "%s")
		# self.blast_cmd = "ssh %s %s@%s %s -p blastp -a %s -d %s <%s -e %s -m 7 -v %s -b %s -M %s -l %s >%s" \
		# % (SSH_OPTION, SSH_USER, "%s" ,QBLASTPROGRAM, NO_CPU, QBLASTDATABASE, "%s", "%s", NO_SEQ_DESC, NO_SEQ_ALIGN, MATRIX, QGILIST, "%s")
		
		# self.blast_cmd = "ssh %s %s@%s %s  -num_threads %s -db %s <%s -evalue %s -outfmt %s -max_target_seqs %s -seg %s -soft_masking %s -matrix %s %s >%s" \
		# % (SSH_OPTION, SSH_USER, "%s", QBLASTPROGRAM, NO_CPU, QBLASTDATABASE, "%s", "%s", BLASTOUT_FORMAT, NO_SEQ, FILTER, SOFT_MASKING, MATRIX, QGILIST, "%s")
		
	def _do_blast(self, user, job, e_value, filter, limit):
		
		# reset blast cmd, needed for SSH host change
		# self.blast_cmd = "ssh %s %s@%s %s -p blastp -a %s -d %s <%s -e %s -m 7 -v %s -b %s -M %s -l %s >%s" \
		# % (SSH_OPTION, SSH_USER, "%s" ,QBLASTPROGRAM, NO_CPU, QBLASTDATABASE, "%s", "%s", NO_SEQ_DESC, NO_SEQ_ALIGN, MATRIX, QGILIST,"%s")
		
		
		if not limit:		# complete refseq workaround
			self.blast_cmd = "ssh -p %s %s %s %s  -num_threads %s -db %s <%s -evalue %s -outfmt %s -max_target_seqs %s -seg %s -soft_masking %s -matrix %s >%s" \
			% ("%s", SSH_OPTION, SSH_USER, QBLASTPROGRAM, NO_CPU, QBLASTDATABASE, "%s", "%s", BLASTOUT_FORMAT, NO_SEQ, filter, SOFT_MASKING, MATRIX, "%s")
		
		else:
			self.blast_cmd = "ssh -p %s %s %s %s  -num_threads %s -db %s <%s -evalue %s -outfmt %s -max_target_seqs %s -seg %s -soft_masking %s -matrix %s -gilist %s >%s" \
			% ("%s", SSH_OPTION, SSH_USER, QBLASTPROGRAM, NO_CPU, QBLASTDATABASE, "%s", "%s", BLASTOUT_FORMAT, NO_SEQ, filter, SOFT_MASKING, MATRIX, limit, "%s")
		
		#print self.count
		# set SSH port
		self.blast_cmd = self.blast_cmd  % (self.port[self.count%3], "%s", "%s", "%s")
		#print self.blast_cmd
		self.count += 1
		
		return Queue_Net_blast._do_blast(self, user, job, e_value, filter, limit)
		
	def _get_fasta(self, fasta_cmd):
		
		return Daemon_blast._get_fasta(self, fasta_cmd)
		
	

def test():
	
	try:
		user_folder = os.path.join(STORE_DATA, 'NP_012649-test')
		
		if not os.path.exists(user_folder):
			os.mkdir(user_folder)
			os.mkdir(STORE_SEQIN %'NP_012649-test')
			os.mkdir(STORE_BLASTOUT %'NP_012649-test')	
		else:
			print("Folder already exists: " + user_folder)
			print("Removing existing test run")
			for inseq in os.listdir(STORE_SEQIN %'NP_012649-test'):
				#print inseq
				os.remove(os.path.join(STORE_SEQIN %'NP_012649-test', inseq))
			for outseq in os.listdir(STORE_BLASTOUT %'NP_012649-test'):
				#print outseq
				os.remove(os.path.join(STORE_BLASTOUT %'NP_012649-test', outseq))
				
	except OSError:
		print("Could not finish file/folder operation: " + user_folder)
		sys.exit(-1)
		
	cgijobDB = PickleShareDB(CGIJOBS)
	cgijobDB.clear()
	cgijobDB['NP_012649-test'] = ['100', None, 'yes', 'BLOSUM62', 'T', QGILIST, 'refseq', None]
	
	blastjobDB = PickleShareDB(BLASTJOBS % 'NP_012649-test')
	blastjobDB.clear()
	
	for i, id in enumerate(("NP_012649","XP_387474","XP_445381","XP_454202","XP_462592","XP_663708","XP_721422","XP_755285","XP_957316")):
		
		blastjobDB[str(i)] = id

	#blast_daemon = Queue_Net_blast()


if __name__ =='__main__':	
	if firstRun(sys.argv[0]):
		print("First run")	
		if sys.argv[1] == '-l':
			blast_daemon = Local_blast()
		elif sys.argv[1] == '-n':
			blast_daemon = Net_blast()
		elif sys.argv[1] == '-q':
			blast_daemon = Queue_blast()
		elif sys.argv[1] == '-qn':
			blast_daemon = Queue_Net_blast()
		elif sys.argv[1] == '-t':				# test
			blast_daemon = Queue_Net_blast()
			test()
			
		blast_daemon.run()
	else:
		print("Already running")
		sys.exit(-1)
