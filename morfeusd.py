#!/usr/bin/python

import optparse, os, pickle, sys, time, zipfile
import urllib.request, urllib.parse, urllib.error
import HTMLTemplate
from pickleshare import PickleShareDB
from init import MORFEUS_USER, MORFEUS_DIR, STORE_SEQIN, STORE_BLASTOUT, STORE_RESULT, DATA_PATH, STORE_DATA, FILTER, MATRIX, \
		 CBS, LIMIT, NBLASTDATABASE, CGIJOBS, JOB_DATETIME, OPTIMA, DEF_EVALUE, HOME, NAME, PORT, EVC_MAX_ITER, EVC_TOL, \
		 NETWORK_SIZE, EXCLUSION_RATIO, QGILIST, FINAL_RESULT, EUTILSJOBS, EUTILS_RESULT #PARSEJOBS, BLASTJOBS,
from p0_start import morfeus_run
from daemontools import firstRun
from network.drawing_mod import build_ortholog_network
from network.scoring_diff_alg import OrthologNetwork
from operator import itemgetter
from p2_eutils import Eutilsdaemon
from threading import Thread
# from mail import send

RE_RUN = False

import logging
logging.basicConfig (format='%(asctime)s %(module)s.%(funcName)s():%(lineno)d %(levelname)s - %(message)s', datefmt='%H:%M:%S', level=logging.DEBUG)
log = logging.getLogger("morfeusd")


def _sort_by_evalue(item1, item2):
	"""helper function for dict sort"""
	x = float(item1['evalue']) - float(item2['evalue'])
	if x < 0:
		return -1
	elif x > 0:
		return 1
	else:
		return 0
	

def _sort_by_score(item1, item2):
	"""helper function for dict sort"""
	x = float(item1['score']) - float(item2['score'])
	if x < 0:
		return 1
	elif x > 0:
		return -1
	else:
		return 0


def main(options):

	t_start = time.time()
	if options.graph:						# default: morfeus runs without PIL 
		from tree import TreeImage				# only necessary for "-g" 

	global folder, matrix, cleanup, jobDB  # paused
	data_path = DATA_PATH.split('/')[-1]  # remove "www/"
	# create DATA_PATH + STOREJOBS folder if not there
	for folder in (STORE_DATA, EUTILSJOBS, EUTILS_RESULT, FINAL_RESULT):
		if not os.path.exists(folder):
			os.mkdir(folder)
		#os.mkdir(STOREJOBS)

	# Launch Thread daemon for eutils request from get_blastinfo and from ajax request on the website
	eutils_thread = Eutilsdaemon()
	eutils_thread.start()

	while True:
		log.info("read job list..")
		# jobList = glob.glob(JOB_DIR+'/*')

		for jobID in list(cgijobDB.keys()):

			# job specific logging
			fh = logging.FileHandler(os.path.join(FINAL_RESULT, jobID + ".log"))
			formatter = logging.Formatter("%(asctime)s %(module)s.%(funcName)s():%(lineno)d %(levelname)s - %(message)s", datefmt='%H:%M:%S')
			fh.setFormatter(formatter)
			fh.setLevel(logging.INFO)
			log.addHandler(fh)

			log.debug("Job ID" + jobID)

			seqID, timeStamp = jobID.split('-')  # sequence id
			evalue, querySeq, filter, matrix, cbs, limit, database, email = cgijobDB[jobID]

			if querySeq:
				seqID = None 			# send seq job

			results = morfeus_run(userID=jobID, queryID=seqID, querySeq=querySeq, matrix=OPTIMA, e_value=evalue,
					      skip=options.skip, HighThroughPut=options, lokal=options.lokal, graph=options.graph, log=log)
			# print(results)
			log.info("END_OF_JOB")
			# results = pickle.load(open("compare/res_mofeus_run", 'rb'))

			if not results:				# no refseq query id
				log.info("Remove job " + jobID)
				del cgijobDB[jobID]
				continue
			# jobFile.close()
			result = {}

			if not options.batch:

				result['ortholog'], result['paralog'], result['uncertain'], blast, bb, common, tree_Michael, done = results

				if options.graph:				# cluster trees 
					# dendrogram generation
					# done tree
					dHits=[]
					for id in done:
						seqID = list(blast['0'][id][0])
						dHits.extend(seqID)
					treeImg = TreeImage(tree_Michael, dHits, [])
					treeFile = os.path.join(os.getcwd(), FINAL_RESULT, jobID+"_tree_done"+".png")
					treeImg.save(treeFile)
				
					# final tree
					try:
						oHits = [bb[o] for o in result['ortholog']]  # get ortholog names
					except:
						oHits = 'Unknown'
					pHits = []		# get paralog names, skip missing paralog '2'
					for p in result['paralog']:
						pHits.append(bb.get(p))
					treeImg = TreeImage(tree_Michael, oHits, pHits)
					treeFile = os.path.join(os.getcwd(), FINAL_RESULT, jobID+"_tree_final"+".png")
					treeImg.save(treeFile)
			
			else:

				result['ortholog'], result['paralog'], result['uncertain'], blast, bb, common, done = results

			# exclude 1st round
			
			bb_excluded_more_than_included = []	 # hits which are more excluded than acceptable/best	# list of ids to leaveout
			for hit in bb:
				id_exclude = 0.0
				id_acceptable_best = 0.0		
				for id in common:					
					if hit in common[id]['exclude']:				# no check for isoform 1st round
						# ncbi_hit = blast['0'][hit][5]				# check for isoform in acceptable/best							
						# for isoform in (common[id]['acceptable'].union(common[id]['best'])):
						#	if ncbi_hit==blast['0'][isoform][5]:
						# 		print "Not excluded as isoform in accp/best found: ", isoform
						#		break					# no exclude++ if isoform is there
						# else:
								id_exclude += 1.0					# exclude++ as no isoform
					elif hit in common[id]['acceptable'] or hit in common[id]['best']:
								id_acceptable_best += 1.0
					# print "id: %s %s ex: %i  in: %i" % (hit, bb[hit], int(id_exclude), int(id_acceptable_best))
				try:
					hit_ratio = id_exclude/(id_acceptable_best+id_exclude)
				except ZeroDivisionError:
					hit_ratio = 1.0  # EXCLUSION_RATIO				# hit gets excluded for shure
				if hit_ratio <= 0.5 or hit == '0':  # EXCLUSION_RATIO or hit=='0':		# not excluded, '0' never gets excluded
					# bb_new[hit] =  bb[hit]
					pass
				else:
					bb_excluded_more_than_included.append(hit)					# put in exclude list

			# exclude 2nd round	
			
			bb_new = {}										# bb after exclusion
			to_exclude = {}										# o/p/u exclude lists
			for s in ("ortholog", "paralog", "uncertain"):
				to_exclude[s] = [] 								# list of ids to exclude
				for hit in result[s]:
					id_exclude = 0.0
					id_acceptable_best = 0.0		
					for id in common:
						if id in bb_excluded_more_than_included: continue
						if hit in common[id]['exclude']:
							ncbi_hit = blast['0'][hit][5]				# check for isoform in acceptable/best							
							for isoform in (common[id]['acceptable'].union(common[id]['best'])):
								if ncbi_hit == blast['0'][isoform][5]:
									# print "Not excluded as isoform in accp/best found: ", isoform
									break					# no exclude++ if isoform is there
							else:
								id_exclude += 1.0					# exclude++ as no isoform
						elif hit in common[id]['acceptable'] or hit in common[id]['best']:
							id_acceptable_best += 1.0
					try:
						print("id: %s %s ex: %f  in: %f" % (hit, bb[hit], id_exclude, id_acceptable_best))
					
					except KeyError:							# KeyError '15' temp workaround
						log.error("Couldn't find hit in bb: %s %s" % (s, hit))
						continue
					try:
						hit_ratio = id_exclude/(id_acceptable_best+id_exclude)
					except ZeroDivisionError:
						hit_ratio = EXCLUSION_RATIO					# hit gets excluded for shure
					if id_acceptable_best == 1 and id_exclude == 0:				# Bianca added to get rid of single inclusions
						hit_ratio = EXCLUSION_RATIO					# Bianca added
					if hit_ratio < EXCLUSION_RATIO or hit == '0':				# not excluded, '0' never gets excluded
						bb_new[hit] = bb[hit]
					else:
						to_exclude[s].append(hit)					# put in exclude list

			# print zip(bb_new.keys(), [blast['0'][id][5] for id in bb_new])

			txtFile = open(os.path.join(os.getcwd(), FINAL_RESULT, jobID + "_excluded" + ".txt"), 'w')
			txtFile.write("ID\tDescription\tScore\tEvalue\tSpecies\n")
			
			for s in ("ortholog", "paralog", "uncertain"):						# check for excluded isoforms	
				for id in to_exclude[s]:
					ncbi_id = blast['0'][id][5]						# get gene id for potentially excluded isoform
					# print "%s %s" % (bb[id], ncbi_id)
					for hit in bb_new:							# find not excluded isoform
						ncbi_hit = blast['0'][hit][5]					# get gene id for new hit
						if ncbi_id == ncbi_hit:
							bb_new[id] = bb[id]					# re-add isoforms for not excluded new hit
							print("Re-add excluded isoform: %s %s" % (id, bb[id]))
							break						
					else:
						result[s].remove(id)						# exclude ortholog/paralog/uncertain hit
						
														# write txt file for excluded ids
						synonym, species, evalue, description, sequence_identity, ncbi_id = blast['0'][id]
						for syn in synonym: ref_id = syn
						txtFile.write("%s\t%s\t%s\t%s\n" % (ref_id, description, evalue, species))
						print("Excluded: %s %s" % (id, bb[id]))

			txtFile.close()

			pickle.dump(bb_new, open(os.path.join(STORE_RESULT % jobID, "bb_storageIds"), 'wb'))  # re-save bb_storageIds for scoring

			page = HTMLTemplate.HTMLTemplate("www/templates/result.tmpl")
			page.Set('job', jobID)
			page.Set('seq', jobID.split('-')[0])
			page.Set('exclfile', jobID + "_excluded" + ".txt")					# prepare excluded id file link
			page.Set('txtfile', jobID + ".txt")

			if len(bb_new) > 1:			# no network for empty result

				# scoring
				cPath = os.path.join(STORE_RESULT % jobID, "common")
				nPath = os.path.join(STORE_RESULT % jobID, "bb_storageIds")
				nw_score = OrthologNetwork(rootNode='0', connectionsPath=cPath, nodeIdPath=nPath, algorithm="eigenvector", max_iter=EVC_MAX_ITER, tol=EVC_TOL) 
				nw_score.loadNetwork()
				nw_score.createNetwork()
				nw_score.do_scoring()
				# nw_score.scoreNetwork()
				#oNW.safeScoredNetwork()

				myDot = []

				if options.graph:
					# network generation
					log.info("Generating Network :")
					build_ortholog_network(myDot, result['ortholog'], result['paralog'], result['uncertain'], blast,
										   bb, common, nw_score=nw_score)

					nwFile = os.path.join(os.getcwd(), FINAL_RESULT, jobID+"_network")
					myDot.append("}")

				#mySif = []
				file1 = os.path.join(os.getcwd(), FINAL_RESULT, jobID+"_Main_cytoscape"+".sif")  # interaction file for cytoscape
				mainfile = open(file1, 'w')
				file2 = os.path.join(os.getcwd(), FINAL_RESULT, jobID+"_Attribute_cytoscape"+".sif") #node attribute file for cytoscape
				nodefile = open(file2, 'w')
				sif, mainsif, nodesif = build_ortholog_network(myDot, result['ortholog'], result['paralog'],
															   result['uncertain'], blast, bb, common, nw_score=nw_score)
				mainfile.write(mainsif)
				nodefile.write(nodesif)
					
				log.info("Zipping the sif files")
				myCyto = os.path.join(os.getcwd(), FINAL_RESULT, jobID+"_cytoscape"+".zip")
				zout = zipfile.ZipFile(myCyto, "w")
				zout.writestr(file1, mainsif)
				zout.writestr(file2, nodesif)
				
				zout.close()
				mainfile.close()
				nodefile.close()
				log.info("Cytoscape files zipped successfully !")


				if not options.batch:
					if options.graph: 
						page.Set('tree', os.path.basename(treeFile))
						page.Set('network', os.path.basename(nwFile)+".html")
				page.Set('Cytoscape_Files', os.path.basename(myCyto))

				result_all = []
				for resultType in ('ortholog', 'paralog', 'uncertain'):
					result_all.extend(result[resultType])		# merge all results

				txtFile = open(os.path.join(os.getcwd(), FINAL_RESULT, jobID + ".txt"), 'w')
				txtFile.write("ID\tDescription\tScore\tEvalue\tSpecies\n")

				output = []				
				for item in result_all:
					outputRow = {}
					#try:
					#    ref = bb[item]
					#except KeyError:
					    #print >> sys.stderr, "Miss id in bb_storageIds: ", item, resultType
					synonym, species, evalue, description, sequence_identity, ncbi_id = blast['0'][item]
					for id in synonym:
						ref_id = id
					outputRow['ref'] = ref_id
					outputRow['species'] = species
					outputRow['evalue'] = evalue
					outputRow['desc'] = description
					outputRow['blastout'] = urllib.parse.quote(ref_id)
					try:
						outputRow['score'] = "%.2f" % nw_score.score[item]
					except KeyError:		# '0' has no score
						outputRow['score'] = "1.00"
					output.append(outputRow)

					txtFile.write("%(ref)s\t%(desc)s\t%(score)s\t%(evalue)s\t%(species)s\n" % outputRow)

				# output.sort(key=_sort_by_score)
				output.sort(key=itemgetter('score'))
				page.Set("result", output)

				txtFile.close()

			else:
				page.Set('Cytoscape_Files', "No results found!")
				synonym, species, evalue, description, sequence_identity, ncbi_id = blast['0']['0']
				for id in synonym:
					ref_id = id
				page.Set("result", [{'ref': ref_id, 'species': species, 'evalue': evalue, 'desc': description,
									 'blastout': urllib.parse.quote(ref_id), 'score': 1.00}])

			outFile = open(os.path.join(os.getcwd(), FINAL_RESULT, jobID+".html"), 'w')
			log.info(os.path.join(os.getcwd(), FINAL_RESULT, jobID + ".html"))
			outFile.write(page.Output())
			outFile.close()
						
			log.info("Remove job " + jobID)
			del cgijobDB[jobID]
			# if email:
			# 	send(email, "Your result is available at http://%s/#%s" % (NAME, jobID))

			fh.close()		# close and remove logging file handler
			log.removeHandler(fh)
		else:
			log.debug("Wait for CGI job..")

		time.sleep(1)
		# paused = True
		# signal.pause()


cgijobDB = PickleShareDB(CGIJOBS)  # no need for path correction
eutilsjobDB = PickleShareDB(EUTILSJOBS)
print("cgijobDB.keys()", list(cgijobDB.keys()))

if not firstRun(sys.argv[0]):
	log.critical("CGI instance already exists")
	sys.exit(-1)

parser = optparse.OptionParser("morfeusd.py [options] id1 [id2 .. idn]" )
parser.add_option("-c", "--cleanup", dest="cleanup", action="store_true", default=False, help="(C)leanup morfeusd queue")
parser.add_option("-e", "--e-value", dest="evalue", action="store", default=DEF_EVALUE, help="(E)-value")
parser.add_option("-f", "--file", dest="input", action="store_true", default=False, help="Input id (F)ile")
parser.add_option("-g", "--graph", dest="graph", action="store_true", default=False, help="Draw network (G)raph ")
parser.add_option("-l", "--local", dest="lokal", action="store_true", default=False, help="Use local BLAST queue")
parser.add_option("-r", "--re-run", dest="rerun", action="store_true", default=False, help="(R)u-run existing job")
parser.add_option("-s", "--skip", dest="skip", action="store_true", default=False, help="(S)kip clustering (re-run)")
parser.add_option("-n", "--no-clustering", dest="batch", action="store_true", default=False, help="(N)o clustering)")
(options, args) = parser.parse_args()

log.debug(options)
log.debug(args)


if options.cleanup:
	cgijobDB.clear()
	if not args:
		sys.exit(0)

if not args:  # daemon mode
	options.lokal = True
	pass

else:
	options.lokal = True
	if options.lokal:
		limit = QGILIST
	else:
		limit = LIMIT

	if options.input:			# id file provided
		id_file = args[0]
		if os.path.exists(id_file):
			queries = [line.rstrip() for line in open(id_file).readlines()]
			for query in queries:
				jobID = query + '-' + time.strftime(JOB_DATETIME)
				cgijobDB[jobID] = [options.evalue, None, FILTER, MATRIX, CBS, limit, NBLASTDATABASE, MORFEUS_USER]
				print(jobID)
			args = []
		else:
			# parser.error("Couldn't find file: ", id_file)
			log.critical("Couldn't find file: " + id_file)
			sys.exit(-1)

	if options.rerun:			# Re-run with existing jobID
		jobID = args[0]
		seq = None
		cgijobDB[jobID] = [options.evalue, seq, FILTER, MATRIX, CBS, limit, NBLASTDATABASE, MORFEUS_USER]

	else:
		queries = args[0:]			# ids provided
		seq = None
		for query in queries:
			jobID = query + '-' + time.strftime(JOB_DATETIME)
			cgijobDB[jobID] = [options.evalue, seq, FILTER, MATRIX, CBS, limit, NBLASTDATABASE, MORFEUS_USER]
			log.info("jobID:" + jobID)


print("Morfeusd started....")
main(options)

