import logging, math, sys
from os import system
from copy import *
import time


def store_position_info(alignment_Ids,bin_codiert):  # ********************ist fertig

	clusterspecific_positioninfo = {}

	for alig_id in list(alignment_Ids.keys()):
		clusterspecific_positioninfo[alig_id] = bin_codiert[alig_id]
	
	return clusterspecific_positioninfo


def combinations(bin_codiert,query,Blosum62):  # ************ist fertig

	id_liste = list(bin_codiert.keys())
	id_liste.sort()
	max_matches_per_alignment, score_matches, score_mismatches, normalize_dict = {},{},{},{}
		
	for alig_id in id_liste:
		numb = 0
		for pos in range(len(query)):
			if bin_codiert[alig_id][pos][0] == 1:
				numb += Blosum62[query[pos]]
		max_matches_per_alignment[alig_id] = numb
	
	x = 0
	for alig1_id in id_liste[0:-1]:
		x += 1
		
		for alig2_id in id_liste[x:]:
					normalize = (max_matches_per_alignment[alig1_id] * max_matches_per_alignment[alig2_id]) ** 0.5
					pair_id = str(min(int(alig1_id),int(alig2_id))),str(max(int(alig1_id),int(alig2_id)))
					normalize_dict[pair_id] = normalize
					
					score_matches[pair_id],score_mismatches[pair_id] = count_common_positions(Blosum62,query,normalize,bin_codiert[alig1_id],bin_codiert[alig2_id])
	return score_matches,score_mismatches,normalize_dict


def combinations_test(bin_codiert, query, Blosum62, log=logging):  # ************ist fertig

	id_liste = list(bin_codiert.keys())
	id_liste.sort()
	max_matches_per_alignment, score_matches, score_mismatches, normalize_dict = {}, {}, {}, {}
		
	for alig_id in id_liste:
		numb = 0
		for pos in range(len(query)):
			if bin_codiert[alig_id][pos][0] == 1:
				numb += Blosum62[query[pos]]
		max_matches_per_alignment[alig_id] = numb
		normalize_dict[alig_id] = numb	
	x = 0
	for alig1_id in id_liste[0:-1]:
		x += 1
		log.debug('alignment %s' %x)
		
		for alig2_id in id_liste[x:]:
					normalize = (max_matches_per_alignment[alig1_id] * max_matches_per_alignment[alig2_id]) ** 0.5
					pair_id = str(min(int(alig1_id), int(alig2_id))), str(max(int(alig1_id), int(alig2_id)))
					normalize_dict[pair_id] = normalize
					
					score_matches[pair_id], score_mismatches[pair_id] = \
						count_common_positions_test(Blosum62, query, normalize, bin_codiert[alig1_id],
													bin_codiert[alig2_id])
	return score_matches, score_mismatches, normalize_dict


def find_maximum(matches_mod_dict, mismatches_mod_dict, alignment_Ids):  # ***************ist fertig
	inv_matches, maximum = inv_dict(matches_mod_dict)
	candi = inv_matches[maximum]	
	mini = mismatches_mod_dict[candi[0]]
	key = candi[0]
	
	for pair in candi:
		if mismatches_mod_dict[pair] < mini:
			mini = mismatches_mod_dict[pair]
			key = pair
	print(key, maximum, end=' ')
		
	key1, key2 = key
	key_pair = [key1, key2]
	del alignment_Ids[key1]
	del alignment_Ids[key2]
	del matches_mod_dict[key]
	del mismatches_mod_dict[key]
	
	return key_pair, alignment_Ids, matches_mod_dict, mismatches_mod_dict, maximum


def store_level_cluster_info(candidates, Levels, Nodes, NodesMembers, NodesScores, NodesLevels, nextNode, subNode1,
							 subNode2, maximum):

	Nodes[nextNode] = (subNode1, subNode2)
	NodesMembers[nextNode] = NodesMembers[subNode1].union(NodesMembers[subNode2])
	try:
		NodesScores[nextNode] = 1.0/maximum
	except:
		NodesScores[nextNode] = 1.0/0.00001	 # because maximum is zero
	# NodesLevels[nextNode] = max(NodesLevel[subNode1],NodesLevels[subNode2]) + 1
	NodesLevels[nextNode] = len(NodesMembers[nextNode])-1

	for alig in NodesMembers[nextNode]:
		Levels[alig][NodesLevels[nextNode]] = nextNode

	return Levels, Nodes, NodesMembers, NodesScores, NodesLevels


def make_tree_NJPLOT(tree_Michael_format, blastinfo):

	Blast_Id = 0
	Species = 1
	EV = 2
	Protein = 3	
	store_NJPLOT_names = {}
	tree_Michael_substIds = {}
	for Id in list(blastinfo.keys()):
		store_NJPLOT_names[Id] = "'%s %s'"%(list(blastinfo[Id][Blast_Id])[0],blastinfo[Id][Species])  # quote blast hits + species to make drawgram happy
	nodes = list(tree_Michael_format.keys())
	nodes.sort()
	for node in nodes:
		subnodes = tree_Michael_format[node][0]
		subnode1 = subnodes[0]
		subnode2 = subnodes[1]
		distance = tree_Michael_format[node][1]
		subnode1_NJ = store_NJPLOT_names[subnode1]
		subnode2_NJ = store_NJPLOT_names[subnode2]
		store_NJPLOT_names[node] = '(%s:%s,%s:%s)'%(subnode1_NJ, distance, subnode2_NJ, distance)
		last_node = store_NJPLOT_names[node]
		if type(subnode1) == str:
			subnode1 = subnode1_NJ.strip("'")  # remove quotes from blast hits in tree_Michael
		if type(subnode2) == str:
			subnode2 = subnode2_NJ.strip("'")
		subnodes = (subnode1, subnode2)
		tree_Michael_substIds[node] = [subnodes, distance]
	return last_node + ';', tree_Michael_substIds # make drawgram happy


def store_tree_data(members, cand0, cand1, distance, nextNode, maximum, tree_Michael_format):
	try:
		tree_Michael_format[nextNode] = [(cand0, cand1), 1.0/maximum]  # Bianca changed from 1 to 1.0 - 8.12.2009
	except:
		tree_Michael_format[nextNode] = [(cand0, cand1), 1.0/0.00001]  # exceptions because maximum is zero
	members_cand0 = members[cand0]
	members_cand1 = members[cand1]
	members[nextNode] = members_cand0 + members_cand1
	for m0 in members_cand0:
		for m1 in members_cand1:
			distance[(str(min(int(m0), int(m1))), str(max(int(m0), int(m1))))] = maximum
	return members, distance, tree_Michael_format


def get_new_matrix(Nodes, bin_codiert, NodesMembers, candidates, nextNode, matches_mod_dict, mismatches_mod_dict,
				   alignment_Ids, clusterspecific_positioninfo_dict, query, normalize_dict, matches_dict,
				   mismatches_dict, Blosum62):  # **************fertig
	# clustering_approach = "classical average linkage"
	# clustering_approach = "modified average linkage, without positionspecific value"
	clustering_approach = "modified average linkage"
	# clustering_approach = "modified average linkage, Bianca method"
	
	new_matrix, new_matrix_mismatch, new_normalize, nextNodespecific_positioninfo_dict = {}, {}, {}, {}
	normalize = None
	
	nextNodespecific_positioninfo_dict[nextNode] = gemeinsam(clusterspecific_positioninfo_dict[candidates[0]],
															 clusterspecific_positioninfo_dict[candidates[1]])
	
	clusterspecific_positioninfo_dict.update(nextNodespecific_positioninfo_dict)
	
	for element in list(alignment_Ids.keys()):
	
		if clustering_approach == "classical average linkage":
			match_score_new,mismatch_score_new = get_new_distance1(NodesMembers, nextNode, element,
																   clusterspecific_positioninfo_dict, query,
																   normalize_dict, matches_mod_dict, Blosum62,
																   candidates, mismatches_mod_dict)  # classical average linkage
		if clustering_approach == "modified average linkage, without positionspecific value":
			match_score_new,mismatch_score_new = get_new_distance2(NodesMembers, nextNode, element,
																   clusterspecific_positioninfo_dict, query,
																   normalize_dict, matches_dict, mismatches_dict,
																   Blosum62)  # positionspecific value excluded
		if clustering_approach == "modified average linkage":
			match_score_new,mismatch_score_new = get_new_distance3(bin_codiert, NodesMembers, nextNode, element,
																   clusterspecific_positioninfo_dict, query,
																   normalize_dict, Blosum62)  # including positionspecific value
		if clustering_approach == "modified average linkage, Bianca method":
			match_score_new,mismatch_score_new, normalize = get_new_distance4(Nodes, bin_codiert, NodesMembers,
																			  nextNode, element,
																			  clusterspecific_positioninfo_dict, query,
																			  normalize_dict, Blosum62)  # including positionspecific value

		for candidate in candidates:
			matches_mod_dict = remove_distance(candidate, element, matches_mod_dict)
			mismatches_mod_dict = remove_distance(candidate, element, mismatches_mod_dict)
			if clustering_approach == "modified average linkage, Bianca method":
				normalize_dict = remove_distance(candidate, element, normalize_dict)
		new_matrix[(nextNode,element)] = match_score_new
		new_matrix_mismatch[(nextNode, element)] = mismatch_score_new
		if clustering_approach == "modified average linkage, Bianca method":
			new_normalize[(nextNode, element)] = normalize

	alignment_Ids[nextNode] = {}
	matches_mod_dict.update(new_matrix)
	mismatches_mod_dict.update(new_matrix_mismatch)
	if clustering_approach == "modified average linkage, Bianca method":	normalize_dict.update(new_normalize)

	return matches_mod_dict, mismatches_mod_dict, clusterspecific_positioninfo_dict, alignment_Ids

def inv_dict(matrix):

	invdistance = {}
	for key in list(matrix.keys()):
			distance = matrix[key]
			if distance in invdistance: invdistance[distance].append(key)
			else: invdistance[distance] = [key]

	distances = list(invdistance.keys())
	distances.sort()

	return invdistance, distances[-1]

def gemeinsam(liste1, liste2):  # **************fertig

	common_liste = []
	for element in range(len(liste1)):
		common_liste.append(liste1[element] + liste2[element])
			
	return common_liste	

def get_new_distance1(NodesMembers,nextNode,element,clusterspecific_positioninfo_dict,query,normalize_dict,matches_mod_dict,Blosum62,candidates,mismatches_mod_dict):# hieracical clustering, average linkage

	sumup_match, sumup_mis = 0,0
	for alig_id in candidates:
		key1 = (alig_id,element)
		key2 = (element,alig_id)

		try:
			x1 = matches_mod_dict[key1]
			x2 = mismatches_mod_dict[key1]

		except KeyError:
			x1 = matches_mod_dict[key2]
			x2 = mismatches_mod_dict[key2]
		sumup_match += x1
		sumup_mis += x2
	return sumup_match/2, sumup_mis/2

def get_new_distance2(NodesMembers,nextNode,element,clusterspecific_positioninfo_dict,query,normalize_dict,matches_dict,mismatches_dict,Blosum62):#*************fertig
	
	pos_value, pos_value_m = [],[]
	
	for pos in range(len(query)):
		cluster_cnt1 = clusterspecific_positioninfo_dict[nextNode][pos].count(1) * 1.0
		element_cnt1 = clusterspecific_positioninfo_dict[element][pos].count(1) * 1.0
		cluster_cnt0 = clusterspecific_positioninfo_dict[nextNode][pos].count(0) * 1.0
		element_cnt0 = clusterspecific_positioninfo_dict[element][pos].count(0) * 1.0
		all_posibilities = len(clusterspecific_positioninfo_dict[nextNode][pos]) * len(clusterspecific_positioninfo_dict[element][pos])
		pos_value.append((cluster_cnt1 * element_cnt1) / all_posibilities)
####		pos_value.append(all_posibilities)
##		pos_value.append(1)
		pos_value_m.append((cluster_cnt0 * element_cnt1 + cluster_cnt1 * element_cnt0) / all_posibilities)
####		pos_value_m.append(all_posibilities)
##		pos_value_m.append(1)

	match_score,mismatch_score = 0,0
	for alig1_id in NodesMembers[nextNode]:
		for alig2_id in NodesMembers[element]:

			key1 = (alig1_id,alig2_id)
			key2 = (alig2_id,alig1_id)
			try:
				matches = matches_dict[key1]
				mismatches = mismatches_dict[key1]

			except KeyError:
				matches = matches_dict[key2]
				mismatches = mismatches_dict[key2]


			match_score += matches
			mismatch_score += mismatches

	n = len(NodesMembers[nextNode]) * len(NodesMembers[element])
	match_score = match_score * 1.0 / n
	mismatch_score = mismatch_score * 1.0 / n
			
	return match_score,mismatch_score

def get_new_distance3(bin_codiert,NodesMembers,nextNode,element,clusterspecific_positioninfo_dict,query,normalize_dict,Blosum62):#*************fertig
	
	pos_value, pos_value_m = [],[]
	
	for pos in range(len(query)):
		cluster_cnt1 = clusterspecific_positioninfo_dict[nextNode][pos].count(1) * 1.0
		element_cnt1 = clusterspecific_positioninfo_dict[element][pos].count(1) * 1.0
		cluster_cnt0 = clusterspecific_positioninfo_dict[nextNode][pos].count(0) * 1.0
		element_cnt0 = clusterspecific_positioninfo_dict[element][pos].count(0) * 1.0
		all_posibilities = len(clusterspecific_positioninfo_dict[nextNode][pos]) * len(clusterspecific_positioninfo_dict[element][pos])
		pos_value.append((cluster_cnt1 * element_cnt1) / all_posibilities)
		pos_value_m.append((cluster_cnt0 * element_cnt1 + cluster_cnt1 * element_cnt0) / all_posibilities)


	match_score,mismatch_score = 0,0

	for alig1_id in NodesMembers[nextNode]:
		a1 = bin_codiert[alig1_id]
		for alig2_id in NodesMembers[element]:
			a2 = bin_codiert[alig2_id]

			key1 = (alig1_id,alig2_id)
			key2 = (alig2_id,alig1_id)
			try:
				normalize = normalize_dict[key1]
			except KeyError:
				normalize = normalize_dict[key2]
			matches,mismatches = 0,0
			for p in range(len(query)):

				if a1[p][0] == 1 and a2[p][0] == 1:
					matches += Blosum62[query[p]] * pos_value[p]
				elif a1[p][0] != a2[p][0]:
					mismatches += Blosum62[query[p]] * pos_value_m[p]

			try:
				matches = (matches * 1.0 / normalize) / (mismatches + 10**-20)
			except:
				matches = (matches * 1.0) / float(mismatches + 10**-20)# exception because Normalize is zero
			match_score += matches
			mismatch_score += mismatches

	n = len(NodesMembers[nextNode]) * len(NodesMembers[element])
	match_score = match_score * 1.0 / n
	mismatch_score = mismatch_score * 1.0 / n
			
	return match_score,mismatch_score

def get_new_distance4(Nodes,bin_codiert,NodesMembers,nextNode,element,clusterspecific_positioninfo_dict,query,normalize_dict,Blosum62):#*************fertig
	
	normalize_nextNode = normalize_dict[Nodes[nextNode]]

	normalize_element = normalize_dict[Nodes[element]]


	match_score,mismatch_score,normalize_nextpotentialNode = 0,0,0
	l_cluster = len(clusterspecific_positioninfo_dict[nextNode][0])
	l_element = len(clusterspecific_positioninfo_dict[element][0])
	all_posibilities = float(l_cluster * l_element)
	for pos in range(len(query)):
		cluster_cnt1 = sum(clusterspecific_positioninfo_dict[nextNode][pos])
		element_cnt1 = sum(clusterspecific_positioninfo_dict[element][pos])
		cluster_cnt0 = l_cluster - cluster_cnt1
		element_cnt0 = l_element - element_cnt1
		pos_value = (cluster_cnt1 * element_cnt1) / all_posibilities
		pos_value_m = (cluster_cnt0 * element_cnt1 + cluster_cnt1 * element_cnt0) / all_posibilities
		match_score += pos_value * Blosum62[query[pos]]
		mismatch_score += pos_value_m * Blosum62[query[pos]]
		normalize_nextpotentialNode += pos_value * Blosum62[query[pos]]
	normalize = (normalize_nextNode * normalize_element)**0.5
	match_score = (match_score * 1.0 / normalize) / (mismatch_score + 10**-20) # Please Bianca, remove this if it does not work. date: 091207


	return match_score,mismatch_score, normalize_nextpotentialNode

def remove_distance(candidate,element,matrix):#************fertig

	key1 = (candidate,element)
	key2 = (element,candidate)
	try:	del matrix[key1]
	except KeyError:	del matrix[key2]
	
	return matrix

def count_common_positions(Blosum62,query,normalize,a1,a2):#**********ist fertig
	
	match_score,mismatch_score = 0,0
	for p in range(len(query)):
		if a1[p][0] == 1 and a2[p][0] == 1:
			match_score += Blosum62[query[p]]
		elif a1[p][0] != a2[p][0]:
			mismatch_score += Blosum62[query[p]]
	#match_score = (match_score * 1.0 ) / (mismatch_score + 10**-20)
	match_score = (match_score * 1.0 / normalize) / (mismatch_score + 10**-20) # Please Bianca, remove this if it does not work. date: 091207

	return match_score,mismatch_score

def count_common_positions_test(Blosum62, query, normalize, a1, a2):  # **********ist fertig
	
	match_score, mismatch_score = 0, 0


	l_cluster = len(a1[0])
	l_element = len(a2[0])
	for pos in range(len(query)):

		cluster_cnt1 = sum(a1[pos])
		element_cnt1 = sum(a2[pos])
		cluster_cnt0 = l_cluster - cluster_cnt1
		element_cnt0 = l_element - element_cnt1
		all_posibilities = l_cluster * l_element
		pos_value = (cluster_cnt1 * element_cnt1) / all_posibilities
		pos_value_m = (cluster_cnt0 * element_cnt1 + cluster_cnt1 * element_cnt0) / all_posibilities

		match_score += pos_value * Blosum62[query[pos]]
		mismatch_score += pos_value_m * Blosum62[query[pos]]
	#match_score = (match_score * 1.0 ) / (mismatch_score + 10**-20)

	try:
		match_score = (match_score * 1.0 / normalize) / (mismatch_score + 10**-20) # Please Bianca, remove this if it does not work. date: 091207
	except:
		match_score = (match_score * 1.0) / float(mismatch_score + 10**-20) #because Normalize is zero

	return match_score,mismatch_score


def get_new_cut(dict_of_cuttings):#*************fertig
	#print dict_of_cuttings
	liste = list(dict_of_cuttings.keys())
	for pos in range(0,len(liste)-1):
		for pos1 in range(pos+1,len(liste)):
			x,y = liste[pos],liste[pos1]
			if len(dict_of_cuttings[x].intersection(dict_of_cuttings[y]))!=0 and len(dict_of_cuttings[x].symmetric_difference(dict_of_cuttings[y]))!=0:
				dict_of_cuttings[x].update(dict_of_cuttings[y])	
				dict_of_cuttings[y].update(dict_of_cuttings[x])	
	return dict_of_cuttings


def determine_home_clusters(storecalc,Levels,NodesMembers,NodesScores,clusterid):
# This function is responsible for cutting a given tree/cluster (ref. clusterid) into subclusters

	cutting_dict,dict_of_cuttings,membership = {},{},{}

# Here we go ! For any entity (ref. alig_Id) included within the tree the cut is calculated separately

	for alig_Id in NodesMembers[clusterid]:

		d_help={}	# d_help stores .....
		schalter = 'yes'
		while schalter:	# actually we never do more than one loop. However, it allows us to "break" whenever required

				schalter = None
				levelnode = Levels[alig_Id]	# levelnode is a dictionary.
				# for a given align_Id it stores the number of entities within any subtree (that includes that particular alig_Id) of the whole (!) tree as keys and the corresponding Id that subtree as values

				file = 'file'

				levels = deepcopy(list(levelnode.keys()))
				
				# as levelnode contains subclusterinformation of the whole tree and we might be interested in cutting a subcluster of the tree only, all nodes (= clusterIds as values of levelnode - as mentioned before) of higher-level-clusters are removed here:

				for level in list(levelnode.keys()):
					if int(levelnode[level]) > clusterid:
						levels.remove(level)

				if len(levels) == 1:	# if there is only one level left it does not make sense to do the calculation, so we stop here
					cutting_dict[alig_Id] = levels[0]					
					#print 'I break this loop'
					break

				levels1 = deepcopy(levels)

				scores = [NodesScores[levelnode[x]] for x in levels]
				#print 'scores in the beginning', scores
				maxlevel=max(levels)
				maxdist=max(scores)
				#print maxlevel, 'maxlevel'
				levels = [x*1.0/maxlevel for x in levels1]
				scores = [x*1.0/maxdist for x in scores]
				for pos in range(len(levels)):
					d_help[levels[pos]] =[scores[pos],levels1[pos]]
				levels.sort()
				scores = [d_help[x][0] for x in levels]
				x0 = levels[0]
				x1 = levels[-1]
				x2 = levels[0]
				y0 = scores[0]
				y1 = scores[-1]
				y2 = scores[0]

				#m = (math.log((1.0 * y2) / (1.0 * y1))) / (x2 - x1)
				#k = y1 / (math.exp(m * x1))
				m = 0.001
				k = 0.001
				anstieg_linear = (y0 - y1) / (x0 - x1)
				
				fh = open(storecalc + '/'+file,"w")
				print('levels(x) and scores(y)', file=sys.stdout)		
				for pos in range(len(levels)):
						fh.write(str(levels[pos]) + ' ' + str(scores[pos]) + '\n')
						print(levels[pos], scores[pos], file=sys.stdout)
				fh.close() 
				
				ipstring = open(storecalc + '/' + 'gnuplotip.txt').read().replace("inputfile",storecalc + '/' + file).replace('k_value','k=%s'%(str(k))).replace('m_value','m=%s'%(str(m)))
				ipfile = open(storecalc + '/' + 'currip.txt','w')
				ipfile.write(ipstring)
				ipfile.close()
				#a und ipstring schliessen??????
				discard = system('gnuplot %s/currip.txt > %s/logop.txt 2>&1'%(storecalc,storecalc))

				temp_dict = {}
				try:
					print('test1', file=sys.stdout)
					values = [float(item) for item in open(storecalc + '/' + 'logop.txt').readlines()[-2:]]
					print('test2 values',values, file=sys.stdout)
					k = values[0]
					print('test3 k=',k, file=sys.stdout)
					m = values[1]
					print('test4 m=',m, file=sys.stdout)
					cut_level = math.log((anstieg_linear / (m * k)) ** (1 / m))
					print('test5 cut_level',cut_level, file=sys.stdout)
					cut_distance = k * math.exp(m * cut_level)
					print('test6 cut_distance',cut_distance, file=sys.stdout)
					for pos in range(len(levels)):
							dist = scores[pos]
							lev = levels[pos]
							distance_to_suggested_cut = ((lev-cut_level) ** 2 + (dist - cut_distance) ** 2 ) ** 0.5
							temp_dict[distance_to_suggested_cut] = levels[pos]
					print('test7 after loop', file=sys.stdout)
					finally_cut = temp_dict[min(temp_dict.keys())]
					print('test8 finally_cut',finally_cut, file=sys.stdout)
				except:
					#print scores
					#print levels,levels1
					print('Cluster-Cutting is not working! (GnuPlotProblem, check determine_home_clusters in clustering.py)')
					break
			#	levels.append(cut_level)
			#	scores.append(cut_distance)
			#	levels.sort()
			#	scores.sort()
				if finally_cut == 0:		
					finally_cut_ = 0
					cutting_dict[alig_Id] = finally_cut_
					#print clusterid, 'does not waaaaaaant'
				else:
						for pos in range(len(levels)):
								if levels[pos] == finally_cut:
										nn=cut_distance+1.0/anstieg_linear*cut_level
										y_score = -1.0/anstieg_linear*finally_cut+nn
										if y_score<scores[pos]:
											finally_cut_=levels[pos-1]
										else:
											finally_cut_ = levels[pos]
										cutting_dict[alig_Id] = d_help[finally_cut_][1]
										break
				#print alig_Id, finally_cut_, cutting_dict[alig_Id]
			
		finally_cut = None
#	print 'Levels',Levels
#	print 'NodesMembers', NodesMembers
#	for mem in d_help.keys():
#		print mem, d_help[mem]
	for alig_Id in list(cutting_dict.keys()):
#		print alig_Id,
		cut = cutting_dict[alig_Id]
#		print cut, cut*maxlevel,d_help[cut][1]
		helpless = deepcopy(Levels[alig_Id][cut])
		useless = deepcopy(NodesMembers[helpless])
		#dict_of_cuttings[alig_Id] = NodesMembers[Levels[alig_Id][cut]]
		dict_of_cuttings[alig_Id] = useless
	new_dict_of_cuttings = get_new_cut(dict_of_cuttings)#************fertig
	for key in list(new_dict_of_cuttings.keys()):
		for alignment_id in new_dict_of_cuttings[key]:
			if alignment_id in membership:	break
			else:	membership[alignment_id] = new_dict_of_cuttings[key]	
	#membership['0'] = set(['0'])


#	for key in dict_of_cuttings.keys():
#		print key, dict_of_cuttings[key]
#	print ines

	cnt_clusters = 0
	dict_clusters = {}
	yes = False
	for entry in list(membership.keys()):
		clust = membership[entry]
		if cnt_clusters == 0:
			cnt_clusters += 1	
			dict_clusters[cnt_clusters] = clust
		listi = deepcopy(list(dict_clusters.keys()))	
		for clu_entry in listi:
			clu = dict_clusters[clu_entry]
			if len(clust.symmetric_difference(clu)) == 0:
				yes = False
				break
					
			else:		
				yes = True

		if yes:
			cnt_clusters += 1
			dict_clusters[cnt_clusters] = clust
			yes = False
	subclusternew = set([])
	#print ''
	#print ''
	#print ''
	for key in list(dict_clusters.keys()):
	#	print key,'key'
	#	print dict_clusters[key]
	#	print 'laenge_allg',len(dict_clusters[key])
		if len(dict_clusters[key]) > 20:
	#		print 'laenge',len(dict_clusters[key])
			for key1 in list(NodesMembers.keys()):
				#if type(key1) == int  and len((dict_clusters[key]).symmetric_difference(NodesMembers[key1])) == 0:
				if type(key1) == int  and len(dict_clusters[key])==len(NodesMembers[key1]):
					subclusternew.add(key1)
					break

	#print 'now I return'
#	for key in subclusternew:
#		print key, NodesMembers[key],'888888888888888888888888888888888888888888'
#	print subclusternew
#	print ines
	return membership,subclusternew

def clustering_main(blastinfo, bin_codiert, query, Blosum62, storecalc, log=logging):
	# print 'bin in clustering_main'

	alignment_Ids, members, distance = {}, {}, {}
	Nodes = {}
	NodesMembers = {}
	NodesScores = {}
	NodesLevels = {}
	Levels = {}

	for k in list(bin_codiert.keys()):
		alignment_Ids[k] = []
		members[k] = [k]
	
	log.info('Initial Scoring Matrix	->	Started')
	t0 = time.time()	
	clusterspecific_positioninfo_dict = store_position_info(alignment_Ids, bin_codiert)  # ************ist fertig

	#matches_dict,mismatches_dict,normalize_dict = combinations(bin_codiert,query,Blosum62)#****************ist fertig
	matches_dict, mismatches_dict, normalize_dict = combinations_test(bin_codiert, query, Blosum62, log)  # ****************ist fertig
	t1 = time.time()
	log.info('Initial Scoring Matrix ...finished: %i' % len(bin_codiert))

	matches_mod_dict = deepcopy(matches_dict)  # becomes modified during clustering
	mismatches_mod_dict = deepcopy(mismatches_dict)  # becomes modified during clustering

	for alig in list(alignment_Ids.keys()):

		NodesMembers[alig] = set([alig])
		NodesScores[alig] = 10**-20
		NodesLevels[alig] = 0
		Nodes[alig] = alig
		Levels[alig] = {0:alig}


	log.info('Clustering 		->	Started')
	t2 = time.time()
	Node_cnt = 1
	tree_Michael_format = {}
	while len(alignment_Ids) > 1:
		Node_cnt += 1
		#nextNode = 'Node' + str(Node_cnt)
		nextNode = Node_cnt
		if len(alignment_Ids)%10 == 0:
			log.info('\t\t\t\t' + str(len(alignment_Ids)))
			
		candidates, alignment_Ids, matches_mod_dict, mismatches_mod_dict, maximum = find_maximum(matches_mod_dict,
																								 mismatches_mod_dict,
																								 alignment_Ids)  # ******************ist fertig
		log.debug('next_node %i' % nextNode)

		subNode1 = candidates[0]
		subNode2 = candidates[1]

		members, distance, tree_Michael_format = store_tree_data(members, subNode1, subNode2, distance, nextNode,
																 maximum, tree_Michael_format)

		Levels, Nodes, NodesMembers, NodesScores, NodesLevels = store_level_cluster_info(candidates, Levels, Nodes,
																						 NodesMembers, NodesScores,
																						 NodesLevels, nextNode,
																						 subNode1, subNode2, maximum)  # ***********fertig

		matches_mod_dict, mismatches_mod_dict, clusterspecific_positioninfo_dict, alignment_Ids = \
			get_new_matrix(Nodes, bin_codiert, NodesMembers, candidates, nextNode, matches_mod_dict,
							mismatches_mod_dict, alignment_Ids, clusterspecific_positioninfo_dict, query,
							normalize_dict, matches_dict, mismatches_dict, Blosum62)  # ***************fertig
	t3 = time.time()
	t_initial = t1-t0
	t_clustering = t3-t2
	log.info("Time wasted for calculating the initial scoring matrix (in seconds): %i" % t_initial)
	log.info("Time wasted for clustering (in seconds): %i" % t_clustering)

	log.info('			....	Finished')
	tree_NJPLOT_format, tree_Michael_substIds = make_tree_NJPLOT(tree_Michael_format, blastinfo)
	again = True
	true_membership = {}
	allclids = []
	for xxxx in list(NodesMembers.keys()):
		if type(xxxx) == int: allclids.append(xxxx)
	clusterid = max(allclids)
	subclusters = set([clusterid])
	subclusters_done = set([])
	log.info('Cutting the Tree		->	Started')
	while len(subclusters):
		#print subclusters
		#print subclusters_done, 'those i did already>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
		subclustersnews = set([])
		for subcluster in subclusters.difference(subclusters_done):
		#	print 'now i do', subcluster
			subclusters_done.add(subcluster)
			membership, subclustersnew = determine_home_clusters(storecalc, Levels, NodesMembers, NodesScores,
																 subcluster)  # fertig
			subclustersnews.update(subclustersnew)
			for cand in list(membership.keys()):
				true_membership[cand] = membership[cand]			
		subclusters = subclustersnews
	log.info('			....	Finished')
	

	return matches_dict, distance, true_membership, tree_Michael_substIds, tree_NJPLOT_format



if __name__ == "__main__":
	
	import pickle, sys
	from Bio.Blast import NCBIXML
	from init import OPTIMA
	from alignmentinfo_initial import get_alignmentinfo as get_alignmentinfo, get_blastinfo_2 as get_blastinfo

	queryLines = open(sys.argv[1]).readlines()
	query = ''.join([line.rstrip() for line in queryLines if not line.startswith('>')])
	
	result_handle = open(sys.argv[2])
	blast_records = NCBIXML.parse(result_handle)
	for record in blast_records:  # get data always from the last record
		blast_record = record

	blast_main_info_0 = get_blastinfo(blast_record)
	bin_codiert = get_alignmentinfo(sys.argv[1],blast_record)
	
	score_matches, distance, membership, tree_Michael, tree_NJPLOT = clustering_main(blast_main_info_0, bin_codiert,
																					 query, OPTIMA, '.')
	
	
	cResultFile = open(sys.argv[1] + ".clustering",'w')
	pickle.dump((score_matches,distance,membership,tree_Michael),cResultFile)
	cResultFile.close()
