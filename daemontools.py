#!/usr/bin/env python

import os

def firstRun(cmd):
    "check for already running instance"
    # just assume first run
    firstRun = True
    # get own pid, name not to get fooled
    # needs base name to exclude program path
    myName = os.path.basename(cmd)
    myID = str(os.getpid())
    myUID = str(os.getuid())
    # print myID, myName
    # ps command magic
    cmd = "ps -C python -o uid,pid,command --no-heading"
    psOutput = os.popen(cmd).readlines()
    # print psOutput.getvalue()
    # go through python processes
    for line in psOutput:
            try:
                item = line.rstrip().split()
                uid = item[0]
                pid = item[1]
                interpreter = item[2]
                scriptName = item[3]
                # param = item[4]
            except IndexError:	 # interactive/open python session
                    continue
            # detect old instance
            # print uid, pid, interpreter, scriptName
            if uid == myUID and pid != myID and os.path.basename(scriptName) == myName:
                    firstRun = False
                    break
    return firstRun


def create_textfile(file, content):
    try:
        fh = open(file, 'w')
        # print file
        fh.write(content)
        fh.close()
        return True
    except IOError:
        return False
