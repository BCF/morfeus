var MORFEUS = MORFEUS || {};
(function($) {

	var _w, _h, _selector, _nodes, _margin = 10, _in_cache, _cache, _alive_queries = 0, _nodes_hierarchy, _vis,
		_scales, _k=[], _c=[], _p=[];

	// var _e_fetch = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi',
	// 	_e_search = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi';
	var _e_fetch = '/eutils/efetch',
		_e_search = '/eutils/esearch';

	function pauseBrowser(millis) {
    var date = Date.now();
    var curDate = null;
    do {
        curDate = Date.now();
    } while (curDate-date < millis);
}

	var _onlyLetters = /^[a-zA-Z]*$/;
	var get_initials = function(str){
		var arr = str.split(' ');
		var name = arr[0][0].toUpperCase();

		if(arr.length > 1) {	// Add exception if the species is composed of only one term
			if(_onlyLetters.test(arr[1][0])){
				name += arr[1][0].toLowerCase();
			}else{
				name += arr[2][0].toLowerCase();
			}
		} else {
			name = arr[0]
		}
		return  name;
	};

	var _get_organism_id = function(node, jobID){

		node.lineage = {
			kingdom: 'other',
		  	phylum: 'other',
		  	clazz: 'other'
		};

		if(_in_cache[node.org] == undefined)
			_in_cache[node.org] = get_initials(node.org);

		node.org_in = _in_cache[node.org];



		var ajax = {};
		if(_cache[node.org] == undefined){
			ajax.url = _e_search;
			ajax.data = {db:'taxonomy', term:node.org, job:jobID};
			ajax.success = function(xml){
		  		_cache[node.org] = $(xml).find('Id').text() || 'other';
		  		node.org_id = _cache[node.org];
		  		_ajax_check();
		  		_get_lineage(node, jobID);
		 	};
		 	ajax.error = function(){
		 		_cache[node.org] = 'other';
		  		node.org_id = _cache[node.org];
		 		_alive_queries --;
		 	};
		 	_alive_queries ++;
		 	$.ajax(ajax);
	  	}
	};

	var _get_lineage = function(node, jobID){
		var ajax = {};
		if(_cache[node.org_id] == undefined){
			ajax.url = _e_fetch;
			ajax.data = {db:'taxonomy', id:node.org_id, job:jobID};
		 	ajax.success = function(xml){
		  		var kingdom = $(xml).find('Rank').filter(function(){return $(this).text().toLowerCase() === 'kingdom'}).prev().text() || 'undefined';
		  		var phylum = $(xml).find('Rank').filter(function(){return $(this).text().toLowerCase() === 'phylum'}).prev().text() ||  'undefined';
		  		var clazz = $(xml).find('Rank').filter(function(){return $(this).text().toLowerCase() === 'class'}).prev().text() ||  'undefined';

		  		if($.inArray(kingdom, _k) == -1)
					_k.push(kingdom);

				if($.inArray(clazz, _c) == -1)
					_c.push(clazz);

				if($.inArray(phylum, _p) == -1)
					_p.push(phylum);

		  		var lineage = {
		  			kingdom: kingdom,
		  			phylum: phylum,
		  			clazz: clazz
		  		};

		  		node.lineage = lineage;
		  		_cache[node.org_id] = lineage;
		  		_ajax_check();
		  	};
		  	ajax.error = function(){
		 		_alive_queries --;
		 	};
		  	_alive_queries ++;
		  	$.ajax(ajax);
	  	}
	};

	var _ajax_check = function(){
		_alive_queries --;
		if(_alive_queries == 0){
			$(MORFEUS).trigger('got_tax_data');
		}
	};

	var _fetch_data = function(jobID){
		console.log("_nodes");
		console.log(typeof _nodes);
		for (let i = 0; i < _nodes.length; i++) {
			// console.log(_nodes[i]);
			// setTimeout(_get_organism_id(_nodes[i]), (3000));
			// pauseBrowser(150);
			_get_organism_id(_nodes[i], jobID);
		}
		// $(_nodes).each(function(i,n){
		// 	_get_organism_id(n);
		// });
		_scales.k = _k;
		_scales.c = _c;
		_scales.p = _p;
	};

	var _create_hierarchy = function(){
		var clazz = {name:'', children:[]}, kingdom = {name:'', children:[]}, phylum = {name:'', children:[]}, tmp = {};

		$(_nodes).each(function(i,n){
			var k = n.lineage.kingdom;
			var c = n.lineage.clazz;
			var p = n.lineage.phylum;

			if(tmp[k] == undefined){
				var kn = {name:k, children:[]};
				kingdom.children.push(kn);
				tmp[k] = kn;
				kn.children.push(n);
			}else{
				tmp[k].children.push(n);
			}

			if(tmp[c] == undefined){
				var cn = {name:c, children:[]};
				clazz.children.push(cn);
				tmp[c] = cn;
				cn.children.push(n);
			}else{
				tmp[c].children.push(n);
			}

			if(tmp[p] == undefined){
				var pn = {name:p, children:[]};
				phylum.children.push(pn);
				tmp[p] = pn;
				pn.children.push(n);
			}else{
				tmp[p].children.push(n);
			}
		});

		_nodes_hierarchy = {};
		_nodes_hierarchy.clazz = clazz;
		_nodes_hierarchy.phylum = phylum;
		_nodes_hierarchy.kingdom = kingdom;
	};

	var _init_graphics = function(){
		$(_selector).append('<div id="menu">Color by: <select id="color"><option>e_value</option><option>score</option></select> Resize by: <select id="size"><option>e_value</option><option selected="selected">score</option></select></div>');
		$('#menu').append(' Sort by:<select id="sort"><option>none</option><option>kingdom</option><option>phylum</option><option>class</option></select>');

		$(_selector).append('<div id="force"></div>');
		$(_selector).append('<div id="loading"></div>');

		$('#menu').hide();
		$('#color').change(function(){MORFEUS.force.set_color($(this).val());});
		$('#size').change(function(){MORFEUS.force.set_size($(this).val());});
		$('#sort').change(function(){MORFEUS.force.set_order($(this).val());});

		_scales = {};
		_scales.fill = d3.scale.ordinal().domain([0,1,2,3,4,5]).range(['#0C5AA6', '#408AD2', '#679ED2', '#FFC673', '#FFB140', '#FF9700'])

		_scales.extent = {};
		_scales.extent.score = d3.extent(_nodes, function(d){return parseFloat(d.score)});
		_scales.extent.eval = d3.extent(_nodes, function(d){return parseFloat(d.eval)});

		_scales.score = d3.scale.linear().nice().range([4,20]).domain(_scales.extent.score);
		_scales.eval = d3.scale.linear().nice().rangeRound([20,4]).domain(_scales.extent.eval);

		_scales.score_color = d3.scale.linear().nice().range([0,5]).domain(_scales.extent.score);
		_scales.eval_color = d3.scale.linear().nice().range([5,0]).domain(_scales.extent.eval);
	};

	//Public
	MORFEUS.init = function(nodes, selector, jobID, w, h){
		_selector = '#'+selector;
		_nodes = nodes;
		_w = w || 1000;
		_h = h || 500;
		_cache = [];
		_in_cache = [];
		_init_graphics();

		MORFEUS.loading.init('loading', _scales.fill, _w, _h);
		_fetch_data(jobID);

		$(MORFEUS).bind('loading_hidden',function(e){
			$('#menu').show();
			MORFEUS.force.init('force', _nodes, _scales, _w, _h);
		});

		$(MORFEUS).bind('got_tax_data',function(e){
			// _create_hierarchy();

			_scales.kingdom = {};
			_scales.kingdom.x = d3.scale.ordinal().rangePoints([_margin, _w-_margin]).domain(_k);
			_scales.kingdom.y = d3.scale.ordinal().rangePoints([_margin, _h-_margin]).domain(_k);

			_scales.clazz = {};
			_scales.clazz.x = d3.scale.ordinal().rangePoints([_margin, _w-_margin]).domain(_c);
			_scales.clazz.y = d3.scale.ordinal().rangePoints([_margin, _h-_margin]).domain(_c);

			_scales.phylum = {};
			_scales.phylum.x = d3.scale.ordinal().rangePoints([_margin, _w-_margin]).domain(_p);
			_scales.phylum.y = d3.scale.ordinal().rangePoints([_margin, _h-_margin]).domain(_p);

			$(_k).each(function(i,n){
				_nodes.push({name:n, type:'kingdom', lineage:{kingdom:n}});
			});

			$(_c).each(function(i,n){
				_nodes.push({name:n, type:'class', lineage:{clazz:n}});
			});

			$(_p).each(function(i,n){
				_nodes.push({name:n, type:'phylum', lineage:{phylum:n}});
			});

			MORFEUS.loading.hide();

		});
	}



})(jQuery);
