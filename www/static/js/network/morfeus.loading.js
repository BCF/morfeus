var MORFEUS = MORFEUS || {};
(function($) {
	
	var _nodes, _force, _selector, _vis;
	
	var self = {
	
		init: function(selector, color_scale, w, h){
			_selector = '#'+selector;
			
			_nodes = d3.range(200).map(function() { return {radius: Math.random() * 12 + 4}; });
			
			_force = d3.layout.force()
			    .gravity(0.05)
			    .charge(function(d, i) { return i ? 0 : -2000; })
			    .nodes(_nodes)
			    .size([w, h]);
		    
		    var root = _nodes[0];
				root.radius = 0;
				root.fixed = true;
			
			_force.start();
			
			_vis = d3.select(_selector).append("svg:svg")
				.attr("width", w)
	    		.attr("height", h);
	    		
	    	_vis.selectAll("circle")
		    	.data(_nodes.slice(1))
		  		.enter().append("svg:circle")
		    	.attr("r", function(d) { return d.radius - 2; })
		    	.style("fill", function(d, i) { return color_scale(i % 5); });
	    	
	    	_vis.append("text")
				.text("LOADING ...").attr("x", (w / 2))
				.attr("y", h/2)
				.attr("font-family","Verdana, Geneva, sans-serif")
				.attr("text-anchor", "middle") 
				.attr("font-size","50px")
				.style("fill", "#AAAAAA"); 	
	    	
	    	_force.on("tick", function(e) {
				var q = d3.geom.quadtree(_nodes),
			    	i = 0,
			    	n = _nodes.length;
			
			  	while (++i < n) {
			    	q.visit(collide(_nodes[i]));
			  	}
			
			  	_vis.selectAll("circle")
			    	.attr("cx", function(d) { return d.x; })
			      	.attr("cy", function(d) { return d.y; });
			});
			
			_vis.on("mousemove", function() {
				var p1 = d3.svg.mouse(this);
			  	root.px = p1[0];
			  	root.py = p1[1];
			  	_force.resume();
			});
			
			function collide(node) {
				  var r = node.radius + 16,
				      nx1 = node.x - r,
				      nx2 = node.x + r,
				      ny1 = node.y - r,
				      ny2 = node.y + r;
				  return function(quad, x1, y1, x2, y2) {
				    if (quad.point && (quad.point !== node)) {
				      var x = node.x - quad.point.x,
				          y = node.y - quad.point.y,
				          l = Math.sqrt(x * x + y * y),
				          r = node.radius + quad.point.radius;
				      if (l < r) {
				        l = (l - r) / l * .5;
				        node.x -= x *= l;
				        node.y -= y *= l;
				        quad.point.x += x;
				        quad.point.y += y;
				      }
				    }
				    return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
				  };
				}
		},
		hide: function(){
			
			var on_hide_fnc = function(d,i){
				if(i==0){ 
					$(_selector).hide(); 
					_force.stop();
					$(MORFEUS).trigger('loading_hidden');	
				}
			}
			
			_vis.selectAll("circle").transition().each("end", on_hide_fnc).duration(1000)
				.style("opacity", 0)
				.attr("r", 0);
		},
		show: function(){
			$(_selector).show();
			_force.start();
			_vis.selectAll("circle").transition().duration(1000)
				.style("opacity", .9)
				.attr("r", function(d){return d.radius;});
		}
	};
	
	MORFEUS.loading = self;
	
})(jQuery);