var MORFEUS = MORFEUS || {};
(function($) {
	
	var _nodes, _force, _selector, _vis, _scales, _order='none', _size='score', _color='e_value', _h, _w, _score_leyend, _eval_leyend, 
		_circles, _score_color, _eval_color;
	
	var _mouseover = function(d,i){
		
		var msg = '<div class="title">'+d.org+'</div><div class="rule"></div><div class="rest">Gene Name: '+d.name
	    		+'<br> Class: '+d.lineage.clazz+'<br> Phylum: '+d.lineage.phylum+'</div>'
	    		+'<div class="vcontainer"><span class="value">E-value: '+d.eval+'</span><span class="change">Score: '+d.score+'</span></div>';
    	
    	MORFEUS.tooltip.show(msg, d3.mouse(this));
	}
	
	var add_scale = function(d,i){
		var extent, r, title;
		
		_score_leyend = _vis.append('g').attr('id','score_leyend');
		extent = _scales.extent.score;
		
		title = _score_leyend.append('text')
			.attr("dx", 5)
		    .attr("dy", (_h/2)-70)
		    .attr("text-anchor", "right")
		    .attr("font-family","Verdana, Geneva, sans-serif")
		    .attr("font-size","10px")
		    .style("fill", "#AAAAAA");
		    
		title.append('tspan').text("Circles are sized");
		    
		title.append('tspan')
			.text("according to the score.")
			.attr('x',5)
			.attr('dy',10);
		
		r = _scales.score(extent[0]);
		_score_leyend.append("circle")
        	.attr('r', r)
        	.attr('class',"circle-legend")
        	.attr('cx', 30)
        	.attr('cy', (_h/2)-r);
        	
        _score_leyend.append("text")
		    .attr("dx", 70)
		    .attr("dy", (_h/2)-2*r)
		    .attr("text-anchor", "left")
		    .attr("font-family","Verdana, Geneva, sans-serif")
		    .attr("font-size","10px")
		    .text(extent[0])
		    .style("fill", "#AAAAAA");
		
		_score_leyend.append("line")
			.attr('x1', 30)
			.attr('x2', 67)
			.attr('y1', (_h/2)-2*r)
			.attr('y2', (_h/2)-2*r)
			.attr('stroke', "#AAAAAA")
			.attr('stroke-width', .3);
        
        r = _scales.score(extent[1]);	
        _score_leyend.append("circle")
        	.attr('r', r)
        	.attr('class',"circle-legend")
        	.attr('cx', 30)
        	.attr('cy', (_h/2)-r);
        	
         _score_leyend.append("text")
		    .attr("dx", 70)
		    .attr("dy", (_h/2)-2*r)
		    .attr("text-anchor", "left")
		    .attr("font-family","Verdana, Geneva, sans-serif")
		    .attr("font-size","10px")
		    .text(extent[1])
		    .style("fill", "#AAAAAA");
		    
		 _score_leyend.append("line")
			.attr('x1', 30)
			.attr('x2', 67)
			.attr('y1', (_h/2)-2*r)
			.attr('y2', (_h/2)-2*r)
			.attr('stroke', "#AAAAAA")
			.attr('stroke-width', .3);
			
			
		_eval_leyend = _vis.append('g').attr('id','eval_leyend').style("opacity", 0);
		extent = _scales.extent.eval;
		
		title = _eval_leyend.append('text')
			.attr("dx", 5)
		    .attr("dy", (_h/2)-70)
		    .attr("text-anchor", "right")
		    .attr("font-family","Verdana, Geneva, sans-serif")
		    .attr("font-size","10px")
		    .style("fill", "#AAAAAA");
		    
		title.append('tspan').text("Circles are sized");
		    
		title.append('tspan')
			.text("according to the e-value.")
			.attr('x',5)
			.attr('dy',10);
		
		r = _scales.eval(extent[0]);
		_eval_leyend.append("circle")
        	.attr('r', r)
        	.attr('class',"circle-legend")
        	.attr('cx', 30)
        	.attr('cy', (_h/2)-r);
        	
        _eval_leyend.append("text")
		    .attr("dx", 70)
		    .attr("dy", (_h/2)-2*r)
		    .attr("text-anchor", "left")
		    .attr("font-family","Verdana, Geneva, sans-serif")
		    .attr("font-size","10px")
		    .text(extent[0])
		    .style("fill", "#AAAAAA");
		
		_eval_leyend.append("line")
			.attr('x1', 30)
			.attr('x2', 67)
			.attr('y1', (_h/2)-2*r)
			.attr('y2', (_h/2)-2*r)
			.attr('stroke', "#AAAAAA")
			.attr('stroke-width', .3);
        
        r = _scales.eval(extent[1]);	
        _eval_leyend.append("circle")
        	.attr('r', r)
        	.attr('class',"circle-legend")
        	.attr('cx', 30)
        	.attr('cy', (_h/2)-r);
        	
         _eval_leyend.append("text")
		    .attr("dx", 70)
		    .attr("dy", (_h/2)-2*r)
		    .attr("text-anchor", "left")
		    .attr("font-family","Verdana, Geneva, sans-serif")
		    .attr("font-size","10px")
		    .text(extent[1])
		    .style("fill", "#AAAAAA");
		    
		 _eval_leyend.append("line")
			.attr('x1', 30)
			.attr('x2', 67)
			.attr('y1', (_h/2)-2*r)
			.attr('y2', (_h/2)-2*r)
			.attr('stroke', "#AAAAAA")
			.attr('stroke-width', .3);
		
		_score_color = _vis.append('g').attr('id','score_color').style("opacity", 0);
		
		title = _score_color.append('text')
			.attr("dx", 5)
		    .attr("dy", (_h/2)+25)
		    .attr("text-anchor", "right")
		    .attr("font-family","Verdana, Geneva, sans-serif")
		    .attr("font-size","10px")
		    .style("fill", "#AAAAAA");
		    
		title.append('tspan').text("Circles are colored");
		    
		title.append('tspan')
			.text("according to the score.")
			.attr('x',5)
			.attr('dy',10);
		
		for(var i=0; i<6; i++){
			_score_color.append('rect')
				.attr('x', 20)
				.attr('y', _h/2 + 50 +i*(25))
				.attr('width', 20)
				.attr('height', 20)
				.style("fill", _scales.fill(i));
				
			_score_color.append("text")
			    .attr("dx", 50)
			    .attr("dy", _h/2+65+i*(25))
			    .attr("text-anchor", "right")
			    .attr("font-family","Verdana, Geneva, sans-serif")
			    .attr("font-size","10px")
			    .text('~ '+Math.round(_scales.score_color.invert(i)*10)/10)
			    .style("fill", "#AAAAAA");
		}
		
		_eval_color = _vis.append('g').attr('id','eval_color');
		
		title = _eval_color.append('text')
			.attr("dx", 5)
		    .attr("dy", (_h/2)+25)
		    .attr("text-anchor", "right")
		    .attr("font-family","Verdana, Geneva, sans-serif")
		    .attr("font-size","10px")
		    .style("fill", "#AAAAAA");
		    
		title.append('tspan').text("Circles are colored");
		    
		title.append('tspan')
			.text("according to the e-value.")
			.attr('x',5)
			.attr('dy',10);
		
		for(var i=0; i<6; i++){
			_eval_color.append('rect')
				.attr('x', 20)
				.attr('y', _h/2 + 50 +i*(25))
				.attr('width', 20)
				.attr('height', 20)
				.style("fill", _scales.fill(i));
				
			_eval_color.append("text")
			    .attr("dx", 50)
			    .attr("dy", _h/2+65+i*(25))
			    .attr("text-anchor", "right")
			    .attr("font-family","Verdana, Geneva, sans-serif")
			    .attr("font-size","10px")
			    .text('~ '+Math.round(_scales.eval_color.invert(i)*10)/10)
			    .style("fill", "#AAAAAA");
		}
	}
	
	var handle_leyend = function(){
		if(_order != 'none'){
			_score_leyend.transition().duration(1000)
				.style("opacity", 0);
				
			_eval_leyend.transition().duration(1000)
				.style("opacity", 0);
				
			_score_color.transition().duration(1000)
				.style("opacity", 0);
			
			_eval_color.transition().duration(1000)
				.style("opacity", 0);
		}else{
			if(_size=='score'){
				_score_leyend.transition().duration(1000)
					.style("opacity", 1);
				
				_eval_leyend.transition().duration(1000)
					.style("opacity", 0);
			}else{
				_score_leyend.transition().duration(1000)
					.style("opacity", 0);
				
				_eval_leyend.transition().duration(1000)
					.style("opacity", 1);
			}
			
			if(_color=='score'){
				_score_color.transition().duration(1000)
					.style("opacity", 1);
				
				_eval_color.transition().duration(1000)
					.style("opacity", 0);
			}else{
				_score_color.transition().duration(1000)
					.style("opacity", 0);
				
				_eval_color.transition().duration(1000)
					.style("opacity", 1);
			}
		}
	}
	
	var self = {
	
		init: function(selector, nodes, scales, w, h){
			_selector = '#'+selector;
			_nodes = nodes;
			_scales = scales;
			_w = w;
			_h = h;
			
			MORFEUS.tooltip.init();
			
			_vis = d3.select(_selector).append("svg:svg")
				.attr("width", w)
	    		.attr("height", h);
	    	
	    	_force = d3.layout.force()
				.nodes(_nodes)
			    .size([w, h])
			    .charge(function(d, i) { return d.type ? -10 : -200; })
			    .start();
			    		
	    	var node = _vis.selectAll(".node")
		    	.data(_nodes)
		  		.enter().append("g")
		    	.call(_force.drag);
		    
		    _circles = node.filter(function(d){return d.type ? null : this}).append('circle')	
		    	.attr("r", function(d) { return scales.score(d.score); })
		    	.attr("cx", function(d) { return d.x; })
	      		.attr("cy", function(d) { return d.y; })
		    	.style("fill", function(d, i) { return _scales.fill(Math.round(_scales.eval_color(d.eval))); });
		    	
		    	
		    	
		    	node.filter(function(d){return d.type ? null : this}).on("mouseover", _mouseover)
		    	.on("mouseout", function(d,i){MORFEUS.tooltip.hide()});
		    	
		   var l = node.filter(function(d){return d.type ? null : this}).append('text')
		    	.attr("dy", ".3em")
		      	.attr("text-anchor", "middle")
		      	.attr("font-family","Verdana, Geneva, sans-serif")
		      	.attr("font-size","8px")
		      	.text(function(d) { return d.org_in })
		      	.style("fill", "#FFFF");
		      	
		      	//node.filter(function(d){return d.type ? null : this}).append('title').text(function(d) { return "hola" });
		    
		    var t = node.filter(function(d){return d.type ? this : null}).append("text")
		    	.attr("dx", 12)
		      	.attr("dy", ".35em")
		      	.attr("text-anchor", "middle")
		      	.attr("font-family","Verdana, Geneva, sans-serif")
		      	.attr("font-size","10px")
		      	.text(function(d) { return d.name })
		      	.style("fill", "#AAAAAA");
		      	
		    add_scale();
		    	
		    _force.on("tick", function(e) {
				var k = .1 * e.alpha;
				
				_force.start()
			  	_nodes.forEach(function(o, i) {
			    	
			    	var x_foci = o.type ? 2*w : w/2;
			    	if(_order == 'class' && o.lineage.clazz){
			    		x_foci = isNaN(_scales.clazz.x(o.lineage.clazz)) ? w/2 : _scales.clazz.x(o.lineage.clazz);
			    	}else if(_order == 'kingdom' && o.lineage.kingdom){
			    		x_foci = isNaN(_scales.kingdom.x(o.lineage.kingdom)) ? w/2 : _scales.kingdom.x(o.lineage.kingdom);
			    	}else if(_order == 'phylum' && o.lineage.phylum){
			    		x_foci = isNaN(_scales.phylum.x(o.lineage.phylum)) ? w/2 : _scales.phylum.x(o.lineage.phylum);
			    	}
			    	
			    	o.y += (h/2 - o.y) * k;
			    	o.x += (x_foci - o.x) * k;
			  	});
				
			  	_circles.attr("cx", function(d) { return d.x; })
			      	.attr("cy", function(d) { return d.y; });
			    
			    l.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

			    t.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

			});
			
			d3.select(_selector).on("click", function() {
				nodes.forEach(function(o, i) {
			    	o.x += (Math.random() - .5) * 40;
			    	o.y += (Math.random() - .5) * 40;
			  	});
			});
		},
		set_size: function(val){
			_size = val;
			
			handle_leyend();
			
			if(val=='score'){
				_circles.transition().duration(1000)
					.attr("r", function(d){return _scales.score(d.score)});
			}else if(val == 'e_value'){
				_circles.transition().duration(1000)
					.attr("r", function(d){return _scales.eval(d.eval);});
			}
		},
		set_color: function(val){
			_color = val;
			
			handle_leyend();
			
			if(val=='score'){
				_circles.transition().duration(1000)
					.style("fill", function(d, i) { return  _scales.fill(Math.round(_scales.score_color(d.score)));});
			}else if(val == 'e_value'){
				_circles.transition().duration(1000)
					.style("fill", function(d, i) { return _scales.fill(Math.round(_scales.eval_color(d.eval)));});
			}
		},
		set_order: function(val){
			_order = val;
			handle_leyend();
		}
	};
	
	MORFEUS.force = self;
})(jQuery);