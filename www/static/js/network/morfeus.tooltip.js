var MORFEUS = MORFEUS || {};
(function($) {
	
	var _x=0, _y=0, _m, _h, _selector;
	
	var self = {
		init: function(){
			
			selector = 'tooltip',
			$('body').append('<div id="'+selector+'"></div>');
			
			_selector = '#'+selector;
			$(_selector).hide();
		},
		show: function(msg, mouse){
			_x = mouse[0];
			_y = mouse[1];
			
			$(_selector).html(msg);
			
			$(_selector).css('top',(_y + 20)+'px');
			$(_selector).css('left',(_x + 30)+'px');
			$(_selector).css('zIndex', 200);
			$(_selector).css('position', 'absolute');
			
			$(_selector).show();
		},
		hide: function(){
			$(_selector).hide();
		}
	}
	
	MORFEUS.tooltip = self;
})(jQuery);
