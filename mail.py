#! /usr/bin/env python

import smtplib
from init import MAILUSER, MAILSERVER, MAILDOMAIN, MAILPORT,  MAILPW, MAILSEC
from Crypto.Cipher import DES

des = DES.new(MAILSEC, DES.MODE_ECB)


def send(to, msg):

	body = "From: %s\r\nTo: %s\r\nSubject: Morfeus job finished\r\n\r\n%s" % (MAILUSER+'@'+ MAILDOMAIN, to, msg)
	#print body
	
	server = smtplib.SMTP(MAILSERVER, "5870")
	server.set_debuglevel(1)
	server.ehlo()
	server.starttls()
	server.ehlo()
	server.login(MAILUSER, des.decrypt(MAILPW))	
	server.sendmail(MAILUSER+'@'+ MAILDOMAIN, to, body)
	server.quit()


if __name__ == '__main__':
	send(MAILUSER, "Morfeus test message")
