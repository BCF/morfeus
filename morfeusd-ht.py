#!/usr/bin/python

import optparse, os, pickle, sys, time, urllib.request, urllib.parse, urllib.error, zipfile, glob # , signal
import HTMLTemplate
from pickleshare_save import PickleShareDB
from init import MORFEUS_USER, MORFEUS_DIR, STORE_SEQIN, STORE_BLASTOUT, STORE_RESULT, DATA_PATH, STORE_DATA, FILTER, MATRIX, \
		 CBS, LIMIT, NBLASTDATABASE, CGIJOBS, JOB_DATETIME, OPTIMA, DEF_EVALUE, HOME, NAME, PORT, EVC_MAX_ITER, EVC_TOL, \
		 NETWORK_SIZE, EXCLUSION_RATIO #PARSEJOBS, BLASTJOBS
from p0_start import morfeus_run
from daemontools import firstRun
from tree import TreeImage
from network.drawing_mod import build_ortholog_network
from network.scoring_diff_alg import OrthologNetwork
from mail import send

RE_RUN = False

## future signal handling
#paused = False # default running
#def handler(sigNum, stackframe):
#	global paused
#	if paused:
#		paused = False
#		main()
#	else:
#		return 	# already running
#	
#signal.signal(signal.SIGCONT, handler)		# connect handler
## write pid for sending signals
#pid_file = sys.argv[0][:-3]+".pid"
#file = open(pid_file,"w")
#file.write(`os.getpid()`)
#file.close()

def _sort_by_evalue(item1,item2):
	"helper function for dict sort"
	x = float(item1['evalue']) - float(item2['evalue'])
	if x < 0:
	        return -1
	elif x > 0:
	    return 1
	else:
	    return 0
	

def _sort_by_score(item1,item2):
	"helper function for dict sort"
	x = float(item1['score']) - float(item2['score'])
	if x < 0:
	        return 1
	elif x > 0:
	    return -1
	else:
	    return 0


def main(options):
	
	global folder, matrix, cleanup, jobDB #,paused
	data_path = DATA_PATH.split('/')[-1] # remove "www/"
	# create DATA_PATH + STOREJOBS folder if not there	
	try:	
		os.mkdir(STORE_DATA)
		#os.mkdir(STOREJOBS)
	except OSError:
		print("folder exist: ", STORE_DATA) #, STOREJOBS
		
	while True:
		print("read job list.." + time.ctime(), file=sys.stderr)
		# jobList = glob.glob(JOB_DIR+'/*')
		
		for jobID in list(cgijobDB.keys()):
			
			seqID, timeStamp = jobID.split('-')	# sequence id			
			evalue, querySeq, filter, matrix, cbs, limit, database, email = cgijobDB[jobID]
			print(querySeq)
			if querySeq:
				seqID = None 			# send seq job
			results = morfeus_run(userID = jobID, queryID = seqID, querySeq = querySeq, matrix = OPTIMA, e_value = evalue, skip=options.skip, HighThroughPut = options.batch)			
			#print results
			print("END_OF_JOB")
			#jobFile.close()
			result = {}

			if not options.batch:

				result['ortholog'], result['paralog'], result['uncertain'], blast, bb, common, tree_Michael, done = results
				
				# dendrogram generation
				# done tree
				dHits=[]
				for id in done:
					seqID = list(blast['0'][id][0])
					dHits.extend(seqID)
				treeImg = TreeImage(tree_Michael, dHits, [])
				treeFile = os.path.join(os.getcwd(),HOME,jobID+"_tree_done"+".png")
				treeImg.save(treeFile)
			
				# final tree
				try:
					oHits = [bb[o] for o in result['ortholog']]  # get ortholog names
				except:
					oHits = 'Unknown'
				pHits = []		# get paralog names, skip missing paralog '2'
				for p in result['paralog']:
				    pHits.append(bb.get(p))
				treeImg = TreeImage(tree_Michael, oHits, pHits)
				treeFile = os.path.join(os.getcwd(),HOME,jobID+"_tree_final"+".png")
				treeImg.save(treeFile)
			
			else:

				result['ortholog'], result['paralog'], result['uncertain'], blast, bb, common, done = results
				
				
			# exclude 1st round
			
			bb_excluded_more_than_included = []						# hits which are more excluded than acceptable/best							# list of ids to leaveout
			for hit in bb:
				id_exclude = 0.0
				id_acceptable_best = 0.0		
				for id in common:					
					if hit in common[id]['exclude']:				# no check for isoform 1st round
						# ncbi_hit = blast['0'][hit][5]				# check for isoform in acceptable/best							
						# for isoform in (common[id]['acceptable'].union(common[id]['best'])):
						#	if ncbi_hit==blast['0'][isoform][5]:
							#print "Not excluded as isoform in accp/best found: ", isoform
						#		break					# no exclude++ if isoform is there
						# else:
								id_exclude+=1.0					# exclude++ as no isoform
					elif hit in common[id]['acceptable'] or hit in common[id]['best']:
								id_acceptable_best+=1.0
					#print "id: %s %s ex: %i  in: %i" % (hit, bb[hit], int(id_exclude), int(id_acceptable_best))
				try:
					hit_ratio = id_exclude/(id_acceptable_best+id_exclude)
				except ZeroDivisionError:
					hit_ratio = 1.0 #EXCLUSION_RATIO				# hit gets excluded for shure
				if hit_ratio <= 0.5 or hit=='0': #EXCLUSION_RATIO or hit=='0':		# not excluded, '0' never gets excluded
					# bb_new[hit] =  bb[hit]
					pass
				else:
					bb_excluded_more_than_included.append(hit)					# put in exclude list
						
						
			# exclude 2nd round	
			
			bb_new = {}										# bb after exclusion
			to_exclude = {}										# o/p/u exclude lists
			for s in ("ortholog", "paralog", "uncertain"):
				to_exclude[s] = [] 								# list of ids to exclude
				for hit in result[s]:
					id_exclude = 0.0
					id_acceptable_best = 0.0		
					for id in common:
						if id in bb_excluded_more_than_included: continue
						if hit in common[id]['exclude']:
							ncbi_hit = blast['0'][hit][5]				# check for isoform in acceptable/best							
							for isoform in (common[id]['acceptable'].union(common[id]['best'])):
								if ncbi_hit==blast['0'][isoform][5]:
									#print "Not excluded as isoform in accp/best found: ", isoform
									break					# no exclude++ if isoform is there
							else:
								id_exclude+=1.0					# exclude++ as no isoform
						elif hit in common[id]['acceptable'] or hit in common[id]['best']:
							id_acceptable_best+=1.0
					print("id: %s %s ex: %f  in: %f" % (hit, bb[hit], id_exclude, id_acceptable_best))
					try:
						hit_ratio = id_exclude/(id_acceptable_best+id_exclude)
					except ZeroDivisionError:
						hit_ratio = EXCLUSION_RATIO					# hit gets excluded for shure
					if hit_ratio < EXCLUSION_RATIO or hit=='0':				# not excluded, '0' never gets excluded
						bb_new[hit] =  bb[hit]
					else:
						to_exclude[s].append(hit)					# put in exclude list
			
			
			
			
						
			#print zip(bb_new.keys(), [blast['0'][id][5] for id in bb_new])
			
			
			
		
			
			
			
			
			
			for s in ("ortholog", "paralog", "uncertain"):						# check for excluded isoforms	
				for id in to_exclude[s]:
					ncbi_id = blast['0'][id][5]						# get gene id for potentially excluded isoform
					# print "%s %s" % (bb[id], ncbi_id)
					for hit in bb_new:							# find not excluded isoform
						ncbi_hit = blast['0'][hit][5]					# get gene id for new hit
						if ncbi_id==ncbi_hit:
							bb_new[id] = bb[id]					# re-add isoforms for not excluded new hit
							print("Re-add excluded isoform: %s %s" % (id, bb[id]))
							break						
					else:
						result[s].remove(id)						# exclude ortholog/paralog/uncertain hit
						print("Excluded: %s %s" % (id, bb[id]))
							
			pickle.dump(bb_new, file(os.path.join(STORE_RESULT %jobID, "bb_storageIds"),'w')) 	# re-save bb_storageIds for scoring
						
				
				
				
			# scoring
			cPath = os.path.join(STORE_RESULT %jobID, "common")
			nPath = os.path.join(STORE_RESULT %jobID, "bb_storageIds")	
			nw_score = OrthologNetwork(rootNode='0', connectionsPath=cPath, nodeIdPath=nPath, algorithm="eigenvector", max_iter=EVC_MAX_ITER, tol=EVC_TOL) 
			nw_score.loadNetwork()
			nw_score.createNetwork()
			nw_score.do_scoring()
			# nw_score.scoreNetwork()
			#oNW.safeScoredNetwork()
			
			myDot = []
			
			if options.graph:
				# network generation
				print("Generating Network :")
				build_ortholog_network(myDot,result['ortholog'],result['paralog'],result['uncertain'], blast, bb, common, nw_score=nw_score)
				nwFile = os.path.join(os.getcwd(),HOME,jobID+"_network")
				cmd = os.popen("circo -Tcmapx -o %s -Tpng -o %s" % (nwFile + ".map", nwFile + ".png"), 'w')
				myDot.append("}")
				if len(myDot) < 20000:
					#close network
					cmd.write(''.join(myDot))
					cmd.close()
					# .map -> .html
					nwHTML = open(nwFile + ".html",'w')
					print('''<html>
								<head><title>%s</title></head>
								<body>
								       <img src="%s" usemap="#Ortholog">
								%s
								</body>
							</html>'''	% (jobID, os.path.basename(nwFile)+".png", file(nwFile+".map").read()), file=nwHTML)
				
					nwHTML.close()
					print("Network generated successfully !")
				else:
					nwHTML = open(nwFile + ".html",'w')
					print('''<html>
								<head><title>%s</title></head>
								<body>
									%s
									</body>
							</html>'''	% ("Did not generate Network !".title(), " The network comprises of too many connections.Please use the sif files for network analysis in Cytoscape !"), file=nwHTML)
				
					nwHTML.close()
			
			#mySif = []
			file1 = os.path.join(os.getcwd(),HOME,jobID+"_Main_cytoscape"+".sif") # interaction file for cytoscape
			mainfile = open(file1,'w')
			file2 =  os.path.join(os.getcwd(),HOME,jobID+"_Attribute_cytoscape"+".sif") #node attribute file for cytoscape
			nodefile = open(file2,'w')
			sif, mainsif, nodesif  = build_ortholog_network(myDot,result['ortholog'],result['paralog'],result['uncertain'], blast, bb, common, nw_score=nw_score)
			mainfile.write(mainsif)
			nodefile.write(nodesif)
				
			print("Zipping the sif files :")
			myCyto = os.path.join(os.getcwd(),HOME,jobID+"_cytoscape"+".zip")
			zout = zipfile.ZipFile(myCyto, "w")
			zout.writestr(file1, mainsif)
			zout.writestr(file2, nodesif)
			
			zout.close()
			mainfile.close()
			nodefile.close()
			print("Cytoscape files zipped successfully !")
			
			page = HTMLTemplate.HTMLTemplate("www/morfeusd.html")		
			page.Set('job', jobID)
			page.Set('seq', jobID.split('-')[0])	
			if not options.batch:	page.Set('tree', os.path.basename(treeFile))
			if options.graph: page.Set('network', os.path.basename(nwFile)+".html")
			page.Set('Cyoscape_Files', os.path.basename(myCyto))
			
			result_all = []
			for resultType in ('ortholog', 'paralog', 'uncertain'):
				result_all.extend(result[resultType])		# merge all results
			
			output = []				
			for item in result_all:
				outputRow = {}
				#try:
				#    ref = bb[item]
				#except KeyError:
				    #print >> sys.stderr, "Miss id in bb_storageIds: ", item, resultType
				synonym, species, evalue, description, sequence_identity, ncbi_id = blast['0'][item]
				for id in synonym:
					ref_id = id
				outputRow['ref']= ref_id
				outputRow['species']= species
				outputRow['evalue']= evalue
				outputRow['desc']= description
				outputRow['blastout'] = urllib.parse.quote(os.path.join(data_path, jobID, os.path.split(STORE_BLASTOUT)[-1], ref_id))
				outputRow['inseq'] = urllib.parse.quote(os.path.join(data_path, jobID, os.path.split(STORE_SEQIN)[-1], ref_id))
				try:
					outputRow['score'] = "%.2f" % nw_score.score[item]
				except KeyError:		# '0' has no score
					outputRow['score'] = "1.00"
				output.append(outputRow)
															
			output.sort(_sort_by_score)
			page.Set("result", output)
				
				
			outFile = open(os.path.join(os.getcwd(),HOME,jobID+".html"),'w')
			outFile.write(page.Output())
			outFile.close()
						
			print("Remove job " + jobID, file=sys.stderr)
			del cgijobDB[jobID]
			
			if email:
				send(email, "Your result is available at http://%s:%s/%s.html" % (NAME, PORT, jobID))			
		else:
			print("Wait for CGI job..", file=sys.stderr)
		sys.exit()
		# time.sleep(1)
		# paused = True
		# signal.pause()

cgijobDB = PickleShareDB(CGIJOBS) # no need for path correction
print(list(cgijobDB.keys()))

if not firstRun(sys.argv[0]):
	print(sys.stderr, "CGI instance already exists")
	sys.exit(-1)
	
parser = optparse.OptionParser("morfeusd.py [options] id1 [id2 .. idn]" )
parser.add_option("-c", "--cleanup", dest="cleanup", action="store_true", default=False, help="(C)leanup morfeusd queue")
parser.add_option("-e", "--e-value", dest="evalue", action="store", default=DEF_EVALUE, help="(E)-value")
parser.add_option("-f", "--file", dest="input", action="store_true", default=False, help="Input id (F)ile")
parser.add_option("-g", "--graph", dest="graph", action="store_true", default=False, help="Draw network (G)raph ")
parser.add_option("-r", "--re-run", dest="rerun", action="store_true", default=False, help="(R)u-run existing job")
parser.add_option("-s", "--skip", dest="skip", action="store_true", default=False, help="(S)kip clustering (re-run)")
parser.add_option("-n", "--no-clustering", dest="batch", action="store_true", default=False, help="(N)o clustering)")
(options, args) = parser.parse_args()
print(options, args)
#sys.exit()


if options.cleanup:
	cgijobDB.clear()
	if not args:
		sys.exit(0)

if not args:				# daemon mode
	pass
	
else:
	if options.input:			# id file provided
		id_file = args[0]
		if os.path.exists(id_file):
			queries = [line.rstrip() for line in open(id_file).readlines()]
			for query in queries:
				jobID = query + '-' +  time.strftime(JOB_DATETIME)
				cgijobDB[jobID] = [options.evalue, None, FILTER, MATRIX, CBS, LIMIT, NBLASTDATABASE, MORFEUS_USER]
				print(jobID)
		else:
			parser.error("Couldn't find file: ", id_file)
			sys.exit(-1)
	elif options.rerun or options.skip:			# Re-run with existing jobID
		jobID = args[0]
		seq = None
		cgijobDB[jobID] = [options.evalue, seq, FILTER, MATRIX, CBS, LIMIT, NBLASTDATABASE, MORFEUS_USER]
	else:
		queries = args[0:]			# ids provided
		seq = None
		for query in queries:
			jobID = query + '-' +  time.strftime(JOB_DATETIME)
			cgijobDB[jobID] = [options.evalue, seq, FILTER, MATRIX, CBS, LIMIT, NBLASTDATABASE, MORFEUS_USER]
			print(jobID)
print("Morfeusd started....")
main(options)

