import pickle
import time
import operator
from copy import deepcopy
from init import USE_CPP



def excluded_(dict, dictionary, exclude):
	""" take all the excluded of dict (like ortholog or paralog) and put them in a set """
	excl=set([])
	for d in dict:
		#print 'derr fehlt?',d
		excl.update(dictionary[d][exclude])
	
	return excl


def find_them(ortholog, uncertain_ortholog, paralog, dictionary, scores1, scores, exclude, done):

	# print("In python ortholog in entry :", ortholog)
	schalter = None
	all_score_cand = set([])
	all_best = set([])
	all_h = set([])

	for ortho in list(ortholog):  # for each ortho
		besthit_cands = list(dictionary[ortho]['best'])  # selection of best candidates
		hit_cands = list(dictionary[ortho]['acceptable'])  # those acceptable too
		cands = list(done.intersection(set(besthit_cands + hit_cands)))  # Take those already done in the bestHit and hit candidates

		#scores1_besthit_cands = map(lambda cand: scores1[(str(min(int(ortho),int(cand))),str(max(int(ortho),int(cand))))],cands)#clustering_score
		scores1_besthit_cands = [scores1[cand] for cand in cands]  # Fill with score1 of candidates
		scores_besthit_cands = [-1*scores[(str(min(int(ortho), int(cand))), str(max(int(ortho), int(cand))))] for cand in cands]  # initial clustering score
		score_cand = set([(scores1_besthit_cands[pos], scores_besthit_cands[pos], cands[pos], ortho) for pos in range(len(scores_besthit_cands))])
		all_score_cand.update(score_cand)
		all_best.update(besthit_cands)
		all_h.update(hit_cands)

	# for para in list(paralog):
	# 	besthit_paras = list(dictionary[para]['best'])
	# 	hit_paras = list(dictionary[para]['acceptable'])
	# 	paras = besthit_paras + hit_paras

	all_score_cand = sorted((x[0], x[1], x[2], x[3]) for (x) in list(all_score_cand))

	for cand in all_score_cand:

		# print(cand[2], cand[3], cand[2] not in ortholog, cand[2] not in paralog)
		symmetricalbest_ortholog, symmetricalbest_paralog = None, None

		if cand[2] not in ortholog and cand[2] not in paralog:
			cand_best_orthos = dictionary[cand[2]]['best'].intersection(ortholog)
			cand_acceptable_orthos = dictionary[cand[2]]['acceptable'].intersection(ortholog)
			cand_best_paras = dictionary[cand[2]]['best'].intersection(paralog)
			ortho_best_cand = set([])
			para_best_cand = set([])

			# if reciproque between possible ortho add at best cand
			for ortho in cand_best_orthos:
				if cand[2] in dictionary[ortho]['best']: 
					ortho_best_cand.add(ortho)
					#print cand,ortho,'vorlaeufig added to ortho_best_cand'

			for para in cand_best_paras:
				if cand[2] in dictionary[para]['best']: 
					para_best_cand.add(para)
					#print cand,para,'vorlaeufig added to para_best_cand'

			if len(ortho_best_cand) > 0 and cand[2] not in excluded_(ortholog, dictionary, exclude) and \
					len(ortholog.intersection(dictionary[cand[2]]['exclude'])) == 0:
				symmetricalbest_ortholog = cand[2]
				#print cand[2],'added to symmetricalBestortholog'

			if len(para_best_cand) > 0 and cand[2] not in excluded_(paralog, dictionary, exclude) and \
					len(paralog.intersection(dictionary[cand[2]]['exclude'])) == 0:
				symmetricalbest_paralog = cand[2]
				#print cand[2],'added to symmetricalBestparalog'
			#print 'orthologe bis hierher',ortholog
			#print 'paraloge bis hierher', paralog

		if symmetricalbest_paralog and symmetricalbest_ortholog:

			#print symmetricalbest_ortholog, 'koennte beides sein'
			treffer_all = list(ortho_best_cand) + list(para_best_cand)
			#scores1_treffer = map(lambda treff: scores1[(str(min(int(treff),int(symmetricalbest_ortholog))),str(max(int(treff),int(symmetricalbest_ortholog))))],treffer_all)
			scores1_treffer = [scores1[treff] for treff in treffer_all]
			scores_treffer = [scores[(str(min(int(treff), int(symmetricalbest_ortholog))), str(max(int(treff), int(symmetricalbest_ortholog))))] for treff in treffer_all]
			#scores_treffer = map(lambda treff: scores[treff],treffer_all)
			score_treffer = set([(scores1_treffer[pos], scores_treffer[pos], symmetricalbest_ortholog, treffer_all[pos]) for pos in range(len(scores_treffer))])
			all_score_treffer = sorted((x[0], x[1], x[2], x[3]) for (x) in list(score_treffer))
			all_score_treffer.reverse()

			if '0' in ortho_best_cand:
				#print symmetricalbest_ortholog, 'symmetrical ortholog, has at least query as best hit'
				print("python in if 0 orthoBestCand")
				ortholog.add(symmetricalbest_ortholog)
				try:	uncertain_ortholog.remove(symmetricalbest_ortholog)
				except:	pass
				schalter = 'yes'
				break

			elif '0' in cand_acceptable_orthos:
				#print symmetricalbest_paralog, 'symmetrical paralog, does not recognize query as best hit'
				paralog.add(symmetricalbest_paralog)
				try:	uncertain_ortholog.remove(symmetricalbest_paralog)
				except:	pass

			else:
				sum_of_scores_ortho = 0
				sum_of_scores_para = 0
				no_of_ortho = 0
				no_of_para = 0
				for hit in all_score_treffer:
					if hit[3] in ortholog:
						no_of_ortho += 1
						sum_of_scores_ortho += hit[1]
					if hit[3] in paralog:
						no_of_para += 1
						sum_of_scores_para += hit[1]
				# if sum_of_scores_ortho/no_of_ortho > sum_of_scores_para/no_of_para:	print 'I predict it should be an ortholog',
				# else:	print 'I predict it should be a paralog',
						
				if all_score_treffer[0][3] in paralog:
					#print symmetricalbest_paralog, 'symmetrical paralog as it is more similar to an paralog, in terms of scoring'
					paralog.add(symmetricalbest_paralog)
					try:	uncertain_ortholog.remove(symmetricalbest_paralog)
					except:	pass
				if all_score_treffer[0][3] in ortholog:
					print("python in score_treffer[0][3]")
				#	print symmetricalbest_ortholog, 'symmetrical ortholog as it is more similar to an ortholog in terms of scoring'
					ortholog.add(symmetricalbest_ortholog)
					try:	uncertain_ortholog.remove(symmetricalbest_ortholog)
					except:	pass
					schalter = 'yes'
					break

		elif symmetricalbest_ortholog:
			#print symmetricalbest_ortholog, 'truely symmetrical ortholog'
			# print(f"python in symBestOrtho {symmetricalbest_ortholog}")
			ortholog.add(symmetricalbest_ortholog)
			try:	uncertain_ortholog.remove(symmetricalbest_ortholog)
			except:	pass
			schalter = 'yes'
			break

		elif symmetricalbest_paralog:
			#print symmetricalbest_paralog, 'truely symmetrical paralog'
			try:	uncertain_ortholog.remove(symmetricalbest_paralog)
			except:	pass
			paralog.add(symmetricalbest_paralog)

		if cand[2] not in ortholog and cand[2] not in paralog:

			if len(cand_best_orthos) + len(cand_acceptable_orthos) >= 1 and len(cand_best_paras) == 0:
				uncertain_ortholog.add(cand[2]) 


	# 	if cand[2] in excluded_(ortholog, dictionary, exclude) or len(ortholog.intersection(dictionary[cand[2]]['exclude'])) > 0:
	# 		cand_hits_paralog = dictionary[cand[2]]['best'].intersection(paralog)
	# 		if len(cand_hits_paralog) > 0: paralog.add(next_ortholog)
	# 		elif (next_ortholog in all_h or next_ortholog in all_best) and (cand[3] in dictionary[cand[2]]['best'] or cand[3] in dictionary[cand[2]]['acceptable']):
	# 			uncertain_ortholog[next_ortholog]=[]#this can be done as soon as all orthologs are assigned
	# all_score_cand = set([])
	# all_best = set([])
	# all_h = set([])
	#
	# for para in list(paralog.keys()):
	# 	print('hallo')
	# 	print(para)
	# 	besthit_cands = dictionary[para]['best']
	# 	scores_besthit_cands = [scores[str(min(int(para), int(cand))) + '*' + str(max(int(para), int(cand)))] for cand
	# 							in besthit_cands]
	# 	score_cand = set([(scores_besthit_cands[pos], besthit_cands[pos]) for pos in range(len(scores_besthit_cands))])
	# 	all_score_cand.update(score_cand)
	# all_score_cand = sorted(all_score_cand, key=operator.itemgetter(0))
	# all_score_cand.reverse()
	# for cand in all_score_cand:
	# 	if cand[1] not in list(ortholog.keys()) and cand[1] not in excluded_(paralog, dictionary, exclude) and len(
	# 			set(ortholog.keys()).intersection(dictionary[cand[1]]['exclude'])) == 0:
	# 		next_ortholog = cand[1]
	# 		paralog[next_ortholog] = []
	# 		schalter = 'yes'
	# 		break
	# for para in paralog.keys():
	# 	print 'hallo'
	# 	print para
	# 	besthit_cands = dictionary[para]['best']
	# 	scores_besthit_cands = map(lambda cand: scores[str(min(int(para),int(cand)))+'*'+str(max(int(para),int(cand)))],besthit_cands)
	# 	score_cand = set([(scores_besthit_cands[pos],besthit_cands[pos]) for pos in range(len(scores_besthit_cands))])
	# 	all_score_cand.update(score_cand)
	# all_score_cand = sorted(all_score_cand, key=operator.itemgetter(0))
	# all_score_cand.reverse()
	# for cand in all_score_cand:
	# 	if cand[1] not in ortholog.keys() and cand[1] not in excluded_(paralog,dictionary,exclude) and len(set(ortholog.keys()).intersection(dictionary[cand[1]]['exclude'])) == 0:
	# 		next_ortholog = cand[1]
	# 		paralog[next_ortholog]=[]
	# 		schalter = 'yes'
	# 		break
	return ortholog, uncertain_ortholog, paralog, schalter


def orthology(dictionary, scores1, scores, ortholog_, uncertain_ortholog_, paralog_, done, job_id=""):
	orthol = deepcopy(ortholog_)
	exclude = 'exclude'
	schalter = 'yes'

	if USE_CPP:
		from lib import orthology
		orthol_cpp = list(orthol)
		done_cpp = list(done)
		dict_cpp = deepcopy(dictionary)
		for k in list(dictionary.keys()):
			dict_cpp[k]['best'] = list(dictionary[k]['best'])
			dict_cpp[k]['acceptable'] = list(dictionary[k]['acceptable'])
			dict_cpp[k]['exclude'] = list(dictionary[k]['exclude'])
		scores1_cpp = {}
		to_save = len(scores1) - len(scores)
		for i in range(to_save):
			scores1_cpp[str(i)] = scores1[str(i)]
		paral_cpp = list(paralog_)
		uncertain_orthol_cpp = list(uncertain_ortholog_)
		ortholog, uncertain_ortholog, paralog = orthology.schalter(orthol_cpp, uncertain_orthol_cpp,
		                                                           paral_cpp, dict_cpp, scores1_cpp, scores, done_cpp)

	else:

		while schalter:
			uncertain_orthol = deepcopy(uncertain_ortholog_)
			paral = deepcopy(paralog_)
			schalter = None
			ortholog, uncertain_ortholog, paralog, schalter = find_them(orthol, uncertain_orthol, paral, dictionary,
																			scores1, scores, exclude, done)

	#implement here uncertain_orthology_check!
	print('orthologues to here are', ortholog)
	print('uncertain orthologous up to here are', uncertain_ortholog)
	print('paraloge  up to here are', paralog)

	return set(ortholog), set(uncertain_ortholog), set(paralog)


def query_paralog(candidates, ortholog_):

	paralog = set([])
	query_identical_sequences = set(['0'])
	species_query = candidates['0'][1]
	for cand in list(candidates.keys()):
		identity = candidates[cand][4]
		# print species_query,cand,identity,candidates[cand][1],
		if candidates[cand][1] == species_query and cand != '0':
			# print 'fullfilles'
			if identity < 0.98 and identity > 0.8:
				paralog.add(cand)
			# 	print 'added to paralo'
			# else:	print 'not added to paralog'
			if identity >= 0.98:
				query_identical_sequences.add(cand)
		# 		print 'added to ortho'
		# 	else:	print 'not added to ortho'
		# else:
		# 	print 'does not fullfilles'
	try:	paralog = set([str(min([int(x) for x in list(paralog)]))])
	except:	pass
	#print paralog
	#print ortholog_
	#print ines
	return paralog, query_identical_sequences


def potential_candidates(ortholog, common_hits, scores1,scores,membership,done):
	candidates = set([])
	tobe_parsed = set([])
	exclude = set([])
	include = set([])
	common_best = set([])
	ortho = list(ortholog)

	for pos in range(0,len(ortho)):
		# print '##########################################################'
		# print ortho[pos],'mitglieder',membership[ortho[pos]]
		candidates.update(membership[ortho[pos]])
		exclude.update(common_hits[ortho[pos]]['exclude'])
	# 	include.update(common_hits[ortho[pos]]['best'])
	# 	include.update(common_hits[ortho[pos]]['acceptable'])
	#
	# if len(ortholog) == 1 or len(common_best.difference(set(['0']))) == 0 or common_best.intersection(set(ortho)) < 2:	candidates.update(include)
	# else:	candidates.update(common_best)
	# candidates.update(include)

	candidates = candidates.difference(done)
	# excl. test candidates = candidates.difference(exclude)
	if len(ortholog) == 1 and len(candidates) == 0:
		pos = 0
		include.update(common_hits[ortho[pos]]['best'])
		candidates.update(include)
		candidates = candidates.difference(done)
		## excl. test: candidates = candidates.difference(exclude)
			
	for cand in candidates:

		score_ = [(scores1[cand], -1*scores[str(min(int(ortho), int(cand))), str(max(int(ortho), int(cand)))]) for ortho in ortholog]
		tobe_parsed.add((score_[0][0], score_[0][1], cand))
	
	for pos in range(0, len(ortho)-1):
			for pos1 in range(pos+1,len(ortho)):
				common_best.update(common_hits[ortho[pos]]['best'].intersection(common_hits[ortho[pos1]]['best']))

	common_best = common_best.difference(done)
	# excl. test: common_best = common_best.difference(exclude)
	tobe_parsed_evt = set([])		
	for cand in common_best:
		# print "candidate",cand
		# for ortho in ortholog:
		#
		# 	print "lrtholog",ortho
		# 	nodl = (str(min(int(ortho),int(cand))),str(max(int(ortho),int(cand))))
		# 	print nodl
		# 	print "scores,nodl",scores[nodl]
		score_ = [(scores1[cand], -1*scores[str(min(int(ortho), int(cand))), str(max(int(ortho), int(cand)))]) for ortho in ortholog]

		tobe_parsed_evt.add((score_[0][0], score_[0][1], cand))

	tobe_parsed.update(tobe_parsed_evt)

	tobe_parsed = sorted((x[0], x[1], x[2]) for (x) in list(tobe_parsed))
	# print '##########################################################################################'
	# for elef in tobe_parsed:
	# 	print elef
	# print '##########################################################################################'
	#tobe_parsed.reverse()
	tobe_parsed = [x[2] for x in tobe_parsed]
	
	# scores = clustering_minor(ortholog,candidates,bin_codiert,query,Matrix)
	# tobe_parsed = sorted((x[1],x[2],x[0]) for (x) in scores)
	# tobe_parsed.reverse()
	# tobe_parsed = map(lambda x: x[2],tobe_parsed)

	print('in dieser reihenfolge zu bearbeiten', tobe_parsed)
	return tobe_parsed


if __name__ == "__main__":

	def extract_all_scores(ortholog, dictionary, scores1, scores):
		all_score_cand = set([])
		for ortho in list(ortholog):  # for each ortho
			besthit_cands = list(dictionary[ortho]['best'])  # selection of best candidates
			hit_cands = list(dictionary[ortho]['acceptable'])  # those acceptable too
			cands = list(done.intersection(
				set(besthit_cands + hit_cands)))  # Take those already done in the bestHit and hit candidates

			# scores1_besthit_cands = map(lambda cand: scores1[(str(min(int(ortho),int(cand))),str(max(int(ortho),int(cand))))],cands)#clustering_score
			scores1_besthit_cands = [scores1[cand] for cand in cands]  # Fill with score1 of candidates
			scores_besthit_cands = [-1 * scores[(str(min(int(ortho), int(cand))), str(max(int(ortho), int(cand))))] for
									cand in cands]  # initial clustering score
			score_cand = set([(scores1_besthit_cands[pos], scores_besthit_cands[pos], cands[pos], ortho) for pos in
							  range(len(scores_besthit_cands))])
			all_score_cand.update(score_cand)

		return all_score_cand

	prev = pickle.load(open("compare/before_ortho_171", "rb"))
	common_hits, distance, score_matches, ortholog_, uncertain_ortholog_, paralog_, done, job_id = [e for e in prev]

	cpp = False
	if cpp:

		ortholog, uncertain_ortholog, paralog = orthology(common_hits, distance, score_matches, ortholog_,  uncertain_ortholog_, paralog_, done)
	else:
		scores1 = {}
		to_save = len(distance) - len(score_matches)
		for i in range(to_save):
			scores1[str(i)] = distance[str(i)]
		orthol = deepcopy(ortholog_)
		t = time.time()
		all_score_cand = extract_all_scores(orthol, common_hits, distance, score_matches)
		print(f"Take {time.time() - t}s")
		# schalter = 'yes'
		# while schalter:
		# 	uncertain_orthol = deepcopy(uncertain_ortholog_)
		# 	paral = deepcopy(paralog_)
		# 	ortho, uncertain_ortho, para, schalter = find_them(orthol, uncertain_orthol, paral, common_hits, distance, score_matches, 'exclude', done)
		# ortholog = ortho
		# uncertain_ortholog = uncertain_orthol
		# paralog = para
	# orthology(dictionary, scores1, scores, ort)
	# print(len(ortholog))
	# print(sorted(list(ortholog)), sorted(list(uncertain_ortholog)), sorted(list(paralog)))


