
import logging, re, uuid
from math import ceil
from Bio import Entrez
from time import sleep
from pickleshare import PickleShareDB
from pathos.multiprocessing import Pool
from init import BLAST_TYPE, MORFEUS_USER, MAILSERVER, MAILUSER, NO_CPU, API_KEY, EUTILSJOBS, EUTILS_RESULT


Entrez.email = MAILUSER
Entrez.api_key = API_KEY
eutilsjobDB = PickleShareDB(EUTILSJOBS)
eutilsresultDB = PickleShareDB(EUTILS_RESULT)


def get_alignmentinfo(queryfile, blast_record):
	"""Create the bits map"""

	bin_codiert = {}								# initial bit alignment dict
	query_len = blast_record.query_letters

	def _get_bits(hsp, bits):
		gaps = 0			   							# count gap positions for hsp
		qs = hsp.query_start - 1 						# index 1 -> 0
		for i, aa_query in enumerate(hsp.query):
			if aa_query == '-':	   						# gap in query
				gaps += 1									# compensate for gap
			else:
				aa_match = hsp.match[i] 
				if aa_match != ' ': 		# aa match
					bits[qs + i - gaps] = [1]# set bit at position qs+i omitting gaps in query


	for a, alignment in enumerate(blast_record.alignments):
		bit_initial = [[0]] * query_len 					# empty bit list for a single alignment	
		hsp0 = alignment.hsps[0]						# always take best alignment
		_get_bits(hsp0, bit_initial)		
		hsp_extension = [0]							# put hsp0 in extension list

		for h, hsp in enumerate(alignment.hsps[1:]):				# go through all other hsps, change to enumerate(list, start=x) for Python 2.6 
			for i in hsp_extension:						# check for independent, non-overlapping hsps
				if hsp.query_end < alignment.hsps[i].query_start or hsp.query_start > alignment.hsps[i].query_end:
					if hsp.sbjct_end < alignment.hsps[i].sbjct_start or hsp.sbjct_start > alignment.hsps[i].sbjct_end:
						_get_bits(hsp, bit_initial)
						hsp_extension.append(h+1) 				# h=0 -> hsps[1]
						break
					else:
							break
				else:
						break

		bin_codiert[str(a)] = bit_initial # alignment key is non-numeric
	return bin_codiert


def get_blastinfo(blast_record, add_ncbi=False, log=logging):
	""" """

	def get_info_worker(list_pos):
		for pos in list_pos:

			title = alignments[pos].title
			columns = title.split("|")
			if BLAST_TYPE == 'blastp':
				accession = columns[1]  # Get the accession number of the match sequence
			if BLAST_TYPE == 'psiblast':
				try:
					accession = columns[5]  # difference in the XML file of PSI_BLAST
				except IndexError:
					accession = columns[3]  # if xml of query exists from previous run as BLAST_TYPE == 'blastp'
			if "pir||" in title:
				ID_row = columns[
					6]  # exceptional example : Hit_def>gi|2137044|pir||I46884 T-cell.. (EMPTY space for ID)
				ID_line = ID_row.split(' ')
				accession = ID_line[0]
				print("Exception occured !")

			evalue = alignments[pos].hsps[0].expect

			identities1 = alignments[pos].hsps[0].identities
			identities2 = alignments[pos].hsps[0].align_length

			try:
				identity = 1.0 * identities1 / identities2  # Get the identity %

			except TypeError:  # workaround for identities1 -> (None, None) as query -> "XXXXXXXXXXXXXXXXXXXXXXXXX"
				identity = 0

			db_IDs = re_IDs.findall(title)
			db_IDs.extend(re_pirf.findall(title))  # add pir/prf ids
			id_list = [item.split('|')[-1] for item in db_IDs]  # split db-id

			# id_list.remove(accession)
			id_list.insert(0, accession)
			m = re_desc.search(title)

			if m:
				# print m.group()
				desc, species = m.group()[:-1].rsplit('[', 1)  # split desc-species, strip ']'
			# split because of "[[x]] .. [species]" case
			else:
				desc, species = title.split('|')[-1], 'Unknown Species'

			if add_ncbi:
				for attempt in range(5):
					try:
						waiting = True
						eutilsjobID = str(uuid.uuid4())
						# create the entry in todo_eutils.db
						eutilsjobDB[eutilsjobID] = ""
						# Wait for the authorization
						while waiting:
							sleep(0.05)
							jobs_authorization = eutilsresultDB.keys()
							for jobs_approval in jobs_authorization:
								if eutilsjobID in eutilsresultDB[jobs_approval]:
									result = Entrez.esearch(db="gene", term=accession.split('.')[0],
												retmode="xml").read()  # No refeseq version number!

									# gets internal gene ID from NCBI, added retmode = 'xml'. without this, program terminated with psiblast
									m = re.search(r'<Id>\d+</Id>', result)
									ncbi_id = m.string[m.start() + 4:m.end() - 5]
									waiting = False # break loop
									# break
					# print("ncbi_id:", ncbi_id)
					except AttributeError:
						log.warn("No NCBI Id for %s found" % accession)
						ncbi_id = None
						break
					except:
						continue
					else:
						log.debug("Retreiving NCBI GeneID %s for accession %s" % (ncbi_id, accession))
						break
			else:
				ncbi_id = None

			hit_dict[str(pos)] = [set([accession]), species, evalue, desc, identity, ncbi_id]
		return hit_dict

	re_IDs = re.compile("[a-z]{2,3}\|[^\|]+")				# RE all "db|id" pairs
	re_pirf = re.compile("[pirf]{3}\|\|\w+")				# RE for "pir||id" and "prf||id"
	re_desc = re.compile("[^\|]*\[.*?\]")					# RE for description + species"| .. [species]"
	
	hit_dict = {}
	alignments = blast_record.alignments

	# if no need for esearch doesn't parallelize to not lose time with thread spawn
	if not add_ncbi:
		get_info_worker([i for i in range(len(alignments) - 1)])
		return hit_dict
	else:
		if int(NO_CPU) <= 10:
			cpu = NO_CPU
		else:
			cpu = 10
		pool = Pool(cpu)

		# Define the part of the job done by each core
		j = 0
		list_id_for_process = []
		number_by_process = ceil(len(alignments) / cpu)

		for process in range(cpu):
			list_id_for_process.append([i for i in range(j, j + int(number_by_process))])
			j += number_by_process
		# if the last one contains
		if list_id_for_process[-1][-1] >= len(alignments):
			list_id_for_process[-1] = [i for i in range(list_id_for_process[-1][0], len(alignments))]

		# launch the multiprocesing
		t = pool.map(get_info_worker, list_id_for_process)

		# join the list of dict obtain in the multiprocessing
		for d in t:
			hit_dict.update(d)

		return hit_dict

####################################################################################################################################################
if __name__ == "__main__":
	
	import os, sys, Bio
	from Bio.Blast import NCBIXML

	result_handle = open(sys.argv[1])
	blast_records = NCBIXML.parse(result_handle)
	for record in blast_records:
		blast_record = record
	print("Blast_record ready !")
	#sample = NP_sample
	print(get_blastinfo(blast_record, add_ncbi=True))
	
	queryfile = open('queryfile','w')
	print(get_aligninfo(queryfile, blast_record))
