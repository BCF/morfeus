from math import log,exp
from os import system
from sets import Set
from copy import *

 
def store_position_info(alignment_Ids,bin_codiert):#********************ist fertig

	clusterspecific_positioninfo = {}

	for alig_id in list(alignment_Ids.keys()):
		clusterspecific_positioninfo[alig_id] = bin_codiert[alig_id]
	
	return clusterspecific_positioninfo


def combinations(bin_codiert,query,Blosum62):#************ist fertig

	id_liste = list(bin_codiert.keys())
	id_liste.sort()
	max_matches_per_alignment, score_matches, score_mismatches, common_2alignments, uncommon_2alignments, normalize_dict = {},{},{},{},{},{}
		
	for alig_id in id_liste:
		numb = 0
		for pos in range(len(query)):
			if bin_codiert[alig_id][pos][0] == 1:
				numb += Blosum62[query[pos]]
		max_matches_per_alignment[alig_id] = numb
	
	x = 0
	for alig1_id in id_liste[0:-1]:
		x += 1
		
		for alig2_id in id_liste[x:]:
					normalize = (max_matches_per_alignment[alig1_id] * max_matches_per_alignment[alig2_id]) ** 0.5
					position_cluster_values = []
					pair_id = str(min(int(alig1_id),int(alig2_id))),str(max(int(alig1_id),int(alig2_id)))
					normalize_dict[pair_id] = normalize
					
					score_matches[pair_id],score_mismatches[pair_id],common_2alignments[pair_id],uncommon_2alignments[pair_id] = count_common_positions(Blosum62,query,normalize,bin_codiert[alig1_id],bin_codiert[alig2_id])
	return score_matches,score_mismatches,common_2alignments,uncommon_2alignments,normalize_dict,max_matches_per_alignment

# combinations = record(combinations)


def count_common_positions(Blosum62,query,normalize,a1,a2):#**********ist fertig
	
	encode,unencode = [],[]
	match_score,mismatch_score = 0,0
	for p in range(len(query)):
		if a1[p][0] == 1 and a2[p][0] == 1:
			match_score += Blosum62[query[p]]
			encode.append('1')
			unencode.append('0')
		elif (a1[p][0] == 1 and a2[p][0] == 0) or  (a2[p][0] == 1 and a1[p][0] ==0):
			mismatch_score += Blosum62[query[p]]
			encode.append('0')
			unencode.append('1')
		else:
			encode.append('0')
			unencode.append('0')
	#match_score = (match_score * 1.0 ) / (mismatch_score + 10**-20)
	match_score = (match_score * 1.0 / normalize) / (mismatch_score + 10**-20)
	
	return match_score,mismatch_score,encode,unencode


def find_maximum(matches_mod_dict,mismatches_mod_dict,alignment_Ids):#***************ist fertig
	inv_matches,maximum = inv_dict(matches_mod_dict)
	candi = inv_matches[maximum]	
	mini = mismatches_mod_dict[candi[0]]
	key = candi[0]
	
	for pair in candi:
		if mismatches_mod_dict[pair] < mini:
			mini = mismatches_mod_dict[pair]
			key = pair
	print((key,maximum))
		
	key1,key2 = key
	key_pair = [key1,key2]
	del alignment_Ids[key1]
	del alignment_Ids[key2]
	del matches_mod_dict[key]
	del mismatches_mod_dict[key]
	
	return key_pair,alignment_Ids,matches_mod_dict,mismatches_mod_dict,maximum


def store_level_cluster_info(candidates,Levels,Nodes,NodesMembers,NodesScores,NodesLevels,nextNode,subNode1,subNode2,maximum):

	Nodes[nextNode] = (subNode1,subNode2)
	NodesMembers[nextNode] = NodesMembers[subNode1].union(NodesMembers[subNode2])
	NodesScores[nextNode] = 1/maximum
#	NodesLevels[nextNode] = max(NodesLevels[subNode1],NodesLevels[subNode2]) + 1
	NodesLevels[nextNode] = len(NodesMembers[nextNode])-1

	for alig in NodesMembers[nextNode]:
		Levels[alig][NodesLevels[nextNode]] = nextNode



	return Levels,Nodes,NodesMembers,NodesScores,NodesLevels



def make_tree_NJPLOT(tree_Michael_format,blastinfo):

	Blast_Id = 0
	Species = 1
	EV = 2
	Protein = 3	
	store_NJPLOT_names = {}
	tree_Michael_substIds = {}
	for Id in list(blastinfo.keys()):
		store_NJPLOT_names[Id] = "'%s %s'"%(list(blastinfo[Id][Blast_Id])[0],blastinfo[Id][Species]) # quote blast hits + species to make drawgram happy
	nodes = list(tree_Michael_format.keys())
	nodes.sort()
	for node in nodes:
		subnodes = tree_Michael_format[node][0]
		subnode1 = subnodes[0]
		subnode2 = subnodes[1]
		distance = tree_Michael_format[node][1]
		subnode1_NJ = store_NJPLOT_names[subnode1]
		subnode2_NJ = store_NJPLOT_names[subnode2]
		store_NJPLOT_names[node] = '(%s:%s,%s:%s)'%(subnode1_NJ,distance,subnode2_NJ,distance)
		last_node = store_NJPLOT_names[node]
		if type(subnode1) == str:
			subnode1 = subnode1_NJ.strip("'") # remove quotes from blast hits in tree_Michael
		if type(subnode2) == str:
			subnode2 = subnode2_NJ.strip("'")
		subnodes = (subnode1,subnode2)
		tree_Michael_substIds[node] = [subnodes,distance]
	return last_node + ';',tree_Michael_substIds # make drawgram happy


def store_tree_data(members,cand0,cand1,distance,nextNode,maximum,tree_Michael_format):

	tree_Michael_format[nextNode] = [(cand0,cand1),1/maximum]
	members_cand0 = members[cand0]
	members_cand1 = members[cand1]
	members[nextNode] = members_cand0 + members_cand1
	for m0 in members_cand0:
		for m1 in members_cand1:
			distance[(str(min(int(m0),int(m1))),str(max(int(m0),int(m1))))] = maximum
	return members, distance,tree_Michael_format


def get_new_matrix(NodesMembers,candidates,nextNode,matches_mod_dict,mismatches_mod_dict,alignment_Ids,clusterspecific_positioninfo_dict,common_2alignments,uncommon_2alignments,query,normalize_dict,mismatches_dict,Blosum62):#**************fertig
	
	new_matrix,	new_matrix_mismatch, nextNodespecific_positioninfo_dict = {},{},{}
	
	nextNodespecific_positioninfo_dict[nextNode] = gemeinsam(clusterspecific_positioninfo_dict[candidates[0]],clusterspecific_positioninfo_dict[candidates[1]])
	
	clusterspecific_positioninfo_dict.update(nextNodespecific_positioninfo_dict)
	
	for element in list(alignment_Ids.keys()):
		match_score_new,mismatch_score_new = get_new_distance(NodesMembers,nextNode,element,clusterspecific_positioninfo_dict,common_2alignments,uncommon_2alignments,query,normalize_dict,mismatches_dict,Blosum62)

		for candidate in candidates:
			matches_mod_dict = remove_distance(candidate,element,matches_mod_dict)
			mismatches_mod_dict = remove_distance(candidate,element,mismatches_mod_dict)
		new_matrix[(nextNode,element)] = match_score_new
		new_matrix_mismatch[(nextNode,element)] = mismatch_score_new
	
	alignment_Ids[nextNode] = {}
	matches_mod_dict.update(new_matrix)
	mismatches_mod_dict.update(new_matrix_mismatch)

	return matches_mod_dict,mismatches_mod_dict,clusterspecific_positioninfo_dict,alignment_Ids


def inv_dict(matrix):

	invdistance = {}
	for key in list(matrix.keys()):
			distance = matrix[key]
			if distance in invdistance: invdistance[distance].append(key)
			else: invdistance[distance] = [key]

	distances = list(invdistance.keys())
	distances.sort()

	return invdistance,distances[-1]


def gemeinsam(liste1,liste2):#**************fertig

	common_liste = []
	for element in range(len(liste1)):
		common_liste.append(liste1[element] + liste2[element])
			
	return common_liste	


def get_new_distance(NodesMembers,nextNode,element,clusterspecific_positioninfo_dict,common_2alignments,uncommon_2alignments,query,normalize_dict,mismatches_dict,Blosum62):#*************fertig
	
	pos_value, pos_value_m = [],[]
	
	for pos in range(len(query)):
		cluster_cnt1 = clusterspecific_positioninfo_dict[nextNode][pos].count(1) * 1.0
		element_cnt1 = clusterspecific_positioninfo_dict[element][pos].count(1) * 1.0
		cluster_cnt0 = clusterspecific_positioninfo_dict[nextNode][pos].count(0) * 1.0
		element_cnt0 = clusterspecific_positioninfo_dict[element][pos].count(0) * 1.0
		all_posibilities = len(clusterspecific_positioninfo_dict[nextNode][pos]) * len(clusterspecific_positioninfo_dict[element][pos])
		pos_value.append((cluster_cnt1 * element_cnt1) / all_posibilities)
		#pos_value.append((cluster_cnt1 * element_cnt1 + cluster_cnt0 * element_cnt0) / all_posibilities)
		pos_value_m.append((cluster_cnt0 * element_cnt1 + cluster_cnt1 * element_cnt0) / all_posibilities)
	match_score,mismatch_score = 0,0
	for alig1_id in NodesMembers[nextNode]:
		for alig2_id in NodesMembers[element]:
			key1 = (alig1_id,alig2_id)
			key2 = (alig2_id,alig1_id)
			try:
				seq = common_2alignments[key1]
				unseq = uncommon_2alignments[key1]
				normalize = normalize_dict[key1]
			except KeyError:
				seq = common_2alignments[key2]
				unseq = uncommon_2alignments[key2]
				normalize = normalize_dict[key2]
			matches,mismatches = 0,0
			for p in range(len(seq)):
				if seq[p] == '1':
					matches += Blosum62[query[p]] * pos_value[p]
					
				if unseq[p] == '1':
					mismatches += Blosum62[query[p]] * pos_value_m[p]
			#matches = (matches * 1.0 ) / (mismatches + 10**-20)
			matches = (matches * 1.0 / normalize) / (mismatches + 10**-20)
			match_score += matches#???
			mismatch_score += mismatches#???
	n = len(NodesMembers[nextNode]) * len(NodesMembers[element])
	match_score = match_score * 1.0 / n
	mismatch_score = mismatch_score * 1.0 / n
			
	return match_score,mismatch_score


def remove_distance(candidate,element,matrix):#************fertig

	key1 = (candidate,element)
	key2 = (element,candidate)
	try:	del matrix[key1]
	except KeyError:	del matrix[key2]
	
	return matrix



def get_new_cut(dict_of_cuttings):#*************fertig
	print(dict_of_cuttings)
	liste = list(dict_of_cuttings.keys())
	for pos in range(0,len(liste)-1):
		for pos1 in range(pos+1,len(liste)):
			x,y = liste[pos],liste[pos1]
			if len(dict_of_cuttings[x].intersection(dict_of_cuttings[y]))!=0 and len(dict_of_cuttings[x].symmetric_difference(dict_of_cuttings[y]))!=0:
				dict_of_cuttings[x].update(dict_of_cuttings[y])	
				dict_of_cuttings[y].update(dict_of_cuttings[x])	
	return dict_of_cuttings


def determine_home_clusters(storecalc,Levels,NodesMembers,NodesScores,clusterid):

	cutting_dict,dict_of_cuttings,membership = {},{},{}
	
#	for alig_Id in Levels.keys():

	print(clusterid)
	print((NodesMembers[clusterid]))
	for alig_Id in NodesMembers[clusterid]:
		d_help={}
		schalter = 'yes'
		while schalter:
				schalter = None
				levelnode = Levels[alig_Id]
				file = 'file'

				levels = deepcopy(list(levelnode.keys()))
				for level in list(levelnode.keys()):
					if level > clusterid:
						levels.remove(level)
				levels1 = levels
				scores = [NodesScores[levelnode[x]] for x in levels]

				maxlevel=max(levels)
				maxdist=max(scores)
				levels = [x*1.0/maxlevel for x in levels]
				scores = [x*1.0/maxdist for x in scores]
				for pos in range(len(levels)):
					d_help[levels[pos]] =[scores[pos],levels1[pos]]
				levels.sort()
				scores = [d_help[x][0] for x in levels]
				x0 = levels[0]
				x1 = levels[-1]
				x2 = levels[0]
				y0 = scores[0]
				y1 = scores[-1]
				y2 = scores[0]

				m = (log((1.0 * y2) / (1.0 * y1))) / (x2 - x1)
				k = y1 / (exp(m * x1))
				anstieg_linear = (y0 - y1) / (x0 - x1)
				
				fh = open(storecalc + '/'+file,"w")		
				for pos in range(len(levels)):
						fh.write(str(levels[pos]) + ' ' + str(scores[pos]) + '\n')
				fh.close() 
				
				ipstring = open(storecalc + '/' + 'gnuplotip.txt').read().replace("inputfile",storecalc + '/' + file).replace('k_value','k=%s'%(str(k))).replace('m_value','m=%s'%(str(m)))
				ipfile = open(storecalc + '/' + 'currip.txt','w')
				ipfile.write(ipstring)
				ipfile.close()
				#a und ipstring schliessen??????
				discard = system('gnuplot %s/currip.txt > %s/logop.txt 2>&1'%(storecalc,storecalc))

				temp_dict = {}
				try:
						values = [float(item) for item in open(storecalc + '/' + 'logop.txt').readlines()[-2:]]
						k = values[0]
						m = values[1]
						cut_level = log((anstieg_linear / (m * k)) ** (1 / m))
						cut_distance = k * exp(m * cut_level)
						for pos in range(len(levels)):
								dist = scores[pos]
								lev = levels[pos]
								distance_to_suggested_cut = ((lev-cut_level) ** 2 + (dist - cut_distance) ** 2 ) ** 0.5
								temp_dict[distance_to_suggested_cut] = levels[pos]
						finally_cut = temp_dict[min(temp_dict.keys())]
				except: print('Cluster-Cutting is not working! (GnuPlotProblem, check determine_home_clusters in clustering.py)')
			#	levels.append(cut_level)
			#	scores.append(cut_distance)
			#	levels.sort()
			#	scores.sort()
				if finally_cut == 0:		
					finally_cut_ = 0
					cutting_dict[alig_Id] = finally_cut_
				else:
						for pos in range(len(levels)):
								if levels[pos] == finally_cut:
										nn=cut_distance+1.0/anstieg_linear*cut_level
										y_score = -1.0/anstieg_linear*finally_cut+nn
										if y_score<scores[pos]:
											finally_cut_=levels[pos-1]
										else:
											finally_cut_ = levels[pos]
										cutting_dict[alig_Id] = d_help[finally_cut_][1]
										break
			
		finally_cut = None
#	print 'Levels',Levels
#	print 'NodesMembers', NodesMembers
#	for mem in d_help.keys():
#		print mem, d_help[mem]
	for alig_Id in list(cutting_dict.keys()):
#		print alig_Id,
		cut = cutting_dict[alig_Id]
#		print cut, cut*maxlevel,d_help[cut][1]
		dict_of_cuttings[alig_Id] = NodesMembers[Levels[alig_Id][cut]]
	dict_of_cuttings = get_new_cut(dict_of_cuttings)#************fertig
	for key in list(dict_of_cuttings.keys()):
		for alignment_id in dict_of_cuttings[key]:
			if alignment_id in membership:	break
			else:	membership[alignment_id] = dict_of_cuttings[key]	
	membership['0'] = Set(['0'])


#	for key in dict_of_cuttings.keys():
#		print key, dict_of_cuttings[key]


	cnt_clusters = 0
	dict_clusters = {}
	yes = False
	for entry in list(membership.keys()):
		clust = membership[entry]
		if cnt_clusters == 0:
			cnt_clusters += 1	
			dict_clusters[cnt_clusters] = clust
		listi = deepcopy(list(dict_clusters.keys()))	
		for clu_entry in listi:
			clu = dict_clusters[clu_entry]
			if len(clust.symmetric_difference(clu)) == 0:
				yes = False
				break
					
			else:		
				yes = True

		if yes:
			cnt_clusters += 1
			dict_clusters[cnt_clusters] = clust
			yes = False
	subclusternew = Set([])
	for key in list(dict_clusters.keys()):
		if len(dict_clusters[key]) > 30:
			for key1 in list(NodesMembers.keys()):
				if type(key1) == int and len((dict_clusters[key]).symmetric_difference(NodesMembers[key1])) == 0:
					print((NodesMembers[key1]))
					subclusternew.add(key1)
					break

	print('now I return')
	for key in subclusternew:
		print((key, NodesMembers[key],'888888888888888888888888888888888888888888'))
	print(subclusternew)
	return membership,subclusternew


def clustering_main(blastinfo, bin_codiert, query, Blosum62, storecalc):
	print('bin in clustering_main')

	alignment_Ids, members, distance = {}, {}, {}
	Nodes = {}
	NodesMembers = {}
	NodesScores = {}
	NodesLevels = {}
	Levels = {}

	for k in list(bin_codiert.keys()):
		alignment_Ids[k] = []
		members[k] = [k]
	
	print('Initial Scoring Matrix	->	Started')
	
	clusterspecific_positioninfo_dict = store_position_info(alignment_Ids, bin_codiert)  # ************ist fertig
	# clusterspecific_positioninfo_dict = bin_codiert.copy()


	matches_dict, mismatches_dict, common_2alignments, uncommon_2alignments, normalize_dict, max_matches_per_alignment = \
		combinations(bin_codiert, query, Blosum62)  # ****************ist fertig

	print('			....	Finished')

	matches_mod_dict = deepcopy(matches_dict)  # becomes modified during clustering
	mismatches_mod_dict = deepcopy(mismatches_dict)  # becomes modified during clustering

	for alig in list(alignment_Ids.keys()):

		NodesMembers[alig] = Set([alig])
		NodesScores[alig] = 10**-20
		NodesLevels[alig] = 0
		Levels[alig] = {0: alig}


	print('Clustering 		->	Started')

	Node_cnt = 0
	tree_Michael_format = {}
	while len(alignment_Ids)>1:
		Node_cnt += 1
		# nextNode = 'Node' + str(Node_cnt)
		nextNode = Node_cnt
		if len(alignment_Ids) % 10 == 0:
			print(('\t\t\t\t', len(alignment_Ids)))
			
		candidates, alignment_Ids, matches_mod_dict, mismatches_mod_dict, maximum = \
			find_maximum(matches_mod_dict, mismatches_mod_dict, alignment_Ids)  # ******************ist fertig

		subNode1 = candidates[0]
		subNode2 = candidates[1]

		members, distance, tree_Michael_format = store_tree_data(members, subNode1, subNode2, distance, nextNode,
																 maximum, tree_Michael_format)

		Levels, Nodes, NodesMembers, NodesScores, NodesLevels = store_level_cluster_info(candidates, Levels, Nodes,
																						 NodesMembers, NodesScores,
																						 NodesLevels, nextNode, subNode1,
																						 subNode2, maximum)  # ***********fertig

		matches_mod_dict, mismatches_mod_dict, clusterspecific_positioninfo_dict, alignment_Ids = \
			get_new_matrix(NodesMembers, candidates, nextNode, matches_mod_dict, mismatches_mod_dict, alignment_Ids,
						   clusterspecific_positioninfo_dict, common_2alignments, uncommon_2alignments, query,
						   normalize_dict, mismatches_dict, Blosum62)  # ***************fertig
	print('			....	Finished')
	tree_NJPLOT_format, tree_Michael_substIds = make_tree_NJPLOT(tree_Michael_format, blastinfo)
	again = True
	true_membership = {}
	allclids = []
	for xxxx in list(NodesMembers.keys()):
		if type(xxxx) == int:
			allclids.append(xxxx)
	clusterid = max(allclids)
	subclusters = Set([clusterid])
	subclusters_done = Set([])
	while len(subclusters):
		subclustersnews = Set([])
		for subcluster in subclusters.difference(subclusters_done):
			subclusters_done.add(subcluster)
			membership, subclustersnew = determine_home_clusters(storecalc, Levels, NodesMembers, NodesScores, subcluster)  # fertig
			subclustersnews.update(subclustersnew)
			for cand in list(membership.keys()):
				true_membership[cand] = membership[cand]			
		subclusters = subclustersnews

	return matches_dict, distance, true_membership, tree_Michael_substIds, tree_NJPLOT_format
