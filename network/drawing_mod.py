#!/usr/bin/env python

import zipfile
from Bio import Entrez
Entrez.email = "msharan@age.mpg.de"                             
NCBI_URL = "http://www.ncbi.nlm.nih.gov/entrez/viewer.fcgi?val="
#foutMain = open ("zMainFile.sif","w")
#foutNAt = open ("zNodeAttributesFile.noa", "w")

def build_ortholog_network(ortho_network,ortholog,paralog,uncertain,blast_main_info,bb_storageIds, c, nw_score=None, eg_score=None):
               
        sif = []
        mainsif =[]
        nodesif = []
        ortho_network.append("graph Ortholog {\n") # open graph
       
        color = ('red', 'blue', 'black')
        #subgraph = ('Orthologs','Paralogs','Uncertain')
        bb_storageIds_neighbor_list = []					#nodes which have only in-degree (edges) which are missed out in sif are stored in this list
        species_list = []							# list of all species including isoforms
        species1_list = []							# list of species containing isoforms
        all_low = []                                                            # list for iso other than high score iso
        id_list = []
        seqID_isoid = {}							# dict for mapping sequence ID with internal ids

        species_evalue_id = {}							# dict for mapping species with e-values and ids
        seqID_species = {}							# dict for mapping sequence ID with species
        seqID_score = {}							# dict for mapping sequence ID with score
        seqID_gene = {}
        #for sif files
        mainsif = []								# stores information for cytoscape 
        nodesif = []								# stores node annotation 

        for n in (1,):								# "dummy loop" as I'm not shure about code ident
                result = []
                for result_part in (ortholog, paralog, uncertain):
                        result.extend(result_part)				# merge all results

                result = list(result)						# necessary?
                result.sort() 							# start with query '0'
                # add result nodes


                for i in range(len(result)):
                  
                    id = result[i]
                    
                    try:
                        seqID = bb_storageIds[id] 				# seq id
                    except KeyError: 						# '2' is not in bb_storageIds!
                        seqID = list(blast_main_info['0'][id][0])[0]
                    seqID_isoid[seqID] = id
                       
                    species = blast_main_info['0'][id][1]
                    evalue = blast_main_info['0'][id][2]
                    geneID = blast_main_info['0'][id][-1]                       
                    seqID_gene[seqID] = geneID

                    if nw_score:
                        try:
                                score = "%.4f" % nw_score.score[id]
                        except KeyError:                # '0' has no score
                                score = "1.00"
                    else:
                        score = ''
                    seqID_score[seqID] = score

                    ortho_network.append('%s [label="%s\\n[%s]\\n%s (%s)" URL="%s"];\n' % (id, seqID, species, score, evalue, NCBI_URL+seqID) )

                    species_evalue_id[species] = (evalue,id)
                    seqID_species[seqID] = species
                
                    #add result edges for unidirected network

                    for neighbor in result[i+1:]: # we leap forward collecting all edges
                                                                                                            
                            if neighbor in c[id]['best'] and id in c[neighbor]['best']:
                                edge_color = 'red'; edge_style = 'solid'
                                sif_edge_type = "bb"
                                #score = 1
                            
                            elif neighbor in c[id]['best'] and id in c[neighbor]['acceptable']:
                                edge_color = 'black'; edge_style = 'solid'
                                sif_edge_type = "ba"
                                #score = 0.75
                             
                            elif neighbor in c[id]['acceptable'] and id in c[neighbor]['best']:
                                edge_color = 'black'; edge_style = 'solid'
                                sif_edge_type = "ab"
                                #score =  0.75
                              
                            elif neighbor in c[id]['acceptable'] and id in c[neighbor]['acceptable']:
                                edge_color = 'green'; edge_style = 'solid'
                                sif_edge_type = "aa"
                                #score = 0.50
                           
                            elif neighbor in c[id]['best']:
                                edge_color = 'red'; edge_style = 'dashed'
                                sif_edge_type = "0b"
                                #score = 0.30
        
                            elif neighbor in c[id]['acceptable']:
                                edge_color = 'black'; edge_style = 'dashed'
                                sif_edge_type = "0a"
                                #score = 0.15
                                     
                            else:
                                edge_color = 'white'; edge_style = 'solid'
                                sif_edge_type = None
                                #score = 0.0
                                                                                                            
                            #ortho_network.add_edge(pydot.Edge(id,neighbor, color = edge_color))
                            ortho_network.append(" %s -- %s [color=%s, style=%s];\n" % (id,neighbor, edge_color, edge_style)) # never change this format
                            #if sif_edge_type:               
                            try:
                                sif.append ("%s %s %s %s %s %s" % (seqID, sif_edge_type, bb_storageIds[neighbor], score , evalue, species))
                                if sif_edge_type != None:
                                    mainsif.append("%s\t%s\t%s" % (seqID,sif_edge_type,bb_storageIds[neighbor]))				    
                                nodedata = "%s\t%s\t%s\t%s" % (seqID,evalue,species,score)
                                if nodedata not in nodesif:
                                    nodesif.append(nodedata)
                                bb_storageIds_neighbor_list.append(bb_storageIds[neighbor])
                                id_list.append(seqID)
                            except KeyError:            
                                pass#pdb.set_trace()

                # the following codes to remove isoforms from the network.

                    if species not in species_list:     
                        species_without_isoform = "Isoforms not present for : %s\n"%species
                        species_list.append(species)                                                            # list of all species including isoforms
                        iso_seqID = seqID
                    else :
                        species1_list = []                     
                        species1_list.append(species)                                                           # list of only isoform species
                        
                        for eachspecies in species1_list:
                            isoform_dict = {}
                    
                            for (isoformid,isoformspecies) in list(seqID_species.items()):
                                isoform_dict.setdefault((isoformspecies), []).append(isoformid)                 # append seqID to respective species and also multiple seqID to the isoform species
                                #print "isoform_dict%s"%isoform_dict
                            isoform_allseq = isoform_dict[eachspecies]                                          # extract the isoform species information from the previous list that we are interested in at this point
                            str(isoform_allseq)
                            species_isoform_seqID = "Multiple proteins from species : %s = %s"%(species,isoform_allseq)      # all the seqID or isoforms of the species
                            print(species_isoform_seqID)

                            ncbiid_list = []
                            for eachid in isoform_allseq:
                                ncbi_id = seqID_gene[eachid]
                                ncbiid_list.append(ncbi_id)
                            length_ncbilist =  len(set(ncbiid_list))

                            if length_ncbilist == 1:
                            #if isoform_allseq:
                                scorecheck = []                                                                     # temperory list for each isoform check
                                tempdict = {}
                                for eachisoID in isoform_allseq:                                                    # this look extracts score information for each isoform seqID
                                    allscore = seqID_score[eachisoID]                                               # findscore
                                    tempdict[allscore] = eachisoID                                                  # reverse the mapping to getthe seqID back later
                                    scorecheck.append(allscore)                                                     # save all score
                                scorecheck.sort()                                                                   # the list sorting based on score
                                #print str(scorecheck)                                                              # make sure there is no duplicates
                                isoform_highscore = scorecheck[-1]                                                  # seqID with high score (this is what we are interested to keep in network and rest of the isoform should be avoided)
                                iso_seqID = tempdict[isoform_highscore]                                             # finally get the sequence with high score
                                #print "iso_seqID = %s"%iso_seqID
                           
                                scorecheck.remove(isoform_highscore)                                                # list with only low score iso so that the information associated with this seqID can be removed from network
                                if scorecheck:                                                                      # check if its empty (for testing locally as the score is null) should be deleted from main
                                    for eachitem in scorecheck :
                                        lowSeq_id = tempdict[eachitem]                                              # find id of all those low scoring iso
                                        all_low.append(lowSeq_id)                                                   # form a list of all seqID of low iso
                                                                                              
                for neighborID in bb_storageIds_neighbor_list:
                    # print "saving left out node information"
                    if neighborID not in id_list:
                        species = seqID_species[neighborID]
                        nodesif.append("%s\t%s\t%s\t%s" % (neighborID, species_evalue_id[species][0], species, seqID_score[neighborID]))
                        id_list.append(neighborID)			

        print(("all isoform ids removed from network are %s"%all_low))

        ### this loop removes all low score isoforms data from the ortho_network and sif files stored previously ###
        for low in all_low:
            for i in range(len(ortho_network)):                                             
                if low in ortho_network[i]:
                    ortho_network[i] = ''
                elif " %s -- "%seqID_isoid[low] in ortho_network[i]:
                    ortho_network[i] = ''
                elif " -- %s "%seqID_isoid[low] in ortho_network[i]:
                    ortho_network[i] = ''        

            for i in range(len(sif)):
                    if low in sif[i]:
                        sif[i] = ''
        
            for i in range(len(mainsif)):					
               if low in mainsif[i]:
                    mainsif[i] = ''

            for i in range(len(nodesif)):
                if low in nodesif[i]:
                    nodesif[i] = ''

        ortho_network = [_f for _f in ortho_network if _f]				# removes all empty lines from network
        ortho_network.append("}")  # close network

        ### Cytoscape Files ###       
        nodesif = [_f for _f in nodesif if _f]						# removes all empty lines from node file
        nodesif = '\n'.join(nodesif)
        # foutNAt.write(nodesif)

        mainsif = [_f for _f in mainsif if _f] 						# removes all empty lines from main sif file
        mainsif = '\n'.join(mainsif)
        # foutMain.write(mainsif)
        
        sif = [_f for _f in sif if _f]                                                   # removes all empty lines from the internal sif data
        sif = '\n'.join(sif)                                                    # close network : sif extension to the file

        return sif, mainsif, nodesif


if __name__ =='__main__':

        from pickle import load
        file1 = "zMainFile.sif"
        mainfile = open(file1, "w")
        file2 = "zNodeAttributesFile.sif"
        nodefile = open(file2, "w")
        myDot = []
        o, p, u, bmi, bb,  c = [load(open(dfn)) for dfn in ['ortholog', 'paralog', 'uncertain_ortholog',
                                                            'blast_main_info_0', 'bb_storageIds', 'common']]
        sif, mainsif, nodesif = build_ortholog_network(myDot, o, p, u, bmi, bb, c)
        print(mainsif)
        # print nodesif
        for interaction in mainsif:
                mainfile.write(interaction)

        for attribute in nodesif:
                nodefile.write(attribute)

        mySif = []                             
        # archive_list = ["zMainFile.sif","zNodeAttributesFile.sif"]
        archive_list = [file1, file2]

        zfilename = "CytoscapeFile" + '_' + 'X' + ".zip"  # Give ID for X in morfeusd
        zout = zipfile.ZipFile(zfilename, "w")
        for fname in archive_list:
                zout.write(fname)
        zout.close()

        mainfile.close()
        nodefile.close()
        # print '\n'.join(myDot)


