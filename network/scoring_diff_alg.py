from networkx import *
from pickle import load
from pickle import dump


class OrthologNetwork:
    
    def __init__(self, rootNode, connectionsPath="common", nodeIdPath="bb_storageIds", networkPath="connections.txt",
                 scoredNodesPath="scoredNodes.txt", scoredNodesPickleFile="scoredNodesDict",
                 algorithm="eigenvector", max_iter=100, tol=9.9999999999999995e-07):
        
        """ initialize the ortholog network, named parameters taken - connections = {}, rootNode = None, useWeights = True, useRbh = True"""
        
        self.network = Graph()
        self.allDistances = {}
        self.weights = {}
        self.mother = rootNode
        self.levelToNodes, self.nodeToLevel = {}, {}
        self.allShortestPaths={}
        self.score={}
        self.connections={}
        self.nodeIds={}
        
        #input paths
        self.twoWayConnections={}
        self.connectionsPath=connectionsPath
        
        
        #output paths
        self.nodeIdPath=nodeIdPath                              
        self.networkPath=networkPath                            
        self.scoredNodesPath=scoredNodesPath                    
        self.scoredNodesPickleFile=scoredNodesPickleFile
        self.algorithm=algorithm
        if algorithm=="closeness":
            self.connectionWeights={'rbh':0.0625,'rb':0.125,'rh':0.25,'b':0.5,'a':1}
        else:
            self.connectionWeights={'rbh':1,'rb':0.5,'rh':0.25,'b':0.125,'a':0.0625}
            
        # eigenvector_centrality parameters
        self.evc_max_iter=max_iter
        self.evc_tol=tol
        
        return
    
    def loadNetwork(self):
        self.connections=load(open(self.connectionsPath, 'rb'))
        self.nodeIds=load(open(self.nodeIdPath, 'rb'))
        
    def saveScoredNetwork(self):
        
        nodeList=list(self.twoWayConnections.keys())
        self.score
        
        file=open(self.networkPath,"w")
        for node in nodeList:    
            for connection in self.twoWayConnections[node]:
                line=self.nodeIds[node]+' '+connection[1]+' '+self.nodeIds[connection[0]]+'\n'
                file.write(line)
        file.close
        
        file=open(self.scoredNodesPath,"w")
        for node in self.score:
                line=self.nodeIds[node]+' '+str(self.score[node])+'\n'
                file.write(line)
        file.close        
        
        file_handle=open(self.scoredNodesPickleFile,"w")
        dump(self.score,file_handle)
        file_handle.close()
        
    def createNetwork(self):
        keysOfNodes=list(self.nodeIds.keys())
        
        trusts=['best','acceptable']#,'exclude']
        twoWayTrusts={'bestbest':'rbh','bestacceptable':'rb','acceptablebest':'rb','acceptableacceptable':'rh',('best'):'b',('acceptable'):'a'}
        
        
        for key in keysOfNodes:
            
            for trust in trusts:
                
                for node in self.connections[key][trust]:
                    
                    #checkForOneWayConnection-key->node
                    if (node in self.nodeIds)==True:
                        hits=trust #onewayconnection
                        
                        
                        for trust2 in trusts:
                            #checkForTwoWayConnection-node->key
                            if key in self.connections[node][trust2]:
                                hits=hits+trust2
                                #deleteConnection-node->key
                                self.connections[node][trust2].discard(key)
                        
                        #addConnectionToConnectionStorage
                        if (key in self.twoWayConnections)==True:
                            self.twoWayConnections[key].append((node,twoWayTrusts[hits]))
                        else:
                            self.twoWayConnections[key]=[(node,twoWayTrusts[hits])]
                            
        self.addToNetwork(self.twoWayConnections)

    
        
    def linear_level(self,useLevel=True):
        
        self.noMotherDistances = self.__withoutMotherDistances__()
        self.__createLevelsHash__()
        self.__scoring__(useLevel)
                
        return self.score
    
    def linear_nolevel(self,useLevel=False):
        
        self.noMotherDistances = self.__withoutMotherDistances__()
        self.__createLevelsHash__()
        self.__scoring__(useLevel)
                
        return self.score
    
    def closeness(self):
        maxScore=0.0
        minScore=1000000.0
        closeness_hash=closeness_centrality(self.network, weighted_edges = True)
        for node in closeness_hash:
            if closeness_hash[node]>maxScore:    
                maxScore=closeness_hash[node]
            if closeness_hash[node]<minScore:    
                minScore=closeness_hash[node]
        maxScore=maxScore-minScore
        for node in closeness_hash:  
            self.score[node]=float((closeness_hash[node]-minScore)/maxScore)
            #print float((closeness_hash[node]-minScore)/maxScore)
        del self.score[self.mother]
        
    def do_scoring(self):
        if self.algorithm=="closeness":
            self.closeness()
        elif self.algorithm=="eigenvector":
            self.eigenvector()
        elif self.algorithm=="linear_level":
            self.linear_level()
        elif self.algorithm=="linear_nolevel":
            self.linear_nolevel()
        
    def eigenvector(self):
        maxScore=0.0
        minScore=1000000.0
        try:
            closeness_hash=eigenvector_centrality(self.network, max_iter=self.evc_max_iter, tol=self.evc_tol, nstart=None)
        except:
            closeness_hash=eigenvector_centrality(self.network, max_iter=(self.evc_max_iter)*100, tol=self.evc_tol, nstart=None)
        for node in closeness_hash:
            if closeness_hash[node]>maxScore:    
                maxScore=closeness_hash[node]
            if closeness_hash[node]<minScore:    
                minScore=closeness_hash[node]
        maxScore=maxScore-minScore
        for node in closeness_hash:
            try:
                self.score[node]=float((closeness_hash[node]-minScore)/maxScore)
            except ZeroDivisionError:
                self.score[node]=float((closeness_hash[node]-minScore)/0.00001)
            #print float((closeness_hash[node]-minScore)/maxScore)
        del self.score[self.mother]
    
    def gradientLevelCalculus(self,numberOfNodesInLevel,gradientPreLevel):
        gradientLevel={}
        gradientLevel['rbh']=gradientPreLevel/float(numberOfNodesInLevel+1)
        gradientLevel['rb']=gradientLevel['rbh']/float(numberOfNodesInLevel+1)
        gradientLevel['rh']=gradientLevel['rb']/float(numberOfNodesInLevel+1)
        gradientLevel['b']=gradientLevel['rh']/float(numberOfNodesInLevel+1)
        gradientLevel['a']=gradientLevel['b']/float(numberOfNodesInLevel+1)
        return gradientLevel
    
    def gradientPreLevelCalculus(self,numberOfNodesInPreLevel):
        gradientPreLevel={}
        gradientPreLevel['rbh']=1
        gradientPreLevel['rb']=gradientPreLevel['rbh']/float(numberOfNodesInPreLevel+1)
        gradientPreLevel['rh']=gradientPreLevel['rb']/float(numberOfNodesInPreLevel+1)
        gradientPreLevel['b']=gradientPreLevel['rh']/float(numberOfNodesInPreLevel+1)
        gradientPreLevel['a']=gradientPreLevel['b']/float(numberOfNodesInPreLevel+1)
        return gradientPreLevel
    
    def __scoring__(self,useLevel):
        score=[]
        gradientLevel={}
        gradientPreLevel={}
        self.score={}
        numberOfNodesInPreLevel=1
        weightsInLevel=['rbh','rb','rh','b','a']
        levels=list(self.levelToNodes.keys())
        levels.sort()
        levels.remove(0)
        
        self.score[self.mother]=0
        
        numberOfAllNodes=self.network.__len__()
        gradientPreLevel=self.gradientPreLevelCalculus(1)
        gradientLevel=self.gradientLevelCalculus(numberOfAllNodes,gradientPreLevel['a'])
        offsetLevel=(numberOfAllNodes-2)*gradientLevel['rbh']
        offsetPreLevel=1*gradientPreLevel['rbh']
        
        #bestNetworkScore=(float(offsetPreLevel+offsetLevel))
        #bestNetworkScore=bestNetworkScore-(gradientLevel['rbh']*(numberOfAllNodes-2))
        #bestNetworkScore=bestNetworkScore-gradientPreLevel['rbh']*1
        #bestNetworkScore=bestNetworkScore/float(offsetLevel+offsetPreLevel)+1
        #print bestNetworkScore,'bestnetworkscore'
        
        for level in levels:
            numberOfNodesInLevel=len(self.levelToNodes[level])
            nodesInLevel=self.levelToNodes[level]
            gradientPreLevel=self.gradientPreLevelCalculus(numberOfNodesInPreLevel)
            
            gradientLevel=self.gradientLevelCalculus(numberOfNodesInLevel-1,gradientPreLevel['a'])
            offsetLevel=(numberOfNodesInLevel-1)*gradientLevel['rbh']
            offsetPreLevel=(numberOfNodesInPreLevel)*gradientPreLevel['rbh']
            
            for node in nodesInLevel:
                scorePreLevel={}
                scoreLevel={}
                numberContributorsLevel={}
                numberContributorsPreLevel={}
                self.score[node]=float(offsetPreLevel+offsetLevel)
                children=self.network[node]
                
                for child in children:
                    
                    if self.nodeToLevel[child]<level:
                        if (self.weights[(node,child)] in numberContributorsPreLevel)==False:
                            numberContributorsPreLevel[self.weights[(node,child)]]=0
                        numberContributorsPreLevel[self.weights[(node,child)]]=numberContributorsPreLevel[self.weights[(node,child)]]+1
                        self.score[node]=self.score[node]-gradientPreLevel[self.weights[(node,child)]]*(float(level)-float(self.score[child]))
                        
                    if self.nodeToLevel[child]==level:
                        if (self.weights[(node,child)] in numberContributorsLevel)==False:
                            numberContributorsLevel[self.weights[(node,child)]]=0
                        numberContributorsLevel[self.weights[(node,child)]]=numberContributorsLevel[self.weights[(node,child)]]+1
                        
                for weight in weightsInLevel:
                    if (weight in numberContributorsLevel)==True:
                        self.score[node]=self.score[node]-(gradientLevel[weight]*numberContributorsLevel[weight])
                
                if useLevel==True:
                    self.score[node]=(self.score[node]/float(offsetLevel+offsetPreLevel)+level)
                else:
                    self.score[node]=float(1-self.score[node]/float(offsetLevel+offsetPreLevel))
                
            numberOfNodesInPreLevel=numberOfNodesInLevel
            
        del self.score[self.mother]
        nodes=list(self.score.keys())
        minScore=1.0
        maxScore=0.0
        if useLevel==True:
            for node in nodes:
                self.score[node]=1/(self.score[node])
                if self.score[node]>maxScore:    
                    maxScore=self.score[node]
                if self.score[node]<minScore:    
                    minScore=self.score[node]
            maxScore=maxScore-minScore
        else:
            for node in nodes:
                if self.score[node]>maxScore:    
                    maxScore=self.score[node]
                if self.score[node]<minScore:    
                    minScore=self.score[node]
            maxScore=maxScore-minScore
        for node in nodes:
            self.score[node]=float((self.score[node]-minScore)/maxScore)
    
 
    def __withoutMotherDistances__(self):
        
        allNodes = self.network.nodes()
        allNodes.remove(self.mother)
        
        self.networkNoMother = self.network.subgraph(allNodes)
        return path.all_pairs_shortest_path_length(self.networkNoMother)
    

        
    def __createLevelsHash__(self):
        
        numOfChildren = len(self.network.nodes()) - 1
        currParents = [self.mother]
        finishedChildren = []
        currLevel = 1
        score={}
        
        while len(finishedChildren) != numOfChildren:
            children = []
            for node in currParents:
                currChildren = self.network[node]
                for child in currChildren:
                    if child == self.mother: continue
                    if (child not in children) and (child not in finishedChildren):
                        children.append(child)
                            
            self.levelToNodes[currLevel] = []
            
            for child in children:
                
                finishedChildren.append(child)
                self.nodeToLevel[child] = currLevel
                self.levelToNodes[currLevel].append(child)
                
            currParents = [parent for parent in children] 
            currLevel += 1
        self.nodeToLevel[self.mother], self.levelToNodes[0] = 0, [self.mother]
        
        return

    def addToNetwork(self, edges):
        
        edgeData = []
        if type(edges) == type({}):
        # deal with the edges being input as a hash, a single edge or a list of edges, where weight is not specified and is to be used use 1, where not specified and not to be used use 0
            for node in edges:
                for anchestor in edges[node]:
                    edgeData.append((node, anchestor[0], anchestor[1]))
                    
        elif type(edges) == type(()):
            edgeData.append((edges[0], edges[1], edges[2]))   

        # add the edges to the network
        
        for edge in edgeData:
            
            self.network.add_edge(edge[0], edge[1], weight=self.connectionWeights[edge[2]])
            self.weights[(edge[0],edge[1])] = self.weights[(edge[1],edge[0])] = edge[2]

        return