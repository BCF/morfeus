# File:         HTMLTemplate.py
#
# Description:  Provide a simple HTML template. Need to write some
#               more extensive documentation
#
#
from re import *
import sys
import copy
from html import escape as html_escape


class HTMLTemplate:
    #
    # Loads the page. If it can't do it it will return an error string
    # which then can be used for displaying
    #
    def LoadPage(self):
        try:
            file = open(self.filename, "r")
            lines = file.readlines()
            file.close()
        except:
            lines = ["HTMLTemplate: error loading " + self.filename]

        return ''.join(lines)  # join(lines,'')

    #
    # Takes care of replacing the following
    #   {IFDEF:variable:HTML Code:variable}
    #   {IFNDEF:variable:HTML Code:variable}
    #
    # It will only insert the HTML code if the variable is Set()
    #
    def ReplaceConditionals(self, text):
        regexplook = "\{(IFN?DEF):(\w+):"
        regexpcomp = compile(regexplook, DOTALL)
        splitresults = regexpcomp.split(text,maxsplit=1)
        size_matches = len(splitresults)

        # so, if there is a IFDEF anywhere, the list should have 4
        # parts. 1 with text before IFDEF, one with the IFDEF or IFNDEF
        # itself, one with the x from {IFDEF:x:, and the other with
        # everything after
        if size_matches!=4:
            # there was no begin.  We just return the original text
            return text

        outputtext = [splitresults[0]]
        operation = splitresults[1]
        tag = splitresults[2]
        text = splitresults[3]

        regendlook = ":%s}" % tag
        start_of_end = text.find(regendlook)

        # part before the end tag   
        replacement = text[:start_of_end]

        # Here is the text after {END:blah} 
        text = text[start_of_end+len(regendlook):]      

        # Now the doc is split into these parts:

        # outputtext
        # tag (which is the real tag)
        # parttoworkon
        # {END:tag}
        # text

        if operation == "IFDEF" and tag not in self.tags:
            replacement = ""
        elif operation == "IFNDEF" and tag in self.tags:
            replacement = ""
        outputtext.append(self.ReplaceConditionals(replacement+text))
        return ''.join(outputtext)  # join(outputtext,'')

    # Takes care of list generator which looks like
    # {BEGIN:LISTNAME}
    #   {LISTNAME:VAR1} {LISTNAME:VAR2}
    # {END:LISTNAME}
    #
    # For this it expects a list of dictionaries
    #

    def ReplaceBeginEnd(self, text, nested_lists = {}):
        outputtext = [] 

#       regbeginlook = "\{BEGIN:(\w+|\w+->\w+)\}"
        regbeginlook = "\{BEGIN:([\w\->]+)\}"
        compbeginlook = compile(regbeginlook, DOTALL)   
        beginsplitresult = compbeginlook.split(text, maxsplit=1)
        size_matches = len(beginsplitresult)

        # nested_lists contains a second list of lists which are to also
        # be replaced.  These come, obviously, from nested BEGIN/ENDS on
        # lists.
        # We need our own version of the nested_lists to pass in recursively
        # with added lists, and we need to keep the original for working on
        # subsequent text at the end of this function.
        mod_nested_lists = copy.deepcopy(nested_lists)

        # so, if there is a begin anywhere, the list should have 3
        # parts. 1 with text before begin, one with the x from 
        # {BEGIN:x}, and the other with everything after

        if size_matches != 3:
            # there was no begin.  We just return the original text
            return text

        outputtext.append(beginsplitresult[0])
        tag = beginsplitresult[1]

        text = beginsplitresult[2]
        regendlook = "{END:%s}" % (tag)
        start_of_end = text.find(regendlook)        

        # part before the end tag   
        parttoworkon = text[:start_of_end]
        
        # Here is the text after {END:blah} 
        text = text[start_of_end+len(regendlook):]      

        # Now the doc is split into these parts:

        # outputtext
        # tag (which is the real tag)
        # parttoworkon
        # {END:tag}
        # text

        this_item = None
        if tag in self.listtags:
            this_item = self.listtags[tag]
        elif tag in nested_lists:
            this_item = nested_lists[tag]

        if this_item is None:
            # This means that we aren't trying to replace this thing
            # thus, we don't worry about it and check the rest of it
            # for other begin's and end's
            outputtext.append("(BEGIN:%s}" % (tag))
            outputtext.append(self.ReplaceBeginEnd(parttoworkon)) 
            outputtext.append("{END:%s}" % (tag))
        elif len(this_item) != 0:
            # We only modify our text if we are setting it to
            # something.  If not, we are deleting everything
            # between the begin and end tags

            regtaglook = "({%s:\w+})" % tag
            # i.e. regtaglook = "(\{INCIDENTS:\w+\})"
            comptaglook = compile(regtaglook) 
            # dictionary to keep track of where the replaceable strings are
            where_dict = {}     
            result = [] 

            # first_item is the first list from the dictionary
            first_item = this_item[0]
            the_strings = comptaglook.split(parttoworkon)       

            # now we have the string split          
            # a few parts of the array look like {INCIDENT:COLOR}, etc
            end_of_array = len(the_strings)
            for key in list(first_item.keys()):
                for lcv in range(end_of_array):
                    toreplace = "{%s:%s}" % (tag,key)
                    if the_strings[lcv] == toreplace:
                        where_dict[lcv] = key   

            # Now we look for nested BEGIN/END's.  These are indicated
            # by {BEGIN:LIST1->LIST2} {LIST1->LIST2:string} {END:LIST1->LIST2}
            regtaglook = "({BEGIN:%s->\w+})" % tag
            comptaglook = compile(regtaglook) 
            nest_strings = comptaglook.split(parttoworkon)
            end_nest_strings = len(nest_strings)
            list_dict = {}
            for key, value in list(first_item.items()):
                for lcv in range(end_nest_strings):
                    listreplace = "{BEGIN:%s->%s}" % (tag, key)
                    if nest_strings[lcv] == listreplace:
                        nestkey = "%s->%s" % (tag, key)
                        list_dict[nestkey] = key

            # For the nested ifdef's, we only want to catch ifdef's
            # which really are on this list's members.  So we rewrite
            # those IFDEF's which we find containing exactly one ->
            # after the current tag.  So if we're currently looking at
            # a doubly nested list list1->list2, then the string
            #   {IFDEF:list1->list2->key: (.*) :list1->list2->key}
            # will be rewritten as:
            #   {IFDEF:key: (.*) :key}.
            beforetxt = '''{(?P<cmd>IFN?DEF):   # Clearly, <cmd> is the IFDEF or IFNDEF
            %s\->(?P<tag>\w+):                  # percent s is replaced by the list name,
                                                                                # tag by the tag being IFDEF'd on
            (?P<txt>.*?)                        # This is the text between the IFDEF start/end
            :%s\->(?P=tag)}                     # Finally, this is the ending :list->key}
            ''' % (tag, tag)
            after = "{\g<cmd>:\g<tag>:\g<txt>:\g<tag>}"
            before = compile(beforetxt, DOTALL|VERBOSE)
            for rep in this_item:
                # now we go through and use the rep dictionary to fill in the
                # values for our strings

                for key in list(where_dict.keys()):
                    try:
                        the_strings[key] = str(rep[where_dict[key]])
                    except:
                        pass

                tmp_result = ''.join(the_strings)  # join(the_strings,'')

                tmp_result = sub(before, after, tmp_result)

                # We insert the CURRENT value of any nested lists into
                # mod_nested_lists.  Those lists will then also be considered
                # for expansion on the recursive call to ourselves.
                for key,value in list(list_dict.items()):
                    mod_nested_lists[key] = rep[value]

                # We call ReplaceConditionals on our resulting string.  We
                # already called it for normal keys, so *all* we care about
                # is keys which are keys of this current list element.
                # Rather than alter how ReplaceConditionals works, we just
                # substitute our list of (non-list) tags, with the list of
                # keys in this current list element.
                realtags = self.tags
                self.tags = rep
                tmp_result = self.ReplaceConditionals(tmp_result)

                # ... and change it back :)
                #
                # (currently that is actually not necessary - we don't use
                # our 'tags' hash again.  But who knows how we'll want to expand
                # later)
                self.tags = realtags
                # And rerun ourselves recursively on the text we just expanded
                tmp_result = self.ReplaceBeginEnd(tmp_result, mod_nested_lists)
                result += tmp_result

            result_string = ''.join(result)  # join(result,'')

            outputtext.append(result_string)

        # text is everything after our {END} tag
        outputtext.append(self.ReplaceBeginEnd(text, nested_lists))

        return ''.join(outputtext)  # join(outputtext,'')

    #
    # Replaces simple tags like {VARIABLE}
    # Calls another function to replace {BEGIN} and {END} tags 
    #

    def ReplaceTags(self, text):
        for tag in list(self.tags.keys()):
            # regexp = "{" + tag + "}"
            regexp = "{%s}" % (tag)
            text = text.replace(regexp,str(self.tags[tag]))
        
        # text is the entire webpage    
        outputtext = self.ReplaceBeginEnd(text)

        return outputtext

    # This immediately replaces the tag with the replacement text, rather
    # than waiting until the page.Output() command.
    # This is needed for the TabList base class.
    def SetNow(self, tag, replacement):
        regexp = "{%s}" % (tag)
        self.lines = self.lines.replace(regexp, replacement)

    def ReplaceTags(self, text):
        for tag in list(self.tags.keys()):
            # regexp = "{" + tag + "}"
            regexp = "{%s}" % (tag)
            text = text.replace(regexp,str(self.tags[tag]))
        
        # text is the entire webpage    
        outputtext = self.ReplaceBeginEnd(text)

        return outputtext

    def DoIncludes(self, text):
            outputtext = []
            regbeginlook = "\{INCLUDE:([^\}]+)\}"
            compbeginlook = compile(regbeginlook, DOTALL)
            beginsplitresult = compbeginlook.split(text, maxsplit=1)
            size_matches = len(beginsplitresult)

            if size_matches != 3:
                return text
            outputtext.append(beginsplitresult[0])
            fnam = beginsplitresult[1]

            endtext = beginsplitresult[2]
            try:
                file = open(fnam, "r")
                inclines = file.readlines()
                file.close()
            except:
                inclines = ["HTMLTemplate: error loading " + fnam]
            inctext = ''.join(inclines)  # join(inclines, '')
            outputtext.append(inctext)
            outputtext.append(self.DoIncludes(endtext))

            return ''.join(outputtext)  # join(outputtext, '')

    def __init__(self, filename):
        self.filename = filename
        self.lines = self.LoadPage()
        self.tags = {}
        self.listtags = {}

    # Run cgi.escape on each string field in a list of dictionaries.
    # If cols is specified, then only fields whose keys are listed
    # in cols are escaped.
    def escape_list(self, l, columns=[]):
        for row in l:
            for k in list(row.keys()):
                if type(row[k]) == str:
                    if columns == [] or k in columns:
                        # row[k] = cgi.escape(row[k], "true")
                        row[k] = html_escape(row[k], "true")
                        
                if type(row[k]) == list:
                    if columns == [] or k in columns:
                        row[k] = self.escape_list(row[k],columns=[])
        return l

    # Run cgi.escape on a single field.  To prevent errors (ie a null
    # pointer or integer), we check whether this is a string.
    def escape_single(self, e):
        if type(e) == str:
            return html_escape(e, "true")
            # return cgi.escape(e, "true")
        return e

    def rep_nbsp(self, l, columns=0):
            # Here we expect to have a list of dictionaries, or just a list 
        for row in l:
            if type(row) == dict:
                for k in list(row.keys()):
                    if type(row[k]) == str:
                        if type(columns) == list and k in columns:
                            if row[k] == '':
                                row[k] = '&nbsp;'
                        elif columns == 1:
                            if row[k] == '':
                                row[k] = '&nbsp;'

                        elif type(row[k]) == list:
                            if type(columns) == list or columns == 1:
                                row[k] = self.rep_nbsp(row[k], columns=1)
                            else:
                                row[k] = self.rep_nbsp(row[k], columns=0)

            # Here we know it was just a set of single replacements, not recursive 
            elif row == '':
                row = '&nbsp;'

        return l

    def rep_single_nbsp(self, e):
        if e == '':
            e = '&nbsp;'
        return e

    def Set(self, tag, replacement=None, setnbsp=0):
        # setnbsp is there so that you can tell it to set empty strings to &nbsp;'s.
        # If it is 0, it does nothing.  If it is 1, all empty strings are set to &nbsp's.
        if replacement is None and type(tag) == dict:
            # 'tag' is now a misnomer:  it's actually just a dictionary,
            # likely the result of a 'select *; ... FetchOneDictionary'
            tag = self.rep_nbsp([tag], setnbsp)[0]
            for k, v in list(tag.items()):
                self.tags[k] = v

        # If it is a list, it replaces just the ones in the list.
        elif type(replacement) == list:
            self.listtags[tag] = replacement
            self.listtags[tag] = self.rep_nbsp(replacement, setnbsp)

        else:
            self.tags[tag] = replacement
            if setnbsp == 1:
                self.tags[tag] = self.rep_single_nbsp(replacement)

    def SetEscape(self, tag, replacement=None, columns=[], setnbsp=0):
        # setnbsp is there so that you can tell it to set empty strings to &nbsp;'s.
        # If it is 0, it does nothing.  If it is 1, all empty strings are set to &nbsp's.
        if replacement is None and type(tag) == dict:
            # 'tag' is now a misnomer:  it's actually just a dictionary,
            # likely the result of a 'select *; ... FetchOneDictionary'
            tags = self.escape_list([tag], columns)
            tag = self.rep_nbsp(tags, setnbsp)[0]
            for k, v in list(tag.items()):
                self.tags[k] = v

        elif type(replacement) == list:
            # If it is a list, it replaces just the ones in the list.
            self.listtags[tag] = self.escape_list(replacement, columns)
            self.listtags[tag] = self.rep_nbsp(replacement, setnbsp)

        else:
            self.tags[tag] = self.escape_single(replacement)
            if setnbsp ==1:
                self.tags[tag] = self.rep_single_nbsp(replacement)

    #
    # Processes the file and returns it as a string
    #
    def Output(self):
        return self.ReplaceTags(self.ReplaceConditionals(self.DoIncludes(self.lines)))

    #
    # Processes the file and then save the output
    #
    def Save(self, name):
        file = open(name, "w")
        file.write(self.output())
        file.close()


def redirect(url):
    print("Content-type: text/html\n")
    print('<HTML><HEAD><META HTTP-EQUIV="REFRESH" CONTENT="0; URL=%s"></HEAD><BODY BGCOLOR="white"></BODY></HTML>' % url)
    sys.exit()
        
#------------------------------------------------------------------------------
# A function to write out a standard error page.  
#
# error_string - describes the error.  
# title -   Project title
# links -   Optional array of arrays.  If links=[], then  the error page 
#           will have one link, with text "Go back", and javascript  href to 
#           go back one page.
#           If links!=[], then each element in the array is an array of two 
#           elements.  element[0] is the text shown, and element[1] is the
#           linked url.
# template: The html template to use for the error page. If not defined,
#           we use base/templates/ErrorTemplate.html
# bgcolor:  The background color for the page.  White if unspecified.
# back_img: background image.  If not defined, we just use a white bg.
# head_img: head image.  If not defined, we don't show one.
# tail_img: tail image, which is a link to www.scionics.de.  Defaults to
#           base/images/scionicscopy.jpg
#------------------------------------------------------------------------------


def error_page(error_string, links=[], title='Scionics: Error!', bgcolor='#FFFFFF', template='', back_img='', head_img='', tail_img='/scionicsimages/scionicscopy.jpg'):

    if template == '':
        import os
        basedir = os.path.dirname(__file__)
        template = '%s/templates/ErrorTemplate.html' % basedir
    page = HTMLTemplate(template)

    # Set the error message.
    page.Set('ERROR_MSG', error_string)

    # Set the page title
    page.Set('TITLE', title)

    # Set the background color
    page.Set('BGCOLOR', bgcolor)

    # Set up the array of links
    if links == []:
        back_links = [{'url': 'javascript:history.go(-1)', 'txt': 'Go Back'}]
    else:
        back_links = []
        for link in links:
            back_links.append({'url': link[1], 'txt': link[0]})
    page.Set('BACK_LINKS', back_links)
                    
    # And load the images, if requested
    if back_img != '':
        page.Set('BACK_IMG', back_img)
    if head_img != '':
        page.Set('HEAD_IMG', head_img)
    if tail_img != '':
        page.Set('TAIL_IMG', tail_img)

    print("Content-type: text/html\n")
    print(page.Output())
    sys.exit()
