import logging, os, sys, time
from pickle import dump, load
from copy import deepcopy
from threading import Thread
# import init data
from pickleshare import PickleShareDB
from init import MORFEUS_USER, MORFEUS_DIR, STORE_DATA, STORE_SEQIN, STORE_BLASTOUT, STORE_CALC,STORE_RESULT, BLASTJOBS,\
PARSEJOBS, QUERYFILE, SAVEQUERY, DEF_EVALUE, CUT_EVALUE, FOLD, OPTIMA, FORMULAFILE, FORMULA,BLASTOUT_PARSER, QSPEC, HOME, MAX_CLUST
from p2_eutils import Eutilsdaemon
from daemontools import create_textfile
from clustering_test import clustering_main
from alignmentinfo_initial import get_alignmentinfo, get_blastinfo
from find_shared_hits import create_common_hits, find_common_hits, ID_synonyms, Ref_IDs
from decide_on_orthology import orthology, potential_candidates, query_paralog


def daem_parseblast(user, job_num, log=logging):
	""" Wait for the end of the blastp and read the output file it with BLASTXML and return the blast_record object """
	parsejobDB = PickleShareDB(PARSEJOBS %user)
	c = 0

	while c < 15:
		try:
			doneblast = parsejobDB[job_num]
			log.debug('Parse:' + job_num)
			time.sleep(1)						# wait for finish of blast
										# and give some time to complete xml writing, otherwise error:
										# File "/usr/local/lib/python2.6/dist-packages/Bio/Blast/NCBIXML.py", line 643, in parse
										# expat_parser.Parse("", True) # End of XML record
										# xml.parsers.expat.ExpatError: no element found: line 21, column 4
			blastout_path = os.path.join(STORE_BLASTOUT %user, doneblast)
			try:
				os.path.exists(blastout_path)
				filesize = os.path.getsize(blastout_path)
			except OSError:						# refseq id not found
				return None, doneblast
			if filesize == 0:
				log.debug("daem_parseblast: File not done yet: " + job_num)
				continue
			else:							# check for complet transfer of BLAST XML output file
				BLAST_XML_END = b"</BlastOutput>"		# No EOL for last line!
				filecheck_pos = len(BLAST_XML_END)
				filecheck_file = open(blastout_path, "rb")
				#filecheck_file.seek(-(len(BLAST_XML_END)),2)
				filecheck_file.seek(-(len(BLAST_XML_END)+2), 2) # correct the bug where he can't find the result. + 2 for BLAST 2.8.1 and +1 for BLAST 2.2.19
				possibleEnd = filecheck_file.read(filecheck_pos)
				if possibleEnd == BLAST_XML_END:
					log.debug("File complete: " + job_num)
				else:
					log.debug("File not complete yet: " + job_num)
					c += 1
					continue

			result_handle = open(blastout_path)
			blast_records = BLASTOUT_PARSER.parse(result_handle)	

			for record in blast_records: 				# get data always from the last record
				blastrecord = record				# important case for getting data from last iteration of psiblast

			log.debug("BLAST XML output is parsed !\n")
			del parsejobDB[job_num]
			# The last true is to inform of the success of the parsing
			return blastrecord, doneblast, True
		except KeyError:
			c += 1
			log.debug('daem_parseblast: I am waiting for BLAST return' + job_num)
			time.sleep(1)
	# return juste a false so morfeus_run can relaunch the blast
	return [], [], False

def morfeus_run(userID=MORFEUS_USER, queryID=None, querySeq=None, matrix=OPTIMA,\
		e_value=DEF_EVALUE, skip=False, HighThroughPut=False, lokal=False, graph=False, log=logging):
	try:
		# Create folder if not present, warning else
		for userFolder in (os.path.join(STORE_DATA, '%s'), STORE_SEQIN, STORE_BLASTOUT, STORE_CALC, STORE_RESULT):
			if not os.path.exists(userFolder %userID):
				os.mkdir(userFolder %userID)
			else:
				log.warning("Folder already exists: " + userFolder %userID)
	except OSError:
		# Crash if cannot create the folder
		log.critical("Could not create user folder: " + userFolder %userID)
		sys.exit(-1)

	# create user owned blast + parse queues
	blastjobDB = PickleShareDB(BLASTJOBS %userID); blastjobDB.clear()
	parsejobDB = PickleShareDB(PARSEJOBS %userID); parsejobDB.clear()

	# create BlastThread

	if lokal:
		from p1_blasting import Queue_Local_blast as Blast  # Queue_NetSSH_blast as Blast
	else:
		from p1_blasting import Queue_Net_blast as Blast

	class BlastThread(Blast, Thread):
		def __init__(self):
			Blast.__init__(self)
			Thread.__init__(self)

	blast_thread = BlastThread()
	blast_thread.daemon = True
	blast_thread.start()

	# Create file that contains the mathformula how to cut the tree
	create_textfile(os.path.join(STORE_CALC %userID, FORMULAFILE), FORMULA)

	# initialization
	done = set([])
	blast_main_info = {}
	IDs_globalblast = {}
	Reference_IDs = {}
	bb_storageIds = {}
	# initial BLAST
	parse_work = False
	while parse_work == False:
		# Update "to do lists"
		if queryID:
			# initial blast job
			blastjobDB['0'] = queryID
		elif querySeq:
			# print "creating query", SAVEQUERY
			create_textfile(SAVEQUERY % userID, querySeq)
			# initial blast job
			blastjobDB['0'] = QUERYFILE

		blast_record, doneID, parse_work = daem_parseblast(userID, '0', log)

	if not blast_record:  # refseq query id not found
		return None
	done.add('0')
	# initial blast info


	blast_main_info['0'] = get_blastinfo(blast_record, add_ncbi=True)  # try with false

	num_hits = len(blast_main_info['0'])
	if num_hits >= MAX_CLUST:
		HighThroughPut.batch = True
		log.info("Switched off clustering" + str(num_hits))
	# initial storage ID
	bb_storageIds['0'] = doneID
	# prepare query seq for clustering
	queryFilename = os.path.join(STORE_SEQIN % userID, doneID)
	queryFile = open(queryFilename, "r")
	query = queryFile.read().rstrip().replace('\n', '')
	queryFile.close()
	# alignment for clustering
	bin_codiert = get_alignmentinfo(queryFilename, blast_record)
	# do clustering and store results for re-run
	cResults = os.path.join(STORE_CALC % userID, "cResults.pickle")
	if not HighThroughPut.batch:
		if skip:
			if os.path.exists(cResults):  # skip clustering
				score_matches, distance, membership, tree_Michael = load(open(cResults))
				log.info("Skip clustering")
			else:
				distance, score_matches, membership = {}, {}, {}
		else:

			score_matches, distance, membership, tree_Michael, tree_NJPLOT =\
			clustering_main(blast_main_info['0'], bin_codiert, query, matrix, STORE_CALC % userID, log=log)

			log.debug('Those subclusters had been defined:')
			for key in list(membership.keys()):
				log.debug(key)
				log.debug(membership[key])
			cResultFile = open(cResults, 'wb')
			dump((score_matches, distance, membership, tree_Michael), cResultFile)
			cResultFile.close()
			cTree = os.path.join(STORE_CALC % userID, 'tree_NJPLOT.txt')
			cTreeFile = open(cTree, 'w')
			cTreeFile.write(tree_NJPLOT)
			cTreeFile.close()

	# empty initial common hits list, orthologs & uncertain
	common_hits = create_common_hits(blast_main_info['0'])
	ortholog_ = set(['0'])
	uncertain_ortholog_ = set([])
	# lookup paralogs
	paralog_, query_identical_sequences = query_paralog(blast_main_info['0'], ortholog_)

	# initial synonyms
	IDs_globalblast['0'] = ID_synonyms(blast_main_info['0'])
	Reference_IDs['0'] = Ref_IDs(blast_main_info['0'])
	# initial common hits
	common_hits, blast_main_info, IDs_globalblast, Reference_IDs = 	\
	find_common_hits(common_hits, blast_main_info, IDs_globalblast,
			Reference_IDs, '0', CUT_EVALUE, FOLD, query_identical_sequences)

	if paralog_:
		jo = list(paralog_)[0]
		print(jo)
		blastdata = [jo, list(blast_main_info['0'][jo][0])[0]]
		parse_work = False
		while not parse_work:
			blastjobDB[blastdata[0]] = blastdata[1]
			log.debug('Added blastdata: ' + str(blastdata))
			blast_record, doneblast, parse_work = daem_parseblast(userID, jo)
		done.add(jo)
		blast_main_info[jo] = get_blastinfo(blast_record)
		IDs_globalblast[jo] = ID_synonyms(blast_main_info[jo])
		Reference_IDs[jo] = Ref_IDs(blast_main_info[jo])

		common_hits, blast_main_info, IDs_globalblast, Reference_IDs = 	\
		find_common_hits(common_hits, blast_main_info, IDs_globalblast,
				Reference_IDs, jo, CUT_EVALUE, FOLD, query_identical_sequences)

	if len(ortholog_) > 1:
		for jo in list(ortholog_):
			if jo != '0':	break					# what if there are more than just one additional ???? (no backblast in that case !!!)
		blastdata = [jo, list(blast_main_info['0'][jo][0])[0]]
		parse_work = False
		while not parse_work:
			blastjobDB[blastdata[0]] = blastdata[1]
			log.debug('Added blastdata: ' + str(blastdata))
			blast_record, doneblast, parse_work = daem_parseblast(userID, jo)
		done.add(jo)
		blast_main_info[jo] = get_blastinfo(blast_record)
		IDs_globalblast[jo] = ID_synonyms(blast_main_info[jo])
		Reference_IDs[jo] = Ref_IDs(blast_main_info[jo])

		common_hits, blast_main_info, IDs_globalblast, Reference_IDs =  \
		find_common_hits(common_hits, blast_main_info, IDs_globalblast,
				Reference_IDs, jo, CUT_EVALUE, FOLD, query_identical_sequences)

	if not HighThroughPut.batch:
		for id in list(blast_main_info['0'].keys()):
			distance[id] = blast_main_info['0'][id][2]
	else:
		for id in list(blast_main_info['0'].keys()):
			distance[id] = blast_main_info['0'][id][2]
			membership[id] = [id]
			for id1 in list(blast_main_info['0'].keys()):
				if id != id1:	score_matches[(id, id1)] = 0

	ortholog, uncertain_ortholog, paralog = orthology(common_hits, distance, score_matches, ortholog_,
													  uncertain_ortholog_, paralog_, done)
	tobe_parsed = potential_candidates(ortholog, common_hits, distance, score_matches, membership, done)

	# initial dendrogram generation
	if not HighThroughPut.batch:
		if graph:
			from tree import TreeImage
			initialHits = [list(blast_main_info['0'][hit][0])[0] for hit in tobe_parsed]
			treeImg = TreeImage(tree_Michael, initialHits, [])

			treeFile = os.path.join(os.getcwd(), HOME, userID+"_tree_initial"+".png")
			treeImg.save(treeFile)

	while len(tobe_parsed) > 0:
		log.debug('Orthos so far: ' + str(ortholog))
		log.debug('Uncertains so far: ' + str(uncertain_ortholog))
		log.debug('Paras so far: ' + str(paralog))
		log.info('No. of seqs blasted so far %02d of %04d' % (len(done), len(list(bin_codiert.keys()))))
		try:
			actual_jobs = tobe_parsed[:QSPEC]			# take QSPEC first jobs
		except IndexError: 						# no job at the end
			actual_jobs = tobe_parsed[:] 				# last jobs
			#break							# no need to continue
		print('I will do now', tobe_parsed)

		# filling blast queue
		for job in actual_jobs:
			blastdata = [job, list(blast_main_info['0'][job][0])[0]]
			blastjobDB[blastdata[0]] = blastdata[1]
			log.debug('Added blastdata: ' + str(blastdata))


		for job in actual_jobs[0:QSPEC]:
			done.add(job)

			if job not in tobe_parsed:
				continue

			parse_work = False
			while not parse_work:
				blast_record, doneblast, parse_work = daem_parseblast(userID, job)
				# If the parsing get in a loop, relaunch the blast
				if not parse_work:
					blastdata = [job, list(blast_main_info['0'][job][0])[0]]
					blastjobDB[blastdata[0]] = blastdata[1]
					log.debug('Added blastdata: ' + str(blastdata))

			blast_main_info[job] = get_blastinfo(blast_record)
			bb_storageIds[job] = doneblast
			IDs_globalblast[job] = ID_synonyms(blast_main_info[job])
			Reference_IDs[job] = Ref_IDs(blast_main_info[job])
			common_hits, blast_main_info, IDs_globalblast, Reference_IDs = 	\
			find_common_hits(common_hits, blast_main_info, IDs_globalblast,
							 Reference_IDs, job, CUT_EVALUE, FOLD, query_identical_sequences)
			log.debug('Just blasted ' + str(job) + str(common_hits[job]))
			ortholog, uncertain_ortholog, paralog = orthology(common_hits, distance, score_matches, ortholog_,
															  uncertain_ortholog_, paralog_, done, job)
			tobe_parsed = potential_candidates(ortholog, common_hits, distance, score_matches, membership, done)

	ortholog.update(query_identical_sequences)
	if not HighThroughPut.batch:
		result_dict = \
			[ortholog, paralog, uncertain_ortholog, blast_main_info, bb_storageIds, common_hits, tree_Michael, done]
		result_paths = \
			['ortholog', 'paralog', 'uncertain_ortholog', 'blast_main_info', 'bb_storageIds', 'common', 'tree_Michael', 'done']
	else:
		result_dict = \
			[ortholog, paralog, uncertain_ortholog, blast_main_info, bb_storageIds, common_hits, done]
		result_paths = \
			['ortholog', 'paralog', 'uncertain_ortholog', 'blast_main_info', 'bb_storageIds', 'common', 'done']
	for entry in range(len(result_dict)):
		fh = open(os.path.join(STORE_RESULT % userID, result_paths[entry]), 'wb')
		dump(result_dict[entry], fh)
		fh.close()
	log.info('Ortholog' + str(ortholog))
	log.info('Paralog' + str(paralog))
	log.info('Uncertain' + str(uncertain_ortholog))
	log.info('Number of backblasts: ' + str(len(done)) + '.... out of ' + str(len(list(blast_main_info['0'].keys()))) + 'total candidates')
	for id in done:
		log.debug('\n'+str(id) + str(common_hits[id]) + str(blast_main_info['0'][id])+'\n')
	return result_dict
