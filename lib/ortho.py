import orthology
import pickle

prev = pickle.load(open("before_find_them", "rb"))

orthol, uncertain_orthol, paral, dictionary, scores1, scores, exclude, done = [e for e in prev]

# Convert the set in list for a conversion in vector in c++ via pybind11
orthol = list(orthol)
uncertain_orthol = list(uncertain_orthol)
paral = list(paral)
done = list(done)
for k in list(dictionary.keys()):
    dictionary[k]['best'] = list(dictionary[k]['best'])
    dictionary[k]['acceptable'] = list(dictionary[k]['acceptable'])
    dictionary[k]['exclude'] = list(dictionary[k]['exclude'])


print("orthol", orthol)
print("uncertain_orthol", uncertain_orthol)
print("paral", paral)
print("done", done)
print("len(done)", len(done))
print(dictionary['0']['best'])
print(dictionary['0']['acceptable'])



# find_them use only the part of score1 using simple string as key and not tuple so
# this loop extract this
distance_for_find_them = {}
to_save = len(scores1) - len(scores)
for i in range(to_save):
    distance_for_find_them[str(i)] = scores1[str(i)]

print(scores1['1'])
ortho, uncertain_ortho, para, schalter = orthology.find_them(orthol, uncertain_orthol, paral, dictionary, distance_for_find_them, scores, done)
print(len(ortho))
print(ortho, uncertain_ortho, para, schalter)
