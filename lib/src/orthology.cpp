//
// Created by akusem on 2/18/19.
//

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py = pybind11;

#include <map>
#include <tuple>
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


// print elements of an STL container
template <typename T>
void printCont (T const& coll, string message = "")
{
	typename T::const_iterator pos;  // iterator to iterate over coll
	typename T::const_iterator end(coll.end());  // end position

	if (message != "")
		cout << message << endl;
	for (pos=coll.begin(); pos!=end; ++pos) {
		std::cout << *pos << ' ';
	}
	std::cout << std::endl;
}


vector<string> intersection(vector<string> &v1, vector<string> &v2){
    vector<string> v3;

    sort(v1.begin(), v1.end());
    sort(v2.begin(), v2.end());

    set_intersection(v1.begin(),v1.end(),
                          v2.begin(),v2.end(),
                          inserter(v3, v3.begin()));
    return v3;
}

bool sort_all_score_cand(tuple<double, double, string, string>& a,
               tuple<double, double, string, string>& b)
{
	int a2 = stoi(get<2>(a)), b2 = stoi(get<2>(b));
    if (a2 < b2) return true;
    else if (get<2>(a) == get<2>(b))  return get<1>(a) < get<1>(b);
    else return false;
}

bool sort_score_treffer(tuple<double, double, string, string>& a,
                         tuple<double, double, string, string>& b)
{
	int a2 = stoi(get<2>(a)), b2 = stoi(get<2>(b));
    if (a2 < b2) return false;
    else if (get<2>(a) == get<2>(b)) return get<1>(a) > get<1>(b);
    else return true;
}

vector<string> excluded_(vector<string>& dict, map<string, map<string, vector<string>>>& dictionary) {
    vector<string> toReturn;
    for (const auto& d: dict) {
        vector<string> temp = dictionary[d]["exclude"];
        toReturn.insert(toReturn.end(), temp.begin(), temp.end());
    }

    // Erase duplicate
	sort(toReturn.begin(), toReturn.end());
	toReturn.erase(unique(toReturn.begin(), toReturn.end()), toReturn.end());

    return toReturn;
}

tuple<vector<string>, vector<string>, vector<string>, string> find_them(vector<string> ortholog,
        vector<string> uncertain_ortholog,
        vector<string> paralog,
        map<string, map<string, vector<string>>> dictionary,
        map<string, double> scores1,
        map<pair<string, string>, double> scores,
        vector<string> done) {

    vector<tuple<double, double, string, string>> all_score_cand;
    string schalter = "no";
    for(const string& ortho: ortholog) {
        vector<string> best_hit_cands = dictionary[ortho]["best"];
        vector<string> hit_cands = dictionary[ortho]["acceptable"];
        vector<string> for_intersect = best_hit_cands;
        for_intersect.insert(for_intersect.end(), hit_cands.begin(), hit_cands.end());
        vector<string> cands = intersection(done, for_intersect);

        // Extract the scores1 of each cands and put it in a vector<double>
        vector<double> scores1_best_hit_cands(cands.size());
        int i = 0;
        for (const auto& cand: cands) {
            scores1_best_hit_cands[i] = scores1[cand];
            i++;
        }

        // Extract scores of each (ortho, cands) and put it in a vector<double>
        vector<double> scores_best_hit_cands(cands.size());
        i = 0;
        for (auto& cand: cands) {
            int t_ortho = stoi(ortho);
            int t_cand = stoi(cand);
            scores_best_hit_cands[i] = -1*scores[make_pair(to_string(min(t_ortho, t_cand)), to_string(max(t_ortho, t_cand)))];
            i++;
        }

        // suppress it and replace score_cand by all_score_cand
        vector<tuple<double, double, string, string>> score_cand(scores_best_hit_cands.size());
        for (unsigned int i = 0; i < scores_best_hit_cands.size(); i++) {
            score_cand[i] = make_tuple(scores1_best_hit_cands[i], scores_best_hit_cands[i], cands[i], ortho);
        }

        all_score_cand.insert(all_score_cand.end(), score_cand.begin(), score_cand.end());

    }

    sort(all_score_cand.begin(), all_score_cand.end(), sort_all_score_cand);

    // Declare variable here to be in the function scope
    vector<string> ortho_best_cand;
    vector<string> para_best_cand;
    vector<string> cand_best_orthos;
    vector<string> cand_acceptable_orthos;
    vector<string> cand_best_paras;

    // Determine if this candidate is a symmetrical_best_ortholog or a  symmetrical_best_paralog
    for (const auto& cand: all_score_cand) {
        // Clear the value between loop
        ortho_best_cand.clear();
        para_best_cand.clear();
        cand_best_orthos.clear();
        cand_acceptable_orthos.clear();
        cand_best_paras.clear();

        string symmetrical_best_ortholog, symmetrical_best_paralog;
        string c2 = get<2>(cand);

        //cout << "cand: " << c2 << " " << get<3>(cand) << endl;
//		cout << "cand: " << c2 << " " << get<3>(cand) << ' ' <<
//		(find(ortholog.begin(), ortholog.end(), c2) == ortholog.end()) << ' ' <<
//		(find(paralog.begin(), paralog.end(), c2) == paralog.end() ) << endl;

        if (find(ortholog.begin(), ortholog.end(), c2) == ortholog.end() &&
			find(paralog.begin(), paralog.end(), c2) == paralog.end())  {

			// working for the 3
            cand_best_orthos = intersection(dictionary[c2]["best"], ortholog);
            cand_acceptable_orthos = intersection(dictionary[c2]["acceptable"], ortholog);
            cand_best_paras = intersection(dictionary[c2]["best"], paralog);


            for (const auto& ortho: cand_best_orthos) {
                if (find(dictionary[ortho]["best"].begin(), dictionary[ortho]["best"].end(), c2)
                        != dictionary[ortho]["best"].end())
                    ortho_best_cand.push_back(ortho);
            }

            for (const auto& para: cand_best_paras) {
                if (find(dictionary[para]["best"].begin(), dictionary[para]["best"].end(), c2)
                    != dictionary[para]["best"].end())
                    para_best_cand.push_back(para);
            }

            vector<string> t_excluded = excluded_(ortholog, dictionary);
            if (ortho_best_cand.size() > 0 && find(t_excluded.begin(), t_excluded.end(), c2) == t_excluded.end()
                    && intersection(ortholog, dictionary[c2]["exclude"]).size() == 0) {

                symmetrical_best_ortholog = c2;
//				cout << "symmetrical_best_ortho " << symmetrical_best_ortholog << endl;

            }

            t_excluded = excluded_(paralog, dictionary);
            if (para_best_cand.size() > 0 && find(t_excluded.begin(), t_excluded.end(), c2) == t_excluded.end()
                    && intersection(paralog, dictionary[c2]["exclude"]).size() == 0) {

                symmetrical_best_paralog = c2;
//				cout << "symmetrical_best_paralog " << symmetrical_best_paralog << endl;

            }
        }

        // if best ortho and best para
        if (symmetrical_best_ortholog.empty() == false && symmetrical_best_paralog.empty() == false) {
            vector<string> treffer_all;
            treffer_all.insert(treffer_all.end(), ortho_best_cand.begin(), ortho_best_cand.end());
            treffer_all.insert(treffer_all.end(), para_best_cand.begin(), para_best_cand.end());


            // Extract the scores1 of each treffer and put it in a vector<double>
            vector<double> scores1_treffer(treffer_all.size());
            int i = 0;
            for (const auto& treff: treffer_all) {
                scores1_treffer[i] = scores1[treff];
                i++;
            }

            // Extract scores of each (treff, symmetrical_best_hit) and put it in a vector<double>
            vector<double> scores_treffer(treffer_all.size());
            i = 0;
            int t_sym_ortho = stoi(symmetrical_best_ortholog);
            for (auto& treff: treffer_all) {
                int t_treff = stoi(treff);
                scores_treffer[i] = scores[make_pair(to_string(min(t_treff, t_sym_ortho)), to_string(max(t_treff, t_sym_ortho)))];
                i++;
            }

            // all_score_treffer correspond to score_treffer
            vector<tuple<double, double, string, string>> score_treffer(scores_treffer.size());
            for (int i = 0; i < (int)scores_treffer.size(); i++) {
                score_treffer[i] = make_tuple(scores1_treffer[i], scores_treffer[i], symmetrical_best_ortholog, treffer_all[i]);
            }

            sort(score_treffer.begin(), score_treffer.end(), sort_score_treffer);


			if (find(ortho_best_cand.begin(), ortho_best_cand.end(), "0") != ortho_best_cand.end()) {
//				cout << "going to expend ortholog with " << symmetrical_best_ortholog << " if 0 in orthoBestCand" << endl;
				ortholog.push_back(symmetrical_best_ortholog);
				vector<string>::iterator fi = find(uncertain_ortholog.begin(), uncertain_ortholog.end(), symmetrical_best_ortholog);
				if (fi != uncertain_ortholog.end())
					uncertain_ortholog.erase(fi);
				schalter = "yes";

			} else if (find(cand_acceptable_orthos.begin(), cand_acceptable_orthos.end(), "0") != cand_acceptable_orthos.end()) {
				paralog.push_back(symmetrical_best_paralog);
				vector<string>::iterator fi = find(uncertain_ortholog.begin(), uncertain_ortholog.end(), symmetrical_best_ortholog);
				if (fi != uncertain_ortholog.end())
					uncertain_ortholog.erase(fi);

			} else {
				if (find(paralog.begin(), paralog.end(), get<3>(score_treffer[0])) != paralog.end()) {
					paralog.push_back(symmetrical_best_paralog);
					vector<string>::iterator fi = find(uncertain_ortholog.begin(), uncertain_ortholog.end(), symmetrical_best_paralog);
					if (fi != uncertain_ortholog.end())
						uncertain_ortholog.erase(fi);
				}

				if (find(ortholog.begin(), ortholog.end(), get<3>(score_treffer[0])) != ortholog.end()) {
//					cout << "going to expend ortholog with " << symmetrical_best_ortholog << " if score_treffer[0][3] in ortho" << endl;
					ortholog.push_back(symmetrical_best_ortholog);
					vector<string>::iterator fi = find(uncertain_ortholog.begin(), uncertain_ortholog.end(), symmetrical_best_ortholog);
					if (fi != uncertain_ortholog.end())
						uncertain_ortholog.erase(fi);
					schalter = "yes";
					break;
				}
			}

        } else if (symmetrical_best_ortholog.empty() == false && symmetrical_best_paralog.empty() == true) {
            // insert symmetrical_best_ortholog in ortholog and if present in uncertain_ortholog erase it
//            cout << "going to expend ortholog with " << symmetrical_best_ortholog << " if symBestOrtho" << endl;
            ortholog.push_back(symmetrical_best_ortholog);
            vector<string>::iterator fi = find(uncertain_ortholog.begin(), uncertain_ortholog.end(), symmetrical_best_ortholog);
            if (fi != uncertain_ortholog.end())
                uncertain_ortholog.erase(fi);
            schalter = "yes";
            break;

        } else if (symmetrical_best_ortholog.empty() == true && symmetrical_best_paralog.empty() == false) {
            // insert symmetrical_best_paralog in paralog and if present in uncertain_ortholog erase it
            paralog.push_back(symmetrical_best_paralog);
            vector<string>::iterator fi = find(uncertain_ortholog.begin(), uncertain_ortholog.end(), symmetrical_best_paralog);
            if (fi != uncertain_ortholog.end())
                uncertain_ortholog.erase(fi);
        }

        // if cand[2] not in ortholog and paralog
        if (find(ortholog.begin(), ortholog.end(), c2) == ortholog.end()  &&
			find(paralog.begin(), paralog.end(), c2) == paralog.end()) {

            if (cand_best_orthos.size() + cand_acceptable_orthos.size() >= 1 and cand_best_paras.size() == 0) {
                if (find(uncertain_ortholog.begin(), uncertain_ortholog.end(), c2) == uncertain_ortholog.end()) {
                    uncertain_ortholog.push_back(c2);
                }
            }
        }
    }

    return make_tuple(ortholog, uncertain_ortholog, paralog, schalter);
}

tuple<vector<string>, vector<string>, vector<string>> schalter(vector<string> ortholog,
														   vector<string> uncertain_ortholog,
														   vector<string> paralog,
														   map<string, map<string, vector<string>>> dictionary,
														   map<string, double> scores1,
														   map<pair<string, string>, double> scores,
														   vector<string> done) {
	vector<string> save_paralog = paralog;
	vector<string> save_uncertain_ortholog = uncertain_ortholog;
	string schalter;
	tuple<vector<string>, vector<string>, vector<string>, string> res;
//    auto now = chrono::system_clock::now();
//    long ms = chrono::time_point_cast<chrono::milliseconds>(now).time_since_epoch().count();;
//    cout << "Time conversion: " << (ms-time)/1000 << "s" << endl;

	do {
		uncertain_ortholog = save_uncertain_ortholog;
		paralog = save_paralog;

		res = find_them(ortholog, uncertain_ortholog, paralog, dictionary, scores1, scores, done);

		ortholog = get<0>(res);
		schalter = get<3>(res);
	} while (schalter == "yes");

	return make_tuple(get<0>(res), get<1>(res), get<2>(res));

}

PYBIND11_MODULE(orthology, m) {
    m.doc() = "re-implementation of the find_them function from morfeus";

    m.def("find_them", &find_them, "Determine orthology",
    py::arg("ortholog"), py::arg("uncertain_ortholog"), py::arg("paralog"),
    py::arg("dictionary"), py::arg("scores1"), py::arg("scores"), py::arg("done"));

    m.def("schalter", &schalter, "Determine orthology, loop around find_them",
	py::arg("ortholog"), py::arg("uncertain_ortholog"), py::arg("paralog"),
	py::arg("dictionary"), py::arg("scores1"), py::arg("scores"), py::arg("done"));
}
