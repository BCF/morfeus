#!/usr/bin/env python

import shelve
from inspect import getargspec
from time import strftime

REC_FILE = 'clustering.fio'


def record_start(fname = REC_FILE):
    
    global rec_file
    rec_file = shelve.open(fname)
    
def record_stop():
    global rec_file
    rec_file.close()

def record(f):
    "records function parameters "
    def wrapped(*params, **kparams):

        #rec_file = shelve.open('clustering.fio')    
        fname = f.__name__
       
        if fname not in rec_file:
            
            rec_file[fname] = strftime("%m%d%H%M")
            pnames = getargspec(f)[0] # "normal" parameters
            print(pnames)
            for i, name in enumerate(pnames):
                key = fname + '_in_' + name
                rec_file[key] = params[i]
            
            print(("Run " + fname))
            results =  f(*params, **kparams)
            
            if not isinstance(results,tuple): # single return value, make tuple            
                output = (results,)
                print("Single value: ")
            else:
                output = results
                
            for (i,value) in enumerate(output):
                key = fname + '_out_' + repr(i)
                print(("Save " + key))
                rec_file[key] = value
                
            #rec_file.close()
            
            return results
            
        else:
            #rec_file.close()
            return f(*params, **kparams)
        
    return wrapped
       
    
    
def replay(f, rec_file):
    
  
    fname = f.__name__
   
    if fname in rec_file:

        param = []
        pnames = getargspec(f)[0] # "normal" parameters
        print(pnames)
        for name in pnames:
            key = fname + '_in_' + name
            param.append(rec_file[key])
       
        print(("Run " + fname))
        return_list = f(*param)
        
        if not isinstance(return_list,tuple): # single return value
            return_list = (return_list,)
            print("Single value: ")
            
        for (i,value) in enumerate(return_list):
            
            key = fname + '_out_' + repr(i)
            stored = rec_file[key]
            
            #if isinstance(stored, dict):  # key order doesn't match
            #    value = value.items();value.sort()
            #    stored = stored.items();stored.sort()
            
            if value == stored:
                print(("Match return " + repr(i))) 
            else:
                print(("Mismatch return " + repr(i)))
                
        return return_list
                


if __name__ == '__main__':
    
    import clustering as test

    function = (test.clustering_main, test.combinations, test.determine_home_clusters,
            test.find_maximum, test.get_new_cut, test.get_new_distance, test.get_new_matrix, test.make_tree_NJPLOT,
            test.remove_distance, test.store_level_cluster_info, test.store_position_info, test.store_tree_data)
    
    rec_file = shelve.open('clustering.fio')
    for f in function:
        replay(f, rec_file)
    
    rec_file.close()
    
    