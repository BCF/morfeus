#!/usr/bin/env python

# based on
# Programming Collective Intelligence by Toby Segaran

import pickle
#import Image, ImageDraw             # PIL 1.1.7 (MacOS X)
from PIL import Image ,ImageDraw

class TreeImage:

    def __init__(self, tree, o=[], p=[]):
        self.tree = tree
        self.oHits = o
        self.pHits = p
        self.root = max(self.tree.keys())
        self.h = self._getheight(self.root)*24+50
        self.w = 1200
        self.depth = self._getdepth(self.root)
        self.scaling = float(self.w-250)/self.depth
        self.img = Image.new('RGB',(self.w,self.h),(255,255,255))
        self.draw = ImageDraw.Draw(self.img)
         # Draw the first line
        self.draw.line((0,self.h/2,10,self.h/2),fill=(0,0,0))
        # Draw the first node
        print(self.root)
        self._drawnode(self.root,10,self.h/2)
    
    def save(self, filename='TreeImage.png'):
        self.img.save(filename,'PNG')
    
    def _getdepth(self, node):
        try:
            left = self.tree[node][0][0] # left subnode id
            right = self.tree[node][0][1] # right subnode id
            dist = self.tree[node][1] # distance
        except KeyError: # Node is already endnode 
            return 0
        return max(self._getdepth(left), self._getdepth(right)) + dist # decision for longer subtree

    def _getheight(self, node):
        #print node
        try:
            left = self.tree[node][0][0] # left subnode id
            right = self.tree[node][0][1] # right subnode id
        except KeyError: # Node is already endnode 
            return 1
        return self._getheight(left) + self._getheight(right)

    
    # The important function here is drawnode, which takes a cluster and its location.
    # It takes the heights of the child nodes, calculates where they should be,
    # and draws lines to them one long vertical line and two horizontal lines.
    # The lengths of the horizontal lines are determined by how much error is in the cluster.
    # Longer lines show that the two clusters that were merged to create the cluster weren't all that similar,
    # while shorter lines show that they were almost identical. 
    
    def _drawnode(self, node, x,y):
        try:
            left = self.tree[node][0][0] # left subnode id
            right = self.tree[node][0][1] # right subnode id
            dist = self.tree[node][1] # distance
        except KeyError:
            return
        
        h1 = self._getheight(left)*20
        h2 = self._getheight(right)*20
        top = y-(h1+h2)/2
        bottom = y+(h1+h2)/2
        # Line length
        ll=dist*self.scaling
    
        # Vertical line from this cluster to children
        self.draw.line((x,top+h1/2,x,bottom-h2/2),fill=(0,0,0))
        # Horizontal line to left item
        self.draw.line((x,top+h1/2,x+ll,top+h1/2),fill=(0,0,0))  
        # Horizontal line to right item
        self.draw.line((x,bottom-h2/2,x+ll,bottom-h2/2),fill=(0,0,0))
        # Call the function to draw the left and right nodes
        self._drawnode(left,x+ll,top+h1/2)
        self._drawnode(right,x+ll,bottom-h2/2)
        
        if type(left) is str or type(left) is str:   #  plain + xml 
            self.draw.text((x+ll+5,top+h1/2-5),left,self._getcolor(left)) # left endpoint
            
        if type(right) is str or type(right) is str:  # plain + xml
            self.draw.text((x+ll+5,bottom-h2/2-5),right,self._getcolor(right)) # right endpoint
            
    def _getcolor(self, node):
        seqid = node.split()[0]
        if seqid in self.oHits:
            color = (255,0,0)    
        elif seqid in self.pHits:
            color = (0,0,255)
        else:
            color = (0,0,0)
        return color
        

if __name__ =='__main__':

    tree = pickle.load(open('tree_Michael'))
#    hits = pickle.load(open('bb_storageIds'))
#    orthos = pickle.load(open('ortholog'))
#    paras = pickle.load(open('paralog'))
    
#    oHits = [hits[o] for o in orthos]
    
    #pHits = []
    #for p in paras:
    #    pHits.append(hits.get(p))
    
    dHits=[]
    blast_main_info = pickle.load(open('blast_main_info'))
    done = pickle.load(open('done'))
    for id in done:
        seqID = list(blast_main_info['0'][id][0])
        dHits.extend(seqID)
    
    treeImg = TreeImage(tree, dHits, [])
    treeImg.save()
