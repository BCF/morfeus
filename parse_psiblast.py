#!/usr/bin/env python

import os, sys
from init import NBLASTDATABASE, DEF_EVALUE, EVAL_INCLUSION, MSA_TYPE, PSI_PROCESS,MSA_SEQ, PSI_ITERATIONS, MAILSERVER,MORFEUS_USER
import subprocess, io, Bio
from Bio import Entrez,AlignIO
mail_domain = MAILSERVER.split('.',1)[-1] 					# mail.age.mpg.de -> age.mpg.de
Entrez.email = '@'.join([MORFEUS_USER, mail_domain])
from Bio.Blast import NCBIXML
from Bio.Blast.Applications import NcbipsiblastCommandline
from Bio.Align import AlignInfo
from Bio.Align.Applications import * #MuscleCommandline

def FetchFasta(queryID):
    if not os.path.exists(queryID+".fasta"):
        fasta_file = open(queryID+".fasta",'w')
        fastaEntry = Entrez.efetch(db="protein", id=queryID, rettype="fasta").read()
        fasta_file.write(fastaEntry)
    else:
        print(("Skip:", queryID))

def psiBlastManual(queryID): #If Manual option is chosen, the first BLAST gives back the result in XML format for MSA
    psiblastout = queryID+"_iteration1.xml"
    if not os.path.exists(psiblastout):
        blast_exe = 'psiblast'
        EVALUE_THRESH = 1
        psi_input = queryID+".fasta"
        psipssm = queryID+"_pssm"
        blast_db = 'nr'
        psi_cline = NcbipsiblastCommandline('psiblast',evalue =EVALUE_THRESH,db =blast_db,\
        				    query = queryID+".fasta",outfmt = 5, out = queryID+"_iteration1.xml") #1st BLAST by PSI is Standard BLAST
        p = subprocess.Popen(str(psi_cline),stdout=subprocess.PIPE, shell=(sys.platform!="win32"))
        for line in p.stdout:
            line()
            print(psi_cline)
    print("PSI-BLAST XML file is created !")
    
def blastParser(queryfile,queryID): #XML parser for 1st BLAST output
    id_expect_score = {}
    XMLdataParser = NCBIXML.parse(queryfile)
    psihit_file = open(queryID+'_psihits.txt','w')
    for record in XMLdataParser:
        if record.alignments:
            for align in record.alignments:
                for hsp in align.hsps:
                    sequence_det = align.title
                    columns = sequence_det.split("|")
                    access_id = columns[5]
                    id_expect_score[access_id]= (hsp.expect,hsp.score)
                    psihit_file.write('%s\t%s\t\t%s\n'%(access_id,hsp.expect,hsp.score))
                    #print '****Alignment****\nAccession id :%s\tE-value:%s\t score:%s'%(access_id, hsp.expect, hsp.score)

def method_msa(queryID, in_file): # MSA of the sequences from 1st BLAST
    print("Multiple alignment of sequence from 1st BLAST! default method is muscle, other options available : mafft, clustalw, tcoffee!")
    msa_method = MSA_TYPE #select in init.py default is muscle
    try:
        if msa_method == 'muscle':
            cline = MuscleCommandline(input=in_file)
            return_code = subprocess.Popen(str(cline),stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=(sys.platform!="win32"))
            alignment = AlignIO.read(return_code.stdout, "fasta")
        elif msa_method == 'mafft':
            cline = MafftCommandline(input = in_file)
            stdout, stderr = cline()
            outfile = open(queryID+"_mafft.fasta", "w")
            outfile.write(stdout)
            outfile.close()
            alignment = AlignIO.read(open(queryID+"_mafft.fasta"), "fasta")
        elif msa_method == 'tcoffee':
            cline = TCoffeeCommandline(infile = in_file, outfile = queryID+"_tcoffee.aln",output="clustalw")
            return_code = subprocess.call(str(cline), shell=(sys.platform!="win32"))
            alignment = AlignIO.read(open(queryID+"_tcoffee.aln"), "clustal")
        elif msa_method == 'clustal':
            cline = ClustalwCommandline(infile = in_file, outfile = queryID+"_clustal.aln")
            return_code = subprocess.call(str(cline), shell=(sys.platform!="win32"))
            alignment = AlignIO.read(open(queryID+"_clustal.aln"), "clustal")
        else :
            cline = MuscleCommandline(input=in_file)
            return_code = subprocess.Popen(str(cline),stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=(sys.platform!="win32"))
            alignment = AlignIO.read(return_code.stdout, "fasta")
    except: #strange problem in NP_012345 xml file, but the exception seems to work
            print("ValueError occured with the given MSA method. muscle is used for to get the alignment.")
            cline = MuscleCommandline(input=in_file)
            return_code = subprocess.Popen(str(cline),stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=(sys.platform!="win32"))
            alignment = AlignIO.read(return_code.stdout, "fasta")        
    
    print(("Align length :" , alignment.get_alignment_length()))
    all_records = list(alignment)
    for i in range(len(alignment)) :
        print(('description:', all_records[i].description))
        print(('sequence:%s \n'%all_records[i].seq))

    summary_align = AlignInfo.SummaryInfo(alignment)
    consensus = summary_align.dumb_consensus()
    print(("Final_Consensus :\n",consensus))
    
    consensus_save = open(queryID+"_consensus.fasta",'w')
    consensus_save.write(">Final_consensus\n")
    for lines in consensus:
       consensus_save.write(lines)
       
    print("\nNOTE: If MSA did not show good result, please switch to another method ! ")
   
def psiBlastAuto(queryID, eval, output): # final PSI-BLAST
    psiblastout = output
    if not os.path.exists(psiblastout):
        iterations = PSI_ITERATIONS  #select in init.py
        global psi_cline
        blast_exe = 'psiblast'
        blast_db = 'nr'
        psi_input = queryID+".fasta"
        EVALUE_THRESH = DEF_EVALUE  
        psi_eval = EVAL_INCLUSION   #select in init.py
        psipssm = queryID+"_pssm"
        consensus = queryID+"_consensus.fasta"
        out_file = psiblastout
    
        if not os.path.exists(consensus):
            in_file = queryID+'.fasta'
    
        else:
            in_file = queryID+"_consensus.fasta"
            print("\nPSI-BLAST with the concensus Pattern !\n")

        psi_cline = NcbipsiblastCommandline(cmd = blast_exe, in_msa = in_file, db =blast_db, \
        					outfmt = 5,evalue = EVALUE_THRESH,out = out_file,\
        					inclusion_ethresh = psi_eval,\
        					out_ascii_pssm = psipssm,num_iterations = PSI_ITERATIONS) # for xml output outfmt = 5,tabular = 7, txt = 11
        
        print(psi_cline)
        print("\nThis process might take some time depending upon the number of iterations ! Default iterations = 5\n")
        stdout, stderr = psi_cline()
    print("\nPSI-BLAST iteration detail file is created !\n")

#Removes overlapping values in list
def unique(querylist):
    unique_file = open(queryID+'_lastPSI.xml','w')
    unique_result = []
    for i in querylist:
        if i not in unique_result:
            unique_result.append(i)
        for lines in unique_result:
            unique_file.write(lines)

def PSI_process(queryID, eval, output):
        FetchFasta(queryID)
        print((queryID,"fasta file is created !\n"))
        
        process_opt = PSI_PROCESS
        if process_opt == 'MANUAL':#select in init.py
            print(("%s entered the first PSI-BLAST process !\n"%queryID))
            psiBlastManual(queryID)
            print((queryID,"PSI-BLAST hits are saved after 1st iteration!\n"))
        
            queryfile = open(queryID+"_iteration1.xml")
            blastParser(queryfile, queryID)

            tophits = open(queryID+"_tophit_msa.fasta",'w')
            if MSA_SEQ == 'AUTO':#select in init.py
                blasthits_1stiter = open(queryID+'_psihits.txt')
                psihits = []
                for lines in blasthits_1stiter:
                    query_column = lines.split('\t')
                    psihits.append(query_column[0])
                print(("Top 5 sequences from 1st BLAST sent for MSA are:\n%s"%tophits))
                for hits in psihits[:5]:
                    print(hits)
                    handle = Entrez.efetch(db = "protein", id = hits, rettype = "fasta")
                    record = handle.read()
                    tophits.write(record)
            elif MSA_SEQ == 'SELECT': #select in init.py
                print(("\nSelect sequences from the file %s_psihits.txt for MSA and save them in a file as newline separated, provide file name below\n"%queryID))
                seq_msa = eval(input('-->'))
                seq_msa_open = open(seq_msa)
                for hits in seq_msa_open:
                    print(hits)
                    handle = Entrez.efetch(db = "protein", id = hits, rettype = "fasta")
                    record = handle.read()
                    tophits.write(record)
            else: #select in init.py
                print("No option is provided for the Sequence selection for MSA after 1st iteration of PSI !")
                pass
            tophits.close()
            print(("\n%s_tophit_msa.fasta : fasta sequence of top hits from 1st BLAST.\n"%(queryID)))
            in_file = queryID+"_tophit_msa.fasta"
            method_msa(queryID,in_file)
            psiBlastAuto(queryID,eval,output)
        
        elif process_opt == 'AUTO': #select in init.py
            print("The PSI-BLAST  has started, this might take longer !")
            psiBlastAuto(queryID,eval,output)
        
        else: #select in init.py
            print("Please choose one option for the PSI-BLAST running ! AUTO : for automatic PSI-BLAST, MANUAL : for choosing the sequences for PSSM !")

if __name__ == "__main__":
    
    queries = [] #all the queries for PSI-BLAST
    try: #from file
        query_file = open(sys.argv[1],'r') 
        for contents in query_file:
            query_var = contents.split('.')
            queries.append(query_var[0])
        query_file.close()
    except : #from Commandline
        query_CL = sys.argv[1:]
        for contents in query_CL:
            query_var = contents.split('.')
            queries.append(query_var[0])
    if not queries:
        print(" \n## Please provide single or multiple queries in file and mention as argv or give queries as argv ! ##\n")
        
    for queryID in queries: #send each query one by one
        eval = DEF_EVALUE
        PSI_process(queryID, eval, output)

    ####### E N D #####