import os
import urllib
from time import strftime
from datetime import datetime
from flask.json import jsonify
from flask import render_template, Response
from Bio.Blast.NCBIXML import parse
from pickleshare import PickleShareDB
from init import CGIJOBS, JOB_DATETIME, DEF_EVALUE, FILTER, MORFEUS_DIR, HOME, MATRIX, MORFEUS_USER, CBS, MAILUSER, \
	BLASTDATABASE, FINAL_RESULT, STORE_BLASTOUT

from Bio import Entrez
Entrez.email = MAILUSER

cgijobDB = PickleShareDB(CGIJOBS)


def check_acc_id(form):
	# Get the accession number if not provided
	if not form['ID'].count('.'):
		# Check id
		try:
			queryID = Entrez.efetch(db="protein", id=form['ID'], rettype="acc").read().rstrip()
		except urllib.error.HTTPError as err:
				if err.code == 400:
					return "", False  # Not valid

		return queryID, True  # Valid
	else:
		possible_queryID = form['ID']
		# Check validity
		try:
			r = Entrez.efetch(db="protein", id=possible_queryID, rettype="fasta").readlines()
		except urllib.error.HTTPError as err:
			if err.code == 400:
				return "", False  # If false

		return possible_queryID, True # if True



def launch_run(form, base_url, queryID):


	if 'JOB' in form:  # reload
		jobID = form['JOB'].value
	else:
		jobID = queryID + '-' + strftime(JOB_DATETIME)
		if jobID not in list(cgijobDB.keys()):  # new query
			# cgijobDB[jobID] = [evalue, querySeq, filter, matrix, cbs, limit, "refseq", email]
			cgijobDB[jobID] = [form['EVALUE'], '', form['FILTER'], MATRIX, form['CBS'], form['LIMIT'],
							   BLASTDATABASE, form['EMAIL']]

	# Message to return
	resultURL = "http://%smorfeus/#%s" % (base_url, jobID)
	return jsonify(message='Your result will be soon available here at <a href="%s">%s</a>' % (resultURL, resultURL),
				jobid=jobID)


def process_script(jobID):

	now = datetime.now()
	logout = []
	try:
		logfile = open(os.path.join(MORFEUS_DIR, HOME, jobID + ".log"))
	except FileNotFoundError:
		print("Run not launch, in queue")
		return jsonify([])

	for i, line in enumerate(logfile):

		line_start = line.split()[0]
		h, m, s = line_start.split(':')
		if int(h) != now.hour or int(m) != now.minute or (now.second - int(s)) >= 5:
			continue
		else:
			logout.append(line)

	return jsonify(logout)


def network_script(jobID):

	nodes_file = os.path.join(FINAL_RESULT, jobID + "_Attribute_cytoscape.sif")
	nodes = []
	lines = open(nodes_file, 'r').readlines()
	n = 0
	for line in lines:
		attrs = line.split("\t")
		nodes.append({"name": attrs[0], "eval": attrs[1], "org": attrs[2], "score": attrs[3]})
		n += 1

	return jsonify(nodes)


def align_script(jobID, hit):

	COLS = 80
	query = jobID.split('-')[0]
	query_path = os.path.join(STORE_BLASTOUT % jobID, query)

	br = next(parse(open(query_path)))
	string = []

	for alignment in br.alignments:
		if alignment.accession in hit:  # No '.x' version in accession!
			string.append("\n")
			string.append(">" + alignment.title)
			string.append("Length = " + repr(alignment.length))
			string.append("\n")
			for hsp in alignment.hsps:
				string.append("Score = %.1f bits (%i), Expect = %.1e, Identities = %i, Positives = %i, Gaps = %s" \
					  % (hsp.bits, hsp.score, hsp.expect, hsp.identities, hsp.positives, hsp.gaps))

				for pos in range(0, hsp.align_length, COLS):

					if hsp.align_length - pos <= COLS:
						qe = hsp.align_length  # last line
					else:
						qe = pos + COLS

					string.append("Query: %4i %s %4i" % (hsp.query_start + pos, hsp.query[pos:qe], hsp.query_start + qe - 1))
					string.append("            %s" % hsp.match[pos: pos + COLS])
					string.append("Sbjct: %4i %s %4i" % (hsp.sbjct_start + pos, hsp.sbjct[pos:qe], hsp.sbjct_start + qe - 1))
			break
	string = "\n".join(string)
	return render_template("align.html", corps=string)


def xsl_script(jobID, blastout):

	xml_file = open(os.path.join(STORE_BLASTOUT % jobID, blastout), 'r')
	string = []
	string.append(xml_file.readline())
	xml_file.readline()
	string.append('<!DOCTYPE BlastOutput SYSTEM  "http://www.ncbi.nlm.nih.gov/dtd/NCBI_BlastOutput.dtd">')
	string.append('<?xml-stylesheet type="text/xsl" href="blast.xsl"?>')
	string.append(xml_file.read())
	string = "\n".join(string)
	return Response(string, mimetype='text/xml')
