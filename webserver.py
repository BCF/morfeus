import re
import uuid
import requests as rq
from time import sleep
from os.path import join
from init import HOME, FINAL_RESULT, API_KEY, EUTILSJOBS, EUTILS_RESULT
from pickleshare import PickleShareDB
from server_script import launch_run, process_script, network_script, check_acc_id, align_script, xsl_script
from flask import Flask, render_template, abort, request

app = Flask(__name__, template_folder=join(HOME, "templates"), static_folder=join(HOME, "static"))

eutilsjobDB = PickleShareDB(EUTILSJOBS)
eutilsresultDB = PickleShareDB(EUTILS_RESULT)

# Start page and form receive to launch morfeus run
@app.route('/', methods=['GET', 'POST'])
def main_page():

	# When arriving on the website
	if request.method == 'GET':
		return render_template('morfeus.html', error_id="")

	# When the form is send
	else:
		form =  dict(request.form)
		# check the query id
		queryID, valid = check_acc_id(form)
		if valid:
			# Launch the run and return the jobID to the webpage if the queryID is valid
			return launch_run(form, request.base_url, queryID)
		else:
			print("Query not valid")
			return render_template('morfeus.html', error_id="style='background-color: red;'")
			# return jsonify(message="id error")

# Launch the process function, serve to test if the job as ended
@app.route('/cgi-bin/process.py')
def process():
	job = request.args.get('job')
	return process_script(job)


# Launch the network function to Retrieve the network information for d3js
@app.route('/cgi-bin/network.py')
def network():
	job = request.args.get('job')
	return network_script(job)


@app.route('/cgi-bin/align.py')
def align():
	job = request.args.get('job')
	blastout = request.args.get('blastout')
	return align_script(job, blastout)


# Call the script to render the detail of the blast output
@app.route('/cgi-bin/xsl.py')
def xsl():
	job = request.args.get('job')
	blastout = request.args.get('blastout')
	return xsl_script(job, blastout)


# Just serve for the xsl_script when it make a href to blast.xsl
# Not really pretty (especially in cgi-bin) but no time
@app.route('/cgi-bin/blast.xsl')
def blast_xsl():
	return render_template("blast.xsl")


@app.route('/eutils/esearch')
def esearch():
	db = request.args.get('db')
	term = request.args.get('term')
	# Create a picklesharedb entry with the term + the jobId
	eutilsjobID = str(uuid.uuid4())
	# create the entry in todo_eutils.db
	eutilsjobDB[eutilsjobID] = ""
	# print("create eutilsjob")
	while True:
		sleep(0.05)
		jobs_authorization = eutilsresultDB.keys()
		for jobs_approval in jobs_authorization:
			if eutilsjobID in eutilsresultDB[jobs_approval]:
				if API_KEY == "":
					res = rq.get(f"http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db={db}" + \
									   f"&term={term.replace(' ', '+')}").content
				else:
					res = rq.get(f"http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db={db}" + \
									   f"&term={term.replace(' ', '+')}&api_key={API_KEY}").content
				# print("end")
				return res


@app.route('/eutils/efetch')
def efetch():
	db = request.args.get('db')
	term = request.args.get('id')
	# Create a picklesharedb entry with the term + the jobId
	eutilsjobID = str(uuid.uuid4())
	# create the entry in todo_eutils.db
	eutilsjobDB[eutilsjobID] = ""
	# print("create efetch")
	while True:
		sleep(0.05)
		jobs_authorization = eutilsresultDB.keys()
		for jobs_approval in jobs_authorization:
			if eutilsjobID in eutilsresultDB[jobs_approval]:
				if API_KEY == "":
					res = rq.get(f"http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db={db}" + \
									   f"&id={term.replace(' ', '+')}").content
				else:
					res = rq.get(f"http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db={db}" + \
									   f"&id={term.replace(' ', '+')}&api_key={API_KEY}").content
				# print("end")
				return res


# Detect if the request correspond to a job.
# If so, read the result and return it to the ajax request
@app.route('/<page>')
def res_and_dummy(page):
	# Check if dummy.html is ask and return it
	if page == "dummy.html":
		return render_template('dummy.html')
	else:
		# Check if its a job
		re_res = re.search("^[A-Z]{1}P_\d*\.\d-\d*", page)
		if re_res != None:
			try:
				return open(join(FINAL_RESULT, page), "r").read()  # Read the result html and return it to ajax call
			except:
				print("No result html file found")
		else:
			print("Doesn't correspond to a run")


# All the help page are manage by morfeus_info
@app.route('/help/')
@app.route('/help/<page>')
def morfeus_info(page=''):
	list_file = ['morFeus-Info.html', 'morFeus-Output.html', 'morFeus-about.html',
		'people.html', 'contact.html', 'morFeus-VM.html', 'index.html']
	
	if page == '':  # return index
		return render_template("/help/index.html")
	elif page not in list_file:
		abort(404)
	else:
		return render_template(join("help", page))


if __name__ == "__main__":
	app.run(Thread=True)

