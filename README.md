## morFeus

morFeus is a tool proposing a Command line interface (CLI) and a Web Interface 
dedicated to the research of remote orthologs based on 
the use of clustering, reciprocal best hit and network scoring. 

## Installation 

First download morFeus
```
git clone https://framagit.org/BCF/morfeus.git
```
### Dependencies

list of dependencies: 
1. Python (tested with Python 3.6 and 3.7)
2. Biopython http://www.biopython.org
3. NetworkX http://networkx.github.io/
4. gnuplot http://www.gnuplot.info/
5. NCBI blast+ ftp://ftp.ncbi.nlm.nih.gov/blast/executables/
6. Morfeus uses BLAST against the protein sequences NCBI RefSeq database available at http://www.ncbi.nlm.nih.gov/refseq/ .
7. pickleshare = 0.7.5
8. Pathos 0.2.1

morFeus has multiple dependencies, but a conda environment is given to fullfil those, except Blast+ as the conda version is quite outdated.

It can be install with this command and will be nammed morfeus3:
```
conda env create -f morfeus_environment.yml
```

and activate with :
```
conda activate morfeus3
``` 

Blast+ can be downloaded [here](ftp://ftp.ncbi.nlm.nih.gov/blast/executables/)

Morfeus uses BLAST against the protein sequences NCBI RefSeq database available at http://www.ncbi.nlm.nih.gov/refseq/.



### Compilation

To build the c++ optimisation use this command:
```
cd lib && python setup.py build_ext --inplace && cd ..
```

If you don't use it change the variable `USE_CPP` to False in init.py

### Configuration 

In the file init.py 3 variable need to be set :
```
MORFEUS_USER    = "user" 			                # your user name
BLASTP          = "/opt/blast/bin/blastp" 	        # path to blast binary
DATABASE        = "/opt/blast/db/refseq_protein"    # path to downloaded refseq database
```

It's recommended to setup the API_Key variable for faster loading on website: 
```{python}
# API_Key to make the esearch/efetch request faster and
# not being limited to 3 search by sec on the IP address.
# If you don't have any, information about their creation are here:
# https://ncbiinsights.ncbi.nlm.nih.gov/2017/11/02/new-api-keys-for-the-e-utilities/
API_KEY = "9bd32707762da632ec5cbb8e452639d81808"   
```
## Launch

morFeus can be launched as a CLI program with its results being saved in the folder 
final_results, or as a web interface, it need an internet connection to work : 

### CLI 

In command line interface you just need to precise the RefSeqID Accession ID of the query.
```
# Activation of the conda environment
conda activate morfeus3

# Launch morFeus
python morfeusd.py [Refseq accession_id]
```

### Web 

First, launch the morFeus in daemon mode
```
# Activation of the conda environment
conda activate morfeus3

# Launch morFeus in daemon mode
python morfeusd.py 
```

And in another terminal launch the webserver
```
# Activation of the conda environment
conda activate morfeus3

# launch the webserver on localhost
gunicorn webserver:app 
```




